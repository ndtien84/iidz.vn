﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using shop.com.Utils;

namespace shop.com
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_End()
        {
            Helpers.ResetOnlineCount();
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            //Helpers.SetAuthenSession(User.Identity.IsAuthenticated);
            Helpers.SetOnlineCount(User.Identity.IsAuthenticated, true);
            Helpers.IncreaseVisitCount();
        }

        protected void Session_End(object sender, EventArgs e)
        {
            Helpers.SetOnlineCount(bool.Parse(Session["IsAuthenticated"].ToString()), false);
        }


        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (!Request.Url.Host.StartsWith("www") && !Request.Url.IsLoopback)
            {
                UriBuilder builder = new UriBuilder(Request.Url);
                builder.Host = "www." + Request.Url.Host;
                Response.StatusCode = 301;
                Response.AddHeader("Location", builder.ToString());
                Response.End();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using shop.com.Utils;

namespace shop.com.Models
{
    #region S_Settings

    [Table("S_Settings")]
    public class S_SettingsModel
    {
        [Key, Column(Order = 1)]
        public string SKey { get; set; }
        /// <summary>
        /// site | ...
        /// </summary>
        [Key, Column(Order = 2)]
        public string SType { get; set; }
        public string SValue { get; set; }

        public S_SettingsModel()
        {
            SKey = "";
            SType = "";
            SValue = "";
        }
    }

    public class S_SiteSettingsModel
    {
        public string Copyright { get; set; }
        public string Logo { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }

        public string Slogan { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Cellphone { get; set; }
        
        public string Yahoo { get; set; }
        public string Skype { get; set; }

        public string FaceBook { get; set; }
        public string Twitter { get; set; }
        public string GooglePlus { get; set; }
        public string LinkedIn { get; set; }
        public string Pinterest { get; set; }
        public string Youtube { get; set; }
        public string Vimeo { get; set; }
        public string Instagram { get; set; }
        public string Tumblr { get; set; }
        public string Flickr { get; set; }

        public string SMTPHost { get; set; }
        public string SMTPPort { get; set; }
        public string SMTPUsername { get; set; }
        public string SMTPPassword { get; set; }
        public bool SMTPEnableSSL { get; set; }

        public string GooleMap { get; set; }
        public string GoogleAnalytics { get; set; }

        public bool EnableLog { get; set; }

        public S_SiteSettingsModel()
        {
            Copyright = "";
            Logo = "default.png";
            Title = "";
            Description = "";
            Keywords = "";
            Slogan = "";
            CompanyName = "";
            Email = "ndtien84@gmail.com";
            Address = "";
            Phone = "";
            Fax = "";
            Cellphone = "";

            Yahoo = "";
            Skype = "";

            FaceBook = "#";
            Twitter = "#";
            GooglePlus = "#";
            LinkedIn = "#";
            Pinterest = "#";
            Youtube = "#";
            Vimeo = "#";
            Instagram = "#";
            Tumblr = "#";
            Flickr = "#";

            SMTPHost = "";
            SMTPPort = "";
            SMTPUsername = "";
            SMTPPassword = "";
            SMTPEnableSSL = true;

            GooleMap = "";
            GoogleAnalytics = "";

            EnableLog = true;
        }
    }

    public class S_SiteCountModel
    {
        public int OnlineMember { get; set; }
        public int OnlineGuest { get; set; }
        public int TotalVisit { get; set; }

        public S_SiteCountModel()
        {
            OnlineMember = 0;
            OnlineGuest = 0;
            TotalVisit = 0;
        }
    }

    #endregion

    #region S_Category

    [Table("S_Category")]
    public class S_CategoryModel
    {
        [Key, Column(Order = 1)]
        public string CategoryID { get; set; }
        [Key, Column(Order = 2)]
        public string CultureCode { get; set; }
        /// <summary>
        /// product | news ...
        /// </summary>
        public string CategoryType { get; set; }
        public string GroupName { get; set; }
        public string ParentsID { get; set; }
        public string PrimaryImage { get; set; }
        public string SecondaryImage { get; set; }
        public string PrimaryDocument { get; set; }
        public string SecondaryDocument { get; set; }
        public string JsonGlobal { get; set; }
        public string Label { get; set; }
        public int Priority { get; set; }
        public bool Visible { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CreateBy { get; set; }
        public string LastUpdateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public string Name { get; set; }
        public string UrlKey { get; set; }
        public string JsonCulture { get; set; }
        public string Overview { get; set; }
        public string Information { get; set; }
        public string Summary { get; set; }
        public string Details { get; set; }
        /// <summary>
        /// if CategoryType = product : ChildCount is total product in this category | if CategoryType = news : ChildCount is total News in this category | ...
        /// </summary>
        [NotMapped]
        public int ChildCount { get; set; }
        [NotMapped]
        public string ClassName { get; set; }
        [NotMapped]
        public int Level { get; set; }
        [NotMapped]
        public int SiblingIndex { get; set; }

        public S_CategoryModel()
        {
            CategoryID = "-1";
            CultureCode = Helpers.DefaultCultureCode;
            CategoryType = "product";
            GroupName = "global";
            ParentsID = "-1";
            PrimaryImage = "default.png";
            SecondaryImage = "default.png";
            PrimaryDocument = "";
            SecondaryDocument = "";
            JsonGlobal = "";
            Label = "";
            Priority = 0;
            Visible = true;
            IsDeleted = false;
            StartDate = DateTime.Now;
            EndDate = DateTime.Now;
            CreateBy = "-1";
            LastUpdateBy = "-1";
            CreateDate = DateTime.Now;
            UpdateDate = DateTime.Now;

            Name = "";
            UrlKey = "";
            JsonCulture = "";
            Overview = "";
            Information = "";
            Summary = "";
            Details = "";

            ChildCount = 0;
            ClassName = "";
            Level = 0;
            SiblingIndex = 0;
        }
    }

    #endregion

    #region S_Product

    [Table("S_Product")]
    public class S_ProductModel
    {
        [Key, Column(Order = 1)]
        public string ProductID { get; set; }
        [Key, Column(Order = 2)]
        public string CultureCode { get; set; }
        public string CategoryID { get; set; }
        public string GroupName { get; set; }
        public string PrimaryImage { get; set; }
        public string SecondaryImage { get; set; }
        public string ReserveImage { get; set; }
        public string PrimaryDocument { get; set; }
        public string SecondaryDocument { get; set; }
        public string ReserveDocument { get; set; }
        public string JsonGlobal { get; set; }
        public string Label { get; set; }
        public int Priority { get; set; }
        public bool Visible { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsOutOfStock { get; set; }
        public string ProductCode { get; set; }
        public int Quantity { get; set; }
        public int ViewCount { get; set; }
        public int LikeCount { get; set; }
        public decimal OldPrice { get; set; }        
        public decimal CurrentPrice { get; set; }
        public string PriceString { get; set; }
        public string Tag { get; set; }
        public string Link { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CreateBy { get; set; }
        public string LastUpdateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public string Name { get; set; }
        public string UrlKey { get; set; }
        public string JsonCulture { get; set; }
        public string Source { get; set; }
        public string PostedBy { get; set; }
        public string Description { get; set; }
        public string Overview { get; set; }
        public string Information { get; set; }
        public string Summary { get; set; }
        public string Details { get; set; }

        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public string CategoryName { get; set; }
        [NotMapped]
        public List<S_ContentModel> ListImage { get; set; }

        public S_ProductModel()
        {
            ProductID = "-1";
            CultureCode = Helpers.DefaultCultureCode;
            GroupName = "global";
            CategoryID = "-1";
            PrimaryImage = "default.png";
            SecondaryImage = "default.png";
            ReserveImage = "default.png";
            PrimaryDocument = "";
            SecondaryDocument = "";
            ReserveDocument = "";
            JsonGlobal = "";
            Label = "";
            Priority = 0;
            Visible = true;
            IsDeleted = false;
            IsOutOfStock = false;
            ProductCode = "";
            Quantity = 0;
            ViewCount = 0;
            LikeCount = 0;
            OldPrice = 0;
            CurrentPrice = 0;
            PriceString = "";
            Tag = "";
            Link = "";
            StartDate = DateTime.Now;
            EndDate = DateTime.Now;
            CreateBy = "-1";
            LastUpdateBy = "-1";
            CreateDate = DateTime.Now;
            UpdateDate = DateTime.Now;

            Name = "";
            UrlKey = "";
            JsonCulture = "";
            Source = "";
            PostedBy = "";
            Description = "";
            Overview = "";
            Information = "";
            Summary = "";
            Details = "";

            TotalRow = 0;
            CategoryName = "";
            ListImage = new List<S_ContentModel>();
        }
    }

    #endregion

    #region S_ProductCategory

    [Table("S_ProductCategory")]
    public class S_ProductCategoryModel
    {
        [Key, Column(Order = 1)]
        public string ProductID { get; set; }
        [Key, Column(Order = 2)]
        public string CategoryID { get; set; }

        public S_ProductCategoryModel()
        {
            ProductID = "-1";
            CategoryID = "-1";
        }
    }

    #endregion

    #region S_Content

    [Table("S_Content")]
    public class S_ContentModel
    {
        [Key, Column(Order = 1)]
        public string ContentID { get; set; }
        [Key, Column(Order = 2)]
        public string CultureCode { get; set; }
        public string CategoryID { get; set; }
        public string GroupName { get; set; }
        /// <summary>
        /// product-image | category-image | banner | news | about-us | product-details-img | content-details-img
        /// </summary>
        public string ContentType { get; set; }
        /// <summary>
        /// -1 | ID from other table
        /// </summary>
        public string ParentsID { get; set; }
        /// <summary>
        /// -1 | S_Category | S_Product
        /// </summary>
        public string ParentsTable { get; set; }
        public string PrimaryImage { get; set; }
        public string SecondaryImage { get; set; }
        public string ReserveImage { get; set; }
        public string PrimaryDocument { get; set; }
        public string SecondaryDocument { get; set; }
        public string ReserveDocument { get; set; }
        public string JsonGlobal { get; set; }
        public string Label { get; set; }
        public string Link { get; set; }
        public string Tag { get; set; }
        public int Priority { get; set; }
        public int ViewCount { get; set; }
        public int LikeCount { get; set; }
        public bool Visible { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CreateBy { get; set; }
        public string LastUpdateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public string Name { get; set; }
        public string UrlKey { get; set; }
        public string JsonCulture { get; set; }
        public string Description { get; set; }
        public string Overview { get; set; }
        public string Information { get; set; }
        public string Summary { get; set; }
        public string Details { get; set; }

        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public string ParentsName { get; set; }
        [NotMapped]
        public List<S_ContentModel> ListChildContent { get; set; }

        public S_ContentModel()
        {
            ContentID = "-1";
            CultureCode = Helpers.DefaultCultureCode;
            CategoryID = "-1";
            GroupName = "global";
            ContentType = "";
            ParentsID = "-1";
            ParentsTable = "-1";
            PrimaryImage = "default.png";
            SecondaryImage = "default.png";
            ReserveImage = "default.png";
            PrimaryDocument = "";
            SecondaryDocument = "";
            ReserveDocument = "";
            JsonGlobal = "";
            Label = "";
            Link = "";
            Tag = "";
            Priority = 0;
            ViewCount = 0;
            LikeCount = 0;
            Visible = true;
            IsDeleted = false;
            StartDate = DateTime.Now;
            EndDate = DateTime.Now;
            CreateBy = "-1";
            LastUpdateBy = "-1";
            CreateDate = DateTime.Now;
            UpdateDate = DateTime.Now;

            Name = "";
            UrlKey = "";
            JsonCulture = "";
            Description = "";
            Overview = "";
            Information = "";
            Summary = "";
            Details = "";

            TotalRow = 0;
            ParentsName = "";

            ListChildContent = new List<S_ContentModel>();
        }
    }

    #endregion

    #region S_ContentCategory

    [Table("S_ContentCategory")]
    public class S_ContentCategoryModel
    {
        [Key, Column(Order = 1)]
        public string ContentID { get; set; }
        [Key, Column(Order = 2)]
        public string CategoryID { get; set; }

        public S_ContentCategoryModel()
        {
            ContentID = "-1";
            CategoryID = "-1";
        }
    }

    #endregion


    #region S_OrderStatus

    [Table("S_OrderStatus")]
    public class S_OrderStatusModel
    {
        [Key]
        public string StatusID { get; set; }
        public string Name { get; set; }
        public string Summary { get; set; }
        public int Priority { get; set; }
        public bool Visible { get; set; }
        public bool IsDelete { get; set; }

        public S_OrderStatusModel()
        {
            StatusID = "-1";
            Name = "";
            Summary = "";
            Priority = 0;
            Visible = true;
            IsDelete = false;
        }
    }

    #endregion

    #region S_CartTemp

    [Table("S_CartTemp")]
    public class S_CartTempModel
    {
        [Key]
        public string CartTempID { get; set; }
        /// <summary>
        /// cart | wishlist | compare
        /// </summary>
        public string TempType { get; set; }
        public string UserID { get; set; }
        public string ProductID { get; set; }
        public int Quantity { get; set; }
        public string Notes { get; set; }
        public string Overview { get; set; }
        public string Summary { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ExpiredDate { get; set; }

        [NotMapped]
        public string ProductName { get; set; }
        [NotMapped]
        public string PrimaryImage { get; set; }
        [NotMapped]
        public decimal CurrentPrice { get; set; }
        [NotMapped]
        public string ProductCode { get; set; }

        public S_CartTempModel()
        {
            CartTempID = "-1";
            TempType = "cart";
            UserID = "-1";
            ProductID = "-1";
            Quantity = 0;
            Notes = "";
            Overview = "";
            Summary = "";
            CreateDate = DateTime.Now;
            ExpiredDate = DateTime.Now.AddYears(1);

            ProductName = "";
            PrimaryImage = "default.png";
            CurrentPrice = 0;
            ProductCode = "";
        }
    }

    #endregion

    #region S_Order

    [Table("S_Order")]
    public class S_OrderModel
    {
        [Key]
        public string OrderID { get; set; }
        public string InvoiceID { get; set; }
        public string UserID { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string CellPhone { get; set; }
        public string Email { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
	    public string City { get; set; }
        public string Address { get; set; }
        public string ShipInfo { get; set; }

        public decimal Tax { get; set; }
        public decimal ShipCost { get; set; }
	    public decimal AdditionAmount { get; set; }
        public string PromoteInfo { get; set; }

        public string PaymentType { get; set; }
        public string StatusID { get; set; }
        public string Overview { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public string Details { get; set; }
        public bool Approved { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime AprrovedDate { get; set; }

        [NotMapped]
        public string StatusName { get; set; }
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public decimal TotalPrice { get; set; }
        [NotMapped]
        public int TotalDetails { get; set; }
        public List<S_OrderDetailsModel> ListDetails { get; set; }

        public S_OrderModel()
        {
            OrderID = "-1";
            InvoiceID = "-1";
            UserID = "-1";
            Name = "";
            Phone = "";
            CellPhone = "";
            Email = "";
            Zip = "";
            Country = "";
            State = "";
            City = "";
            Address = "";
            ShipInfo = "";

            Tax = 0;
            ShipCost = 0;
            AdditionAmount = 0;
            PromoteInfo = "";

            PaymentType = "";
            StatusID = "";
            Overview = "";
            Summary = "";
            Description = "";
            Details = "";
            Approved = false;
            IsDelete = false;
            CreateDate = DateTime.Now;
            UpdateDate = DateTime.Now;
            AprrovedDate = DateTime.Now;

            StatusName = "";

            TotalRow = 0;
            TotalPrice = 0;
            TotalDetails = 0;
            ListDetails = new List<S_OrderDetailsModel>();
        }
    }

    #endregion

    #region S_OrderDetails

    [Table("S_OrderDetails")]
    public class S_OrderDetailsModel
    {
        [Key]
        public string DetailsID { get; set; }
        public string OrderID { get; set; }
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public string Notes { get; set; }
        public string Overview { get; set; }
        public string Summary { get; set; }

        [NotMapped]
        public string PrimaryImage { get; set; }

        public S_OrderDetailsModel()
        {
            DetailsID = "-1";
            OrderID = "-1";
            ProductID = "-1";
            ProductName = "";
            ProductCode = "";
            Quantity = 0;
            Price = 0;
            Notes = "";
            Overview = "";
            Summary = "";

            PrimaryImage = "default.png";
        }
    }

    #endregion


    #region Users
    
    public class S_UsersModel
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public string CellPhone { get; set; }
        public string Address { get; set; }
        public string JsonContent { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime LastLoginDate { get; set; }

        public int TotalRow { get; set; }
        public List<S_RolesModel> ListRoles { get; set; }

        public S_UsersModel()
        {
            Id = "-1";
            DisplayName = "";
            CellPhone = "";
            Address = "";
            JsonContent = "";
            Email = "";
            UserName = "";

            CreateDate = DateTime.Now;
            UpdateDate = DateTime.Now;
            LastLoginDate = DateTime.Now;

            TotalRow = 0;
            ListRoles = new List<S_RolesModel>();
        }
    }
    
    #endregion

    #region Roles

    public class S_RolesModel
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public S_RolesModel()
        {
            Id = "-1";
            Name = "";
        }
    }
    
    #endregion

    #region Others

    public class PartialListProductModel
    {
        /// <summary>
        /// catid | proid
        /// </summary>
        public string PType { get; set; }
        public int PageIndex { get; set; }
        public string CurrentID { get; set; }
        public string CategoryName { get; set; }
        public string CategoryUrlKey { get; set; }
        public int TotalPage { get; set; }

        public S_ProductModel ObjProduct { get; set; }
        public List<S_ProductModel> ListProduct { get; set; }


        public PartialListProductModel()
        {
            PType = "catid";
            CurrentID = "all";
            PageIndex = 1;
            TotalPage = 1;
            CategoryName = "";
            CategoryUrlKey = "";
            ListProduct = new List<S_ProductModel>();
            ObjProduct = new S_ProductModel();
        }
    }

    #endregion

    #region S_Page

    public class S_PageModel
    {
        public string PageID { get; set; }
        public string PageTitle { get; set; }
        public string ActionName { get; set; }
        public string UrlKey { get; set; }
        public string LinkTitle { get; set; }

        public S_PageModel()
        {
            PageID = "";
            PageTitle = "";
            ActionName = "";
            UrlKey = "";
            LinkTitle = "";
        }
    }

    #endregion


    #region S_Log

    [Table("S_Log")]
    public class S_LogModel
    {
        [Key]
        public string LogID { get; set; }
        public S_LogType LogType { get; set; }
        public string UserID { get; set; }
        public string Summary { get; set; }
        public string Details { get; set; }
        public DateTime CreateDate { get; set; }

        [NotMapped]
        public string DisplayName { get; set; }
        [NotMapped]
        public int TotalRow { get; set; }

        public S_LogModel()
        {
            LogID = "-1";
            LogType = S_LogType.All;
            UserID = "-1";
            Summary = "";
            Details = "";
            CreateDate = DateTime.Now;
            TotalRow = 0;
        }
    }

    #endregion
}
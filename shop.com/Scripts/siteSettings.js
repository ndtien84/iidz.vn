﻿'use strict';

ndtApp.controller('ssCtrl', ['$scope', '$http', '$compile', 'workWithList', function ($scope, $http, $compile, workWithList) {
    $scope.ctn = $('.site-settings-ctn'); // jquery object, must have to use the ndtLightbox plugins

    $scope.isHost = false;
    $scope.siteSettings = {};

    $scope.isSending = false;
    $scope.i18n = shopText.siteSettings.template;

    //#region Init & Utilities

    $scope.init = function () {
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'load-init'
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) console.log(ctJson.error);else {
                $scope.isHost = ctJson.isHost;
                $scope.siteSettings = ctJson.siteSettings;
            }
        });
    };

    //#endregion

    //#region Save Site Settings

    $scope.saveSiteSettings = function () {
        if ($scope.isSending) return;

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'save-site-settings',
                data: JSON.stringify($scope.siteSettings)
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (result.error) console.log(ctJson.error);else {
                appHelpers.alertCall(shopText.siteSettings.updateSuccess, 5000);
            }
        });
    };

    //#endregion

    //#region Test SMTP

    $scope.testSMTP = function () {
        if ($scope.isSending) return;

        $scope.isSending = true;
        appHelpers.callOverlayLoading();

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'test-smtp',
                data: JSON.stringify($scope.siteSettings)
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (result.error) appHelpers.alertCall(result.error, 25000, 'alert-danger');else {
                appHelpers.alertCall('SMTP Settings OK', 15000);
            }
        });
    };

    //#endregion

    //#region Upload Logo

    $scope.uploadLogo = function () {
        var fileSelect = $scope.ctn.find('.fulLogo');
        if (fileSelect.val() == '') {
            appHelpers.alertCall(shopText.siteSettings.requireSelectLogo, 5000, 'alert-danger');
            return;
        }

        $scope.uploadFiles(fileSelect, 'Logo');
    };

    //#endregion

    //#region Upload Files

    // fileType: Logo
    $scope.uploadFiles = function (objSelectFile, fileType) {
        if ($scope.isSending) return;

        var objQuery = {
            fileType: fileType
        };

        var objForm = new FormData();
        objForm.append('act', 'upload-files');
        objForm.append('data', JSON.stringify(objQuery));

        var listFiles = objSelectFile[0].files;

        if (listFiles.length > 0) {
            for (var i = 0; i < listFiles.length; i++) {
                objForm.append('file' + i, listFiles[i]);
            }
        }

        $scope.isSending = true;
        $.ajax({
            url: window.location.href,
            type: 'POST',
            contentType: false,
            processData: false,
            data: objForm,
            success: function success(result) {
                var ctJson = angular.fromJson(result);
                if (ctJson.error) console.log(ctJson.error);else {
                    if (fileType == 'Logo') {
                        $scope.siteSettings.Logo = ctJson.newLogo;
                        //workWithList.AddObjectToList($scope.lstProduct, $scope.curProduct, 'ProductID');
                    }

                    $scope.$apply();

                    objSelectFile.parents('.input-group').find('.browse-file-name').val('');
                    objSelectFile.val('');
                }

                $scope.isSending = false;
            }
        });
    };

    //#endregion

    // init the control
    $scope.init();
}]);


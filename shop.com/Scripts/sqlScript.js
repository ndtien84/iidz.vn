﻿'use strict';

ndtApp.controller('sqlCtrl', ['$scope', '$http', '$compile', 'workWithList', function ($scope, $http, $compile, workWithList) {
    $scope.ctn = $('.sql-script-ctn'); // jquery object, must have to use the ndtLightbox plugins

    $scope.objQuery = {
        runAsScript: false,
        sqlCommand: ''
    };

    $scope.isSending = false;

    $scope.lstResult = [];

    $scope.submitSql = function () {
        if ($scope.isSending) return;

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'submit-sql',
                data: JSON.stringify($scope.objQuery)
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) console.log(ctJson.error);else {
                appHelpers.alertCall('DONE', 5000);

                if (!$scope.objQuery.runAsScript) {
                    console.log('is query');
                    $scope.lstResult = ctJson.lstResult;
                }
            }
        });
    };

    //#region Init & Utilities

    $scope.init = function () {};

    //#endregion

    $scope.init();
}]);


﻿'use strict';

ndtApp.controller('logCtrl', ['$scope', '$http', '$compile', '$filter', 'workWithList', 'dateHelpers', function ($scope, $http, $compile, $filter, workWithList, dateHelpers) {
    $scope.ctn = $('.manage-log-ctn');

    $scope.objLB;
    $scope.tplLogDetail = '';
    $scope.isSending = false;

    $scope.fromDate = '';
    $scope.toDate = '';

    $scope.curType = 'date-range'; // date-range | ...
    $scope.logType = 0; // All
    $scope.orderColumn = "CreateDate";
    $scope.orderDirection = "desc";
    $scope.pageIndex = 1;
    $scope.pageSize = 15;
    $scope.totalRow = 0;
    $scope.totalPage = 0;

    $scope.lstLog = [];
    $scope.curLog = {};

    $scope.lstLogType = [{ id: 0, name: 'All' }, { id: 1, name: 'Create Log Error' }, { id: 2, name: 'User Login' }, { id: 3, name: 'User Register' }, { id: 4, name: 'User Update Profile' }, { id: 5, name: 'User CreateBy Admin' }, { id: 6, name: 'User UpdateBy Admin' }, { id: 7, name: 'User Delete' }, { id: 8, name: 'Product Add' }, { id: 9, name: 'Product Update' }, { id: 10, name: 'Product Delete' }, { id: 11, name: 'Content Add' }, { id: 12, name: 'Content Update' }, { id: 13, name: 'Content Delete' }, { id: 14, name: 'Order Create' }, { id: 15, name: 'Order Update' }, { id: 16, name: 'Order Delete' }];

    $scope.filterTitle = '';

    //#region Init & Utilities

    $scope.init = function () {

        //#region Query Hash

        if (appHelpers.queryString("type")) $scope.curType = appHelpers.queryString("type");

        if (appHelpers.queryHash("lt")) $scope.logType = parseInt(appHelpers.queryHash("lt"));

        if (appHelpers.queryHash('oc')) $scope.orderColumn = appHelpers.queryHash('oc');

        if (appHelpers.queryHash('od')) $scope.orderDirection = appHelpers.queryHash('od');

        if (appHelpers.queryHash('pg')) $scope.pageIndex = appHelpers.queryHash('pg');

        if (appHelpers.queryHash('fd')) {
            $scope.fromDate = appHelpers.queryHash('fd');
        } else $scope.fromDate = dateHelpers.getCurDateString('date', -60);

        if (appHelpers.queryHash('td')) $scope.toDate = appHelpers.queryHash('td');else $scope.toDate = dateHelpers.getCurDateString();

        //#endregion

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'load-init'
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.tplLogDetail = ctJson.tplLogDetail;

                $scope.getList($scope.curType, $scope.pageIndex);
            }
        });
    };

    $scope.getPageArray = function () {
        return new Array($scope.totalPage);
    };

    $scope.getLogTypeName = function (logTypeID) {
        return $scope.lstLogType.filter(function (item) {
            return item.id == logTypeID;
        })[0].name;
    };

    //#endregion

    //#region Get List Logs

    // type : date-range | ...
    $scope.getList = function (type, pageIndex) {
        if ($scope.isSending) return;

        if (!type) type = 'date-range';
        if (!pageIndex) pageIndex = 1;

        // lt: filter log type
        // oc: order by ColumnName
        // od: order direction (desc/asc)
        // pg: page index
        var hashString = '#lt=' + $scope.logType + '&oc=' + $scope.orderColumn + '&od=' + $scope.orderDirection + '&pg=' + pageIndex;

        if (type == 'date-range') {
            hashString += '&fd=' + $scope.fromDate + '&td=' + $scope.toDate;
        }

        window.location.href = window.location.pathname + '?type=' + type + hashString;

        if (type != $scope.curType || !appHelpers.queryString('type')) return;

        var data = {
            Type: type,
            LogType: $scope.logType,
            FromDate: $scope.fromDate,
            ToDate: $scope.toDate,
            PageSize: $scope.pageSize,
            PageIndex: pageIndex,
            OrderColumn: $scope.orderColumn,
            OrderDirection: $scope.orderDirection
        };

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'get-list',
                data: JSON.stringify(data)
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.lstLog = ctJson.lstLog;
                $scope.totalRow = ctJson.totalRow;
                $scope.totalPage = ctJson.totalPage;

                $scope.pageIndex = pageIndex;
                $scope.curType = type;
            }
        });
    };

    // #endregion

    //#region Load Log By Profile

    // logtype : raw | simple
    $scope.loadLogDetails = function (objLog) {
        $scope.curLog = angular.fromJson(objLog.Details);

        $scope.objLB = $('body').ndtLightbox({
            title: 'Log',
            headerContent: '',
            //fixWidth: '70%',
            //fixHeight: '600',
            outerClass: 'form-log',
            content: $scope.tplLogDetail,
            onComplete: function onComplete() {},
            onSetContent: function onSetContent() {},
            onDestroy: function onDestroy() {}
        }, $compile, $scope);
    };

    //#endregion

    //#region Log Utilitites

    $scope.parseLogDetails = function (detailsString) {
        return angular.fromJson(detailsString);
    };

    $scope.getStatusName = function (statusID) {
        var sName = '';

        angular.forEach($scope.lstStatus, function (objStatus) {
            if (objStatus.StatusID == statusID) sName = objStatus.Name;
        });

        return sName;
    };

    $scope.getProviderName = function (providerID) {
        var pName = '';

        angular.forEach($scope.lstProvider, function (objProvider) {
            if (objProvider.ProviderID == providerID) pName = objProvider.Name;
        });

        return pName;
    };

    $scope.simplifyProfileForLog = function (objProfile, logType) {
        var displayField = [];

        if (logType == 'ApiCreateProfile' || logType == 'ApiUpdateProfile') {
            displayField = ['FirstName', 'LastName', 'Address', 'City', 'State', 'Zip', 'Birthday', 'Phone', 'AlternativePhone', 'DrugDose', 'DrugQuantity', 'InsuranceCarrierName', 'InsuranceBin', 'InsurancePCN', 'InsuranceGroup', 'InsuranceID', 'TransactionID', 'PatientID'];
        } else if (logType == 'PortalCreateProfile' || logType == 'PortalUpdateProfile') {
            displayField = ['FirstName', 'LastName', 'Address', 'City', 'State', 'Zip', 'Birthday', 'Phone', 'AlternativePhone', 'DrugDose', 'DrugQuantity', 'InsuranceCarrierName', 'InsuranceBin', 'InsurancePCN', 'InsuranceGroup', 'InsuranceID', 'CoPay28Days', 'CoPay84Days', 'Electronic', 'CommercialPlan', 'RejectReason', 'InsuranceUpdate', 'Notes'];
        } else if (logType == 'ChangeStatus') {
            displayField = ['PatientID', 'TransactionID', 'CoPay28Days', 'Notes', 'Electronic', 'CommercialPlan', 'InsuranceUpdate', 'RejectReason', 'RejectCode', 'RejectCodeName', 'InsurancePCN', 'InsuranceBin', 'InsuranceCarrierName', 'InsuranceGroup', 'InsuranceID'];
        }

        objProfile = angular.copy(objProfile);

        var isDisplay = false;

        for (var key in objProfile) {
            isDisplay = false;

            // THIS NOT WORK ON IE & SAFARI
            //for (var fName of displayField) {
            //    if (key == fName) {
            //        isDisplay = true;
            //        break;
            //    }
            //}

            $.each(displayField, function (index, value) {
                if (key == value) {
                    isDisplay = true;
                    return;
                }
            });

            if (!isDisplay) delete objProfile[key];
        }

        return objProfile;
    };

    //#endregion

    $scope.init();
}]);

//#region Filter ParseLog HTML

ndtApp.filter('parseLogHTML', function () {
    return function (objLog, scope) {
        var htmlString = '';
        var objDetails = angular.fromJson(objLog.Details);

        if (objLog.LogType == 'ApiCreateProfile') {

            //#region ApiCreateProfile

            htmlString += '<strong>' + scope.getProviderName(objLog.ProviderID) + '</strong> submitted a patient: <br/>';

            var simpleProfile = scope.simplifyProfileForLog(objDetails.ProfileCurrent, objLog.LogType);

            htmlString += '<div class="toggle-ctn">';
            htmlString += '<div class="show-details">Show/hide details</div>';

            htmlString += '<table class="profile-table toggle-item">';
            for (var key in simpleProfile) {
                htmlString += '<tr>';
                htmlString += '<td>' + key + '</td>';
                htmlString += '<td>' + simpleProfile[key] + '</td>';
                htmlString += '</tr>';
            }
            htmlString += '</table>';

            htmlString += '</div>';

            //#endregion
        } else if (objLog.LogType == 'PortalCreateProfile') {

                //#region PortalCreateProfile (CURRENTLY NOT USE, USER CAN NOT CREATE PATIENT PROFILE)

                htmlString += 'User ' + objLog.UserDisplayName + ' create a patient profile: <br/>';

                var simpleProfile = scope.simplifyProfileForLog(objDetails.ProfileCurrent, objLog.LogType);

                htmlString += '<div class="toggle-ctn">';
                htmlString += '<div class="show-details">Show/hide details</div>';

                htmlString += '<table class="profile-table toggle-item">';
                for (var key in simpleProfile) {
                    htmlString += '<tr>';
                    htmlString += '<td>' + key + '</td>';
                    htmlString += '<td>' + simpleProfile[key] + '</td>';
                    htmlString += '</tr>';
                }
                htmlString += '</table>';

                htmlString += '</div>';

                //#endregion
            } else if (objLog.LogType == 'ApiUpdateProfile') {

                    //#region ApiUpdateProfile

                    htmlString += '<strong>' + scope.getProviderName(objLog.ProviderID) + '</strong> re-sent info for patient: <br/>';

                    var simpleCurProfile = scope.simplifyProfileForLog(objDetails.ProfileCurrent, objLog.LogType);
                    var simplePrevProfile = scope.simplifyProfileForLog(objDetails.ProfilePrev, objLog.LogType);

                    htmlString += '<div class="toggle-ctn">';
                    htmlString += '<div class="show-details">Show/hide details</div>';

                    htmlString += '<table class="profile-table toggle-item">';

                    htmlString += '<tr>';
                    htmlString += '<th>&nbsp;</th>';
                    htmlString += '<th>Previous Info</th>';
                    htmlString += '<th>New Info</th>';
                    htmlString += '</tr>';

                    for (var key in simplePrevProfile) {
                        htmlString += '<tr>';
                        htmlString += '<td>' + key + '</td>';
                        htmlString += '<td>' + simplePrevProfile[key] + '</td>';
                        htmlString += '<td>' + simpleCurProfile[key] + '</td>';
                        htmlString += '</tr>';
                    }

                    htmlString += '</table>';

                    htmlString += '</div>';

                    //#endregion
                } else if (objLog.LogType == 'PortalUpdateProfile') {

                        //#region PortalUpdateProfile

                        htmlString += '<strong>' + objLog.UserDisplayName + '</strong> updated info for this patient: <br/>';

                        var simpleCurProfile = scope.simplifyProfileForLog(objDetails.ProfileCurrent, objLog.LogType);
                        var simplePrevProfile = scope.simplifyProfileForLog(objDetails.ProfilePrev, objLog.LogType);

                        htmlString += '<div class="toggle-ctn">';
                        htmlString += '<div class="show-details">Show/hide details</div>';

                        htmlString += '<table class="profile-table toggle-item">';

                        htmlString += '<tr>';
                        htmlString += '<th>&nbsp;</th>';
                        htmlString += '<th>Previous Info</th>';
                        htmlString += '<th>New Info</th>';
                        htmlString += '</tr>';

                        for (var key in simplePrevProfile) {
                            htmlString += '<tr>';
                            htmlString += '<td>' + key + '</td>';
                            htmlString += '<td>' + simplePrevProfile[key] + '</td>';
                            htmlString += '<td>' + simpleCurProfile[key] + '</td>';
                            htmlString += '</tr>';
                        }

                        htmlString += '</table>';

                        htmlString += '</div>';

                        //#endregion
                    } else if (objLog.LogType == 'ChangeStatus') {

                            //#region ChangeStatus

                            htmlString += '<strong>' + objLog.UserDisplayName + '</strong> changed status for this patient: <br/>';
                            htmlString += '<ul>';
                            if (objDetails.StatusPrev != '1') {
                                htmlString += '<li>Previous status: ' + scope.getStatusName(objDetails.StatusPrev) + '</li>';
                            }

                            htmlString += '<li>New status: ' + scope.getStatusName(objDetails.StatusCurrent) + '</li>';

                            if (objDetails.StatusCurrent == '3' || objDetails.StatusCurrent == '4') {
                                htmlString += '<li>Send to ' + scope.getProviderName(objLog.ProviderID) + ' result: ' + objDetails.ResponseResult.PostStatus + '</li>';
                            }

                            htmlString += '</ul>';

                            if (objDetails.StatusCurrent == '3' || objDetails.StatusCurrent == '4') {
                                var simpleProfile = scope.simplifyProfileForLog(objDetails.ProfileCurrent, objLog.LogType);

                                htmlString += '<div class="toggle-ctn">';
                                htmlString += '<div class="show-details">Show/hide details</div>';

                                htmlString += '<table class="profile-table toggle-item">';

                                htmlString += '<tr>';
                                htmlString += '<th colspan="2">Data sent to ' + scope.getProviderName(objLog.ProviderID) + ' API</th>';
                                htmlString += '</tr>';

                                for (var key in simpleProfile) {
                                    htmlString += '<tr>';
                                    htmlString += '<td>' + key + '</td>';
                                    htmlString += '<td>' + simpleProfile[key] + '</td>';
                                    htmlString += '</tr>';
                                }
                                htmlString += '</table>';

                                htmlString += '</div>';
                            }

                            //#endregion
                        } else if (objLog.LogType == 'DeleteProfile') {

                                //#region DeleteProfile

                                //#endregion

                            }

        return htmlString;
    };
});

//#endregion


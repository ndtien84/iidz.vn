﻿'use strict';

(function ($) {
    $.ndtLightbox = function (element, options, $angularCompiler, $scope) {
        var defaults = {
            skin: 'indigo',
            showHeader: true,

            allowFullScreen: false,

            mobileWidth: 700,
            isMobile: false,
            ctHeight: 0,

            title: '',
            headerContent: '',
            content: '',
            outerClass: '',

            fixWidth: '',
            fixHeight: '',
            fixWidthType: 'auto',
            fixHeightType: 'auto',
            fixWidthInt: 0,
            fixHeightInt: 0,

            withTab: false,
            tabIndex: 0,

            withOverlay: true,
            overlayClickClose: false,
            dragable: false,

            onInit: function onInit() {},
            onComplete: function onComplete() {},
            onTabSelect: function onTabSelect(event, ui) {},
            onSetContent: function onSetContent() {},
            onDestroy: function onDestroy() {}
        };

        var p = this;
        p.s = {};
        var $element = $(element);
        var element = element;

        p.init = function () {
            p.s = $.extend({}, defaults, options);
            checkMobile();

            var lbClass = '';
            if (p.s.fixWidth == 0) lbClass += 'responsive-width';
            if (p.s.fixHeight == 0) lbClass += ' responsive-height';

            if (!p.s.showHeader) lbClass += ' no-header';
            if (p.s.withTab) lbClass += ' with-tab';

            p.s.onInit();

            // load overlay & loadgif
            if (p.s.withOverlay) {
                $('body').addClass('my-fix');
                $('<div class="ndt-gb-overlay"></div>').css('opacity', 0).animate({ 'opacity': '0.8' }, 200).appendTo($element);
            }

            // load empty lightbox
            var Template = '<div class="ndt-lb-ctn ' + p.s.skin + ' ' + lbClass + ' ' + p.s.outerClass + '">';
            if (p.s.showHeader) {
                Template += '<div class="ndt-lb-header">';
                Template += '<div class="ndt-lb-close gb-icon"></div>';
                Template += '<div class="ndt-lb-title">' + p.s.title + '</div>';

                if (p.s.allowFullScreen) Template += '<div class="btn-fullscreen"></div>';

                Template += '<div class="ndt-lb-other-ctn">' + p.s.headerContent + '</div>';

                Template += '<div style="height:0px;clear:both;"></div>';
                Template += '</div>';
            }
            Template += '<div class="ndt-lb-ct ndt-ov-tabs loading">' + p.s.content + '</div>';
            Template += '</div>';

            if ($angularCompiler && $scope) {
                appendLightbox(Template);
            } else {
                $(Template).appendTo($element);
            }

            if (p.s.withTab) {
                $('.ndt-lb-tabs').tabs({
                    /*
                    select: function (event, ui) {
                        p.s.onTabSelect(event, ui);
                    }
                    */
                });
                //$(".ndt-lb-tabs").tabs("select", p.s.tabIndex);
            } else {}
            //$('.ndt-lb-ctn .ndt-lb-ct').height(p.s.ctHeight);

            //#region Calculate Lightbox Width & Height

            p.s.fixWidth = p.s.fixWidth.toString();
            p.s.fixHeight = p.s.fixHeight.toString();

            if (p.s.fixWidth.indexOf('%') > 0) p.s.fixWidthType = 'percent';else if (p.s.fixWidth != '' || p.s.fixWidth.indexOf('px') > 0) p.s.fixWidthType = 'pixel';

            if (p.s.fixHeight.indexOf('%') > 0) p.s.fixHeightType = 'percent';else if (p.s.fixHeight != '' || p.s.fixHeight.indexOf('px') > 0) p.s.fixHeightType = 'pixel';

            p.s.fixWidthInt = parseInt(p.s.fixWidth);
            p.s.fixHeightInt = parseInt(p.s.fixHeight);

            if (p.s.fixWidth != '') $element.find('.ndt-lb-ctn').width(p.s.fixWidth);
            if (p.s.fixHeight != '') $element.find('.ndt-lb-ctn').height(p.s.fixHeight);

            //#endregion

            calPos();

            if (p.s.dragable) {
                $element.find('.ndt-lb-ctn').draggable({ handle: ".ndt-lb-header" });
                $element.find('.ndt-lb-ctn .ndt-lb-header').css({ 'cursor': 'move' });
            }

            p.s.onComplete();

            // button fullscreen
            if (p.s.allowFullScreen) {
                $element.find('.btn-fullscreen').bind('click', function () {
                    $element.find('.ndt-lb-ctn').toggleClass('full-screen');
                    $(this).toggleClass('is-full');
                });
            }

            // button close
            $element.find('.ndt-lb-close').bind('click', function () {
                p.close();
            });
            if (p.s.overlayClickClose) {
                $element.find('.ndt-gb-overlay').bind('click', function () {
                    p.close();
                });
            }

            // detect resize
            $(window).bind('resize.ndtLightbox', function () {
                checkMobile();
                calPos();
            });

            if (p.s.content != '') $element.find('.ndt-lb-ct').removeClass('loading');
        };

        var appendContent = function appendContent(title, headerContent, content) {
            var tpl = $angularCompiler(content)($scope);

            if (title) $element.find('.ndt-lb-title').html(title);
            if (headerContent) $element.find('.ndt-lb-other-ctn').html(headerContent);

            if (content) {
                $element.find('.ndt-lb-ct').html(tpl);
            }
        };

        var appendLightbox = function appendLightbox(template) {
            var tpl = $angularCompiler(template)($scope);
            $element.append(tpl);
        };

        //#region private method

        var checkMobile = function checkMobile() {
            p.s.isMobile = $(window).width() <= p.s.mobileWidth ? true : false;
        };

        var calPos = function calPos() {
            if (p.s.isMobile) return;

            var left = '';
            var top = '';

            if (p.s.fixWidthType == 'percent') {
                left = (100 - p.s.fixWidthInt) / 2;
            } else if (p.s.fixWidthType == 'pixel') {
                left = ($(window).width() - p.s.fixWidthInt) / 2;
            }

            if (p.s.fixHeightType == 'percent') {
                top = (100 - p.s.fixHeightInt) / 2;
            } else if (p.s.fixHeightType == 'pixel') {
                top = ($(window).height() - p.s.fixHeightInt) / 2;
            }

            if (p.s.fixWidthType == 'percent') left = left.toString() + '%';else if (p.s.fixWidthType == 'pixel') {
                if (p.s.fixWidthInt >= $(window).width()) {
                    // if the width is larger than browser width, the lightbox content width will set to 100% (fit browser width)
                    $element.find('.ndt-lb-ctn').width('100%');
                    left = '0';
                } else {
                    $element.find('.ndt-lb-ctn').width(p.s.fixWidth);
                    left = left.toString() + 'px';
                }
            }

            if (p.s.fixHeightType == 'percent') top = top.toString() + '%';else if (p.s.fixHeightType == 'pixel') {
                if (p.s.fixHeightInt >= $(window).height()) {
                    // if the height is larger than browser height, the lightbox content height will set to 100% (fit browser height)
                    $element.find('.ndt-lb-ctn').height('100%');
                    top = '0';
                } else {
                    $element.find('.ndt-lb-ctn').height(p.s.fixHeight);
                    top = top.toString() + 'px';
                }
            }

            if (top != '') $element.find('.ndt-lb-ctn').css('top', top);
            if (left != '') $element.find('.ndt-lb-ctn').css('left', left);
        };

        //#endregion

        //#region public method

        p.close = function () {
            p.s.onDestroy();

            $element.find('.ndt-lb-close, .ndt-gb-overlay, .lb-tabs-ct.conversation').unbind('click');
            $(window).unbind('resize.ndtLightbox');
            $element.removeData('ndtLightbox');
            $element.remove();

            if ($('.ndt-lb-ctn').length <= 0) $('body').removeClass('my-fix');
        };

        p.setContent = function (title, headerContent, content) {
            if ($angularCompiler && $scope) {
                appendContent(title, headerContent, content);
            } else {
                if (title) $element.find('.ndt-lb-title').html(title);
                if (headerContent) $element.find('.ndt-lb-other-ctn').html(headerContent);
                if (content) $element.find('.ndt-lb-ct').html(content);
            }

            $element.find('.ndt-lb-ct').removeClass('loading');

            if (p.s.withTab) {
                //$('.ndt-lb-ctn .lb-tabs-ct').height(p.s.ctHeight);
                $('.ndt-lb-tabs').tabs({
                    select: function select(event, ui) {
                        p.s.onTabSelect(event, ui);
                    }
                });
                $(".ndt-lb-tabs").tabs("select", p.s.tabIndex);
            }

            p.s.onSetContent();
        };

        //#endregion

        p.init();
    };

    $.fn.ndtLightbox = function (options, $angularCompiler, $scope) {
        if ($('.ndt-lb-super-ctn-' + options.outerClass).length <= 0) {
            var $ctn = $('<div class="ndt-lb-super-ctn-' + options.outerClass + '"></div>').appendTo($('body'));
            return $ctn.each(function () {
                var plugin = new $.ndtLightbox($ctn, options, $angularCompiler, $scope);
                $ctn.data('ndtLightbox', plugin);
            });
        }
    };
})(jQuery);


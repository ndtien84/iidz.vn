﻿'use strict';

ndtApp.controller('asCtrl', ['$scope', '$http', '$compile', 'workWithList', function ($scope, $http, $compile, workWithList) {
    $scope.ctn = $('.advanced-settings-ctn');
    $scope.isHost = false;
    $scope.objLB;
    $scope.isSending = false;

    $scope.lstOrderStatus = [];
    $scope.newOrderStatus = { StatusID: '-1', Priority: 0 };

    //#region Init & Utilities

    $scope.init = function () {
        $('.admin-header-ctn .page-title').html('Advanced Settings');

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'load-init'
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.lstOrderStatus = ctJson.lstOrderStatus;
                $scope.isHost = ctJson.isHost;
            }
        });
    };

    //#endregion

    //#region Update Order Status

    $scope.updateOrderStatus = function (objStatus) {
        if ($scope.isSending) return;

        var itemCtn = $('.order-status-ctn .list-group-item[data-id="' + objStatus.StatusID + '"]');
        var valMsg = appHelpers.validate(itemCtn);
        if (valMsg != 'passed') {
            appHelpers.alertCall(valMsg, 6000, 'alert-danger');
            return;
        }

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'update-order-status',
                data: JSON.stringify(objStatus)
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();
            $scope.newOrderStatus = { StatusID: '-1', Priority: 0 };

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                var newObj = ctJson.newObj;
                workWithList.AddObjectToList($scope.lstOrderStatus, newObj, 'StatusID');
                appHelpers.alertCall('Order status updated', 6000);
            }
        });
    };

    //#endregion

    //#region Delete Order Status

    $scope.deleteOrderStatus = function (statusID) {
        if ($scope.isSending) return;

        if (!confirm('Delete this order status ?')) return;

        $scope.isSending = true;
        appHelpers.callOverlayLoading();

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'delete-order-status',
                data: statusID
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                workWithList.removeObjectByID($scope.lstOrderStatus, statusID, 'StatusID');
                appHelpers.alertCall('Order status deleted', 6000);
            }
        });
    };

    //#endregion

    // init the control
    $scope.init();
}]);


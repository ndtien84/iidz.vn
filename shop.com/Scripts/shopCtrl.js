﻿'use strict';

ndtApp.filter('cartQuantity', function () {
    return function (lstCart) {
        var returnValue = '0';

        if (lstCart && lstCart.length > 0) {
            var qty = 0;
            angular.forEach(lstCart, function (objCart) {
                if (objCart.Quantity != '' && !isNaN(objCart.Quantity)) qty += parseInt(objCart.Quantity);
            });

            returnValue = qty.toString();
        }

        return returnValue;
    };
});

ndtApp.filter('cartTotalPrice', function () {
    return function (lstCart) {
        var totalPrice = 0;

        if (lstCart && lstCart.length > 0) {
            angular.forEach(lstCart, function (objCart) {
                if (objCart.Quantity != '' && !isNaN(objCart.Quantity)) {
                    totalPrice += parseInt(objCart.Quantity) * objCart.CurrentPrice;
                }
            });
        }

        return totalPrice;
    };
});

ndtApp.controller('shopCtrl', ['$scope', '$http', '$compile', '$filter', 'workWithList', 'dateHelpers', function ($scope, $http, $compile, $filter, workWithList, dateHelpers) {
    $scope.ctn = $('body');
    $scope.objLB;

    $scope.userProfile = {};

    $scope.tplUserOrderDetails = '';
    $scope.lstOrder = [];
    $scope.curOrderDetails = [];
    $scope.orderDetailsTotal = {};

    $scope.lstCart = [];
    $scope.isSending = false;

    $scope.checkoutForm = { Name: '', CellPhone: '', Email: '', Overview: '' };

    $scope.addQty = 1;

    //#region Init & Utilities

    $scope.init = function () {
        $http({
            method: 'POST',
            url: '/cart-ctrl',
            data: {
                ccAct: 'load-cart-temp'
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) alert(ctJson.error);else {
                $scope.lstCart = ctJson.lstCart;
                $scope.userProfile = ctJson.userProfile;

                //console.log(angular.toJson($scope.userProfile, true));

                $scope.checkoutForm.Email = angular.copy($scope.userProfile.Email);
                $scope.checkoutForm.Name = angular.copy($scope.userProfile.DisplayName);
                $scope.checkoutForm.CellPhone = angular.copy($scope.userProfile.CellPhone);

                $('.header-ctn .checkout-link .count').show();
            }
        });
    };

    $scope.changeQty = function (dType) {
        if (isNaN($scope.addQty)) $scope.addQty = 1;

        if (dType == 'minus') {
            $scope.addQty--;
            if ($scope.addQty <= 1) $scope.addQty = 1;
        } else $scope.addQty++;
    };

    //#endregion

    //#region Load List Order

    $scope.listOrderInit = function () {
        $http({
            method: 'POST',
            url: '/cart-ctrl',
            data: {
                ccAct: 'list-order-init'
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) alert(ctJson.error);else {
                $scope.lstOrder = ctJson.lstOrder;
                $scope.tplUserOrderDetails = ctJson.tplUserOrderDetails;
            }
        });
    };

    //#endregion

    //#region View Order Details

    $scope.viewOrderDetail = function (orderID) {
        $scope.orderDetailsTotal = { Quantity: 0, TotalPrice: 0 };
        $scope.curOrderDetails = workWithList.getObjectByID($scope.lstOrder, orderID, 'OrderID').ListDetails;

        angular.forEach($scope.curOrderDetails, function (details) {
            $scope.orderDetailsTotal.Quantity += details.Quantity;
            $scope.orderDetailsTotal.TotalPrice += details.Price * details.Quantity;
        });

        $scope.objLB = $('body').ndtLightbox({
            title: 'Danh mục',
            //fixWidth: 600,
            //fixHeight: 500,
            bindMaterial: false,
            showHeader: false,
            outerClass: 'form-user-order-details',
            overlayClickClose: true,
            content: $scope.tplUserOrderDetails
        }, $compile, $scope);
    };

    //#endregion

    //#region Add To Cart

    $scope.addToCart = function (productID, quantity) {
        if ($scope.isSending) return;

        if (quantity.toString().trim() == '' || isNaN(quantity) || quantity <= 0) quantity = 1;

        $scope.isSending = true;
        $http({
            method: 'POST',
            url: '/cart-ctrl',
            data: {
                ccAct: 'add-to-cart',
                data: JSON.stringify({ ProductID: productID, Quantity: quantity })
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 10000, 'alert-shop');else {
                var newItem = ctJson.newItem;
                workWithList.AddObjectToList($scope.lstCart, newItem, 'CartTempID');
                appHelpers.alertCall('Đã thêm sản phẩm vào giỏ hàng', 5000, 'alert-shop');
            }
            $scope.isSending = false;
        });
    };

    //#endregion

    //#region Update Cart Quantity

    $scope.updateCart = function (cartID) {
        if ($scope.isSending) return;

        for (var i = 0; i < $scope.lstCart.length; i++) {
            var qty = $scope.lstCart[i].Quantity;
            if (qty.toString().trim() == '' || isNaN(qty) || qty <= 0) {
                $scope.lstCart[i].Quantity = 1;
            }
        }

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: 'POST',
            url: '/cart-ctrl',
            data: {
                ccAct: 'update-cart',
                data: JSON.stringify($scope.lstCart)
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) alert(ctJson.error);else {
                appHelpers.alertCall('Cart updated', 5000, 'alert-shop');
            }
        });
    };

    //#endregion

    //#region remove Cart

    $scope.removeCart = function (cartID) {
        if ($scope.isSending) return;

        $.cxDialog({
            title: 'Xóa sản phẩm khỏi giỏ hàng ?',
            //info: 'Xóa sản phẩm khỏi giỏ hàng ?',
            ok: function ok() {
                $scope.isSending = true;
                $http({
                    method: 'POST',
                    url: '/cart-ctrl',
                    data: {
                        ccAct: 'remove-cart',
                        data: cartID
                    }
                }).success(function (result) {
                    var ctJson = angular.fromJson(result);
                    if (ctJson.error) alert(ctJson.error);else {
                        workWithList.removeObjectByID($scope.lstCart, cartID, 'CartTempID');
                    }
                    $scope.isSending = false;
                });
            },
            okText: 'Yes',
            no: function no() {},
            noText: 'No',
            baseClass: 'ios'
        });
    };

    //#endregion

    //#region Complete Cart

    $scope.completeCart = function () {
        if ($scope.isSending) return;

        var formCtn = $('.check-out-ctn .checkout-form');
        var valDom = $('.check-out-ctn .checkout-form .validate-msg');

        var valMesg = appHelpers.validate(formCtn);
        if (valMesg != 'passed') {
            valDom.html(valMesg);
            return;
        }

        var lstSubmit = [];
        angular.forEach($scope.lstCart, function (objCart) {
            if (objCart.Quantity.toString().trim() != '' && !isNaN(objCart.Quantity) && objCart.Quantity > 0) {
                lstSubmit.push(objCart);
            }
        });

        //console.log(angular.toJson(lstSubmit, true));
        //return;

        valDom.html('').addClass('loading');
        $scope.isSending = true;
        $http({
            method: 'POST',
            url: '/cart-ctrl',
            data: {
                ccAct: 'complete-cart',
                data: JSON.stringify(lstSubmit),
                extdata: JSON.stringify($scope.checkoutForm)
            }
        }).success(function (result) {
            valDom.removeClass('loading');

            var ctJson = angular.fromJson(result);
            if (ctJson.error) alert(ctJson.error);else {
                formCtn.find('.textbox').val('');
                $scope.lstCart = [];
                $.cxDialog({
                    title: 'Gửi đơn hàng thành công',
                    info: 'Cám ơn bạn đã đặt hàng ở website myshop.vn<br/>Chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất',
                    okText: 'OK',
                    ok: function ok() {
                        window.location.href = 'http://' + window.location.host;
                    },
                    baseClass: 'ios'
                });
            }
            $scope.isSending = false;
        });
    };

    //#endregion

    $scope.init();
}]);


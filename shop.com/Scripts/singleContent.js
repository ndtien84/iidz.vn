﻿'use strict';

ndtApp.controller('scCtrl', ['$scope', '$http', '$compile', '$filter', 'workWithList', 'appSettings', 'dateHelpers', function ($scope, $http, $compile, $filter, workWithList, appSettings, dateHelpers) {
    $scope.ctn = $('.single-content-ctn');

    $scope.objLBImage;
    $scope.ckeDetails;

    $scope.isSending = false;

    $scope.tplFormDetailsImage = '';
    $scope.curContent = {};

    $scope.lstExtendsImage = [];
    $scope.lstDetailsImage = [];

    $scope.contentType = '';

    $scope.settings = {
        updateContentSuccess: 'Content updated',
        uploadImagesSuccess: 'Extends images uploaded',
        requiredSelectImage: 'Please select images to upload',
        confirmDeleteImage: 'Are you sure to delete this image ?',
        deleteImageSuccess: 'Image deleted',

        thumbsSize: 500,
        detailsImage: false,
        extendsImage: false
    };

    $scope.i18n = shopText.singleContent.template;

    //#region Init & Utilities

    $scope.init = function () {
        if (appHelpers.queryString('type')) $scope.contentType = appHelpers.queryString("type");

        if ($scope.contentType == 'about-us') {
            $('.admin-header-ctn .page-title').html(shopText.singleContent.aboutTitle);
        } else if ($scope.contentType == 'test-single') {
            $scope.settings.detailsImage = false;
        }

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'load-init',
                data: $scope.contentType
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.curContent = ctJson.curContent;
                $scope.tplFormDetailsImage = ctJson.tplFormDetailsImage;

                setTimeout(function () {
                    $scope.loadCkEditor();
                }, 500);

                if ($scope.settings.extendsImage) $scope.loadListExtendsImages();
            }
        });
    };

    $scope.toggleContentImageForm = function () {
        if ($scope.objLB.find('.form-manage-content').hasClass('has-image')) {

            $scope.objLBImage.find('.form-content-details-image').fadeOut(300, function () {
                $scope.objLB.find('.form-manage-content').removeClass('has-image');
            });
        } else {
            $scope.objLB.find('.form-manage-content').addClass('has-image');
            setTimeout(function () {
                $scope.objLBImage.find('.form-content-details-image').show();
            }, 300);
        }
    };

    $scope.loadCkEditor = function () {
        if ($('#ckeDetailsHTML').length <= 0) return;

        var config = {
            baseFloatZIndex: 100002,
            enterMode: CKEDITOR.ENTER_BR,
            shiftEnterMode: CKEDITOR.ENTER_P
        };

        if ($scope.ckeDetails) {
            try {
                $scope.ckeDetails.destroy();
            } catch (ex) {}

            $scope.ckeDetails = null;
        }

        var content = $('#ckeDetailsHTML').text();

        $scope.ckeDetails = CKEDITOR.appendTo('ckeDetails', config, content);
    };

    //#endregion

    //#region Save Content

    $scope.saveContent = function () {
        if ($scope.isSending) return;

        var valMsg = appHelpers.validate($scope.ctn);
        if (valMsg != 'passed') {
            appHelpers.alertCall(valMsg, 5000, 'alert-danger');
            return;
        }

        if ($scope.ckeDetails) $scope.curContent.Details = $scope.ckeDetails.getData();

        $scope.curContent.ContentType = $scope.contentType;

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'update-content',
                data: JSON.stringify($scope.curContent)
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.curContent = ctJson.newContent;

                var objFile = $scope.ctn.find('.fulPrimaryImage');
                if (objFile.length > 0 && objFile.val() != '') {
                    $scope.uploadFiles(objFile, $scope.curContent.ContentID, 'PrimaryImage');
                }

                appHelpers.alertCall($scope.settings.updateContentSuccess, 7000);
            }
        });
    };

    //#endregion

    //#region Load List Extends Images

    $scope.loadListExtendsImages = function () {
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'load-extends-image',
                data: $scope.curContent.ContentID
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.lstExtendsImage = ctJson.lstExtendsImage;
            }
        });
    };

    //#endregion

    //#region Upload Extends Image

    $scope.uploadExtendsImages = function () {
        var fileSelect = $scope.ctn.find('.fulExtendsImages');

        if (fileSelect.val() == '') {
            appHelpers.alertCall($scope.settings.requiredSelectImage, 5000, 'alert-danger');
            return;
        }

        $scope.uploadFiles(fileSelect, $scope.curContent.ContentID, 'ContentImage');
    };

    //#endregion

    //#region Delete Extends Image

    $scope.deleteExtendsImages = function (contentID) {
        if (!confirm($scope.settings.confirmDeleteImage)) return;

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'delete-extends-image',
                data: contentID
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                appHelpers.alertCall($scope.settings.deleteImageSuccess, 5000);
                workWithList.removeObjectByID($scope.lstExtendsImage, contentID, 'ContentID');
            }
        });
    };

    //#endregion

    //#region Load Form Content Details Image

    $scope.loadFormDetailsImage = function () {
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'load-content-details-image',
                data: $scope.curContent.ContentID
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.lstDetailsImage = ctJson.lstDetailsImage;

                $scope.objLBImage = $('body').ndtLightbox({
                    title: 'Images',
                    fixWidth: 200,
                    //fixHeight: '50%',
                    dragable: true,
                    withOverlay: false,
                    allowFullScreen: true,
                    outerClass: 'well form-single-content-details-image',
                    content: $scope.tplFormDetailsImage,
                    onComplete: function onComplete() {}
                }, $compile, $scope);
            }
        });
    };

    //#endregion

    //#region Upload Content Details Image

    $scope.uploadContentDetailsImage = function () {
        var fileSelect = $scope.objLBImage.find('.fulContentDetailsImage');
        if (fileSelect.val() == '') {
            appHelpers.alertCall($scope.settings.requiredSelectImage, 5000, 'alert-danger');
            return;
        }

        $scope.uploadFiles(fileSelect, $scope.curContent.ContentID, 'ContentDetailsImage');
    };

    //#endregion

    //#region Delete Content Details Image

    $scope.deleteContentDetailsImage = function (contentID) {
        if (!confirm(shopText.mContent.confirmDeleteImage)) return;

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'delete-content-details-image',
                data: contentID
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                appHelpers.alertCall($scope.settings.deleteImageSuccess, 5000);
                workWithList.removeObjectByID($scope.lstDetailsImage, contentID, 'ContentID');
            }
        });
    };

    //#endregion

    //#region Upload Files

    // fileType: PrimaryImage | ContentImage | ContentDetailsImage
    $scope.uploadFiles = function (objSelectFile, contentID, fileType) {
        if ($scope.isSending) return;

        var objQuery = {
            contentID: contentID,
            fileType: fileType,
            thumbsSize: $scope.settings.thumbsSize
        };

        var objForm = new FormData();
        objForm.append('act', 'upload-files');
        objForm.append('data', JSON.stringify(objQuery));

        var listFiles = objSelectFile[0].files;

        if (listFiles.length > 0) {
            for (var i = 0; i < listFiles.length; i++) {
                objForm.append('file' + i, listFiles[i]);
            }
        }

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $.ajax({
            url: window.location.href,
            type: 'POST',
            contentType: false,
            processData: false,
            data: objForm,
            success: function success(result) {
                $scope.isSending = false;
                appHelpers.removeOverlayLoading();

                var ctJson = angular.fromJson(result);
                if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                    if (fileType == 'PrimaryImage') {
                        $scope.curContent.PrimaryImage = ctJson.imgName;
                    } else if (fileType == 'ContentImage') {
                        var lstUploaded = ctJson.lstImage;
                        angular.forEach(lstUploaded, function (objImg) {
                            workWithList.AddObjectToList($scope.lstExtendsImage, objImg, 'ContentID');
                        });
                        appHelpers.alertCall($scope.settings.uploadImagesSuccess, 7000);
                    } else if (fileType == 'ContentDetailsImage') {
                        var lstUploaded = ctJson.lstImage;
                        angular.forEach(lstUploaded, function (objImg) {
                            workWithList.AddObjectToList($scope.lstDetailsImage, objImg, 'ContentID');
                        });
                        appHelpers.alertCall($scope.settings.uploadImagesSuccess, 7000);
                    }

                    $scope.$apply();

                    objSelectFile.parents('.input-group').find('input[type="text"]').val('');
                    objSelectFile.val('');
                }
            }
        });
    };

    //#endregion

    $scope.init();
}]);


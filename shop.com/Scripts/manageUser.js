﻿'use strict';

ndtApp.controller('userCtrl', ['$scope', '$http', '$compile', 'workWithList', function ($scope, $http, $compile, workWithList) {
    $scope.ctn = $('.manage-user-ctn'); // jquery object, must have to use the ndtLightbox plugins

    $scope.lstUser = [];
    $scope.lstRole = [];

    $scope.objFilter = {
        type: 'all', // all | search
        colName: 'Email',
        keyword: '',
        pageSize: 15,
        pageIndex: 1
    };

    $scope.totalPage = 1;
    $scope.totalRow = 0;

    $scope.i18n = shopText.manageUser.template;

    //#region Init & Utilities

    $scope.init = function () {
        if (appHelpers.queryHash('filtype')) {
            $scope.objFilter.type = appHelpers.queryHash('filtype');
        }
        if (appHelpers.queryHash('col')) {
            $scope.objFilter.colName = appHelpers.queryHash('col');
        }
        if (appHelpers.queryHash('keyword')) {
            $scope.objFilter.keyword = decodeURIComponent(appHelpers.queryHash('keyword'));
        }
        if (appHelpers.queryHash('pg')) {
            $scope.objFilter.pageIndex = appHelpers.queryHash('pg');
        }

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'load-init',
                data: $scope.curPage
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.lstRole = ctJson.lstRole;
                //$scope.lstUser = ctJson.lstUser;
                //$scope.totalPage = ctJson.totalPage;

                $scope.getListUser(null, $scope.objFilter.pageIndex);
            }
        });
    };

    $scope.checkKeywordEnter = function (e) {
        var evt = e || window.event;
        if (evt.keyCode === 13) {
            $scope.getListUser('search', 1);
            return false;
        }
    };

    $scope.getListUser = function (getType, pageIndex) {
        if ($scope.isSending) return;

        if (getType) $scope.objFilter.type = getType;
        if (pageIndex) $scope.objFilter.pageIndex = pageIndex;

        var hashString = '';
        if ($scope.objFilter.type == 'all') {
            hashString = '#filtype=' + $scope.objFilter.type + '&pg=' + $scope.objFilter.pageIndex;
        } else if ($scope.objFilter.type == 'search') {
            hashString = '#filtype=' + $scope.objFilter.type + '&col=' + $scope.objFilter.colName + '&pg=' + $scope.objFilter.pageIndex + '&keyword=' + encodeURIComponent($scope.objFilter.keyword);
            if ($scope.objFilter.keyword.trim() == '') {
                appHelpers.alertCall(shopText.manageUser.requireKeyword, 7000, 'alert-danger');
                $scope.ctn.find('.txt-keyword').focus();
                return;
            }
        }

        window.location.href = window.location.pathname + '?type=users' + hashString;

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: "POST",
            url: window.location.href,
            data: {
                act: 'get-user',
                data: JSON.stringify($scope.objFilter)
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 10000, 'alert-danger');else {
                $scope.lstUser = ctJson.lstUser;
                $scope.totalPage = ctJson.totalPage;
                $scope.totalRow = ctJson.totalRow;
            }
        });
    };

    //#endregion

    //#region Update User

    $scope.updateUser = function (userID) {
        var item = $scope.ctn.find('.list-group-item[data-id="' + userID + '"]');
        var listRole = '';

        item.find('input:checkbox').each(function () {
            if ($(this).is(':checked')) listRole += $(this).val() + '|';
        });

        if (listRole.length > 1) listRole = listRole.substring(0, listRole.length - 1);

        var objUser = {
            userid: userID,
            listRole: listRole,
            newPass: item.find('.txt-newpassword').val()
        };

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'update-user',
                data: JSON.stringify(objUser)
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) console.log(ctJson.error);else {
                appHelpers.alertCall(shopText.manageUser.updateSuccess, 5000);

                // update list user json
                //var objUser = workWithList.getObjectByID($scope.lstUser, username, "UserName");
                //objUser.ListRole = listRole.split('|');
                //workWithList.AddObjectToList($scope.lstUser, objUser, "UserName");

                item.find('.txt-newpassword').val('');
                $scope.bindMaterial();
            }
        });
    },

    //#endregion

    //#region Delete User

    $scope.deleteUser = function (userID) {
        console.log(shopText.confirmDelete);

        if (!confirm(shopText.manageUser.confirmDelete)) return;

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'delete-user',
                data: userID
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) console.log(ctJson.error);else {
                appHelpers.alertCall(shopText.manageUser.deleteComplete, 5000);
                workWithList.removeObjectByID($scope.lstUser, userID, "Id");
            }
        });
    };

    //#endregion

    //#region Utilities

    $scope.roleCheck = function (roleID, userID) {
        var listRoleByUser = workWithList.getObjectByID($scope.lstUser, userID, "Id").ListRoles;
        var isHas = false;

        angular.forEach(listRoleByUser, function (objRole) {
            if (!isHas) {
                if (objRole.Id == roleID) {
                    isHas = true;
                }
            }
        });

        return isHas;
    };

    $scope.getPageArray = function () {
        return new Array($scope.totalPage);
    };

    //#endregion

    // init the control
    $scope.init();
}]);


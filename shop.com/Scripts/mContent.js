﻿'use strict';

ndtApp.controller('mctCtrl', ['$scope', '$http', '$compile', '$filter', 'workWithList', 'appSettings', 'dateHelpers', function ($scope, $http, $compile, $filter, workWithList, appSettings, dateHelpers) {
    $scope.ctn = $('.manage-content-ctn');
    //$scope.mediaLink = appSettings.mediaLink();
    $scope.objLB;
    $scope.objLBContentImage;
    $scope.objLBImage;
    $scope.ckeDetails;

    $scope.isSending = false;

    $scope.tplFormContent = '';
    $scope.tplFormImage = '';
    $scope.tplFormDetailsImage = '';

    $scope.lstCategory = [];

    $scope.lstContent = [];
    $scope.curContent = {};

    $scope.curLstImg = [];
    $scope.curLstContentImage = [];
    $scope.genContentID = '-1';

    $scope.totalPage = 0;
    $scope.totalRow = 0;

    $scope.objFilter = {
        type: '', // news | project | ...
        filterType: 'bycategory', // bycategory | search
        categoryID: 'all',
        contentID: '-1',
        keyword: '',
        keywordColumn: 'Name',
        orderColumn: 'Priority',
        orderDirection: 'desc', // desc | asc
        pageSize: 12,
        pageIndex: 1
    };

    $scope.settings = {
        showCategory: false,
        detailsImage: false,
        contentImage: false,
        showSearch: true,
        maxItem: 1000,
        thumbsSize: 500,
        labelSearchBy: 'Tìm theo tên',
        labelAllContents: 'Tất cả tin tức'
    };

    $scope.i18n = shopText.mContent.template;

    //#region Init & Utilities

    $scope.init = function () {
        if (appHelpers.queryString('type')) $scope.objFilter.type = appHelpers.queryString('type');

        if (appHelpers.queryHash('filtype')) $scope.objFilter.filterType = appHelpers.queryHash('filtype');
        if (appHelpers.queryHash('keyword')) $scope.objFilter.keyword = decodeURIComponent(appHelpers.queryHash('keyword'));
        if (appHelpers.queryHash('ctid')) $scope.objFilter.contentID = appHelpers.queryHash('ctid');
        if (appHelpers.queryHash('pg')) $scope.objFilter.pageIndex = appHelpers.queryHash('pg');
        if (appHelpers.queryHash('cat')) $scope.objFilter.categoryID = appHelpers.queryHash('cat');

        if ($scope.objFilter.type == 'news') {
            $scope.objFilter.orderColumn = "UpdateDate";

            $scope.settings.showCategory = true;
            $scope.settings.detailsImage = true;
            $scope.settings.contentImage = true;
            $('.admin-header-ctn .page-title').html(shopText.mContent.newsTitle);
        } else if ($scope.objFilter.type == 'service') {
            $('.admin-header-ctn .page-title').html('Quản trị dịch vụ');
            $scope.settings.labelAllContents = 'Tất cả dịch vụ';
        } else if ($scope.objFilter.type == 'project') {
            $('.admin-header-ctn .page-title').html('Quản trị dự án');
            $scope.settings.labelAllContents = 'Tất cả dự án';
        }

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'load-init',
                data: $scope.objFilter.type
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.tplFormImage = ctJson.tplFormImage;
                $scope.tplFormDetailsImage = ctJson.tplFormDetailsImage;
                $scope.tplFormContent = ctJson.tplFormContent;
                $scope.lstCategory = ctJson.lstCategory;
                $scope.genContentID = ctJson.genContentID;

                if ($scope.settings.showCategory) {
                    setTimeout(function () {
                        $scope.ctn.find('.my-dropdown').dropdown();
                    }, 500);
                }

                $scope.loadListContent(null, $scope.objFilter.pageIndex, $scope.objFilter.categoryID);
            }
        });
    };

    // formType: extends | details
    $scope.toggleContentImageForm = function (formType) {
        var subLBClass = formType == 'extends' ? '.form-content-image' : '.form-content-details-image';

        if ($('body').find(subLBClass).is(':visible')) {
            $('body').find(subLBClass).fadeOut(1);
        } else {
            setTimeout(function () {
                $('body').find(subLBClass).show();
            }, 300);
        }

        $scope.objLB.find('.form-manage-content').toggleClass('has-image-' + formType);
    };

    $scope.getPageArray = function () {
        return new Array($scope.totalPage);
    };

    $scope.checkKeywordEnter = function (e) {
        var evt = e || window.event;
        if (evt.keyCode === 13) {
            $scope.loadListContent('search', 1, null);
            return false;
        }
    };

    $scope.loadCkEditor = function () {
        var config = {
            baseFloatZIndex: 100002,
            enterMode: CKEDITOR.ENTER_BR,
            shiftEnterMode: CKEDITOR.ENTER_P
        };

        if ($scope.ckeDetails) {
            try {
                $scope.ckeDetails.destroy();
            } catch (ex) {}

            $scope.ckeDetails = null;
        }

        var content = $('#ckeDetailsHTML').text();

        $scope.ckeDetails = CKEDITOR.appendTo('ckeDetails', config, content);
    };

    //#endregion

    //#region Load List Content

    $scope.loadListContent = function (getType, pageIndex, categoryID) {
        //window.location.href = window.location.pathname + '?type=' + $scope.objFilter.type + '#cat=' + categoryID + '&pg=' + pageIndex;

        if ($scope.isSending) return;

        if (getType) $scope.objFilter.filterType = getType;
        if (pageIndex) $scope.objFilter.pageIndex = pageIndex;
        if (categoryID) $scope.objFilter.categoryID = categoryID;

        var hashString = '';
        if ($scope.objFilter.filterType == 'bycategory') {
            hashString = '#filtype=' + $scope.objFilter.filterType + '&cat=' + $scope.objFilter.categoryID + '&pg=' + $scope.objFilter.pageIndex;
        } else if ($scope.objFilter.filterType == 'search') {
            if ($scope.objFilter.keyword == '') {
                appHelpers.alertCall(shopText.mContent.requiredKeyword, 7000, 'alert-danger');
                $scope.ctn.find('.txt-keyword').focus();
                return;
            }
            hashString = '#filtype=' + $scope.objFilter.filterType + '&pg=' + $scope.objFilter.pageIndex + '&keyword=' + encodeURIComponent($scope.objFilter.keyword);
        }

        window.location.href = window.location.pathname + '?type=' + $scope.objFilter.type + hashString;

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'load-list-content',
                data: JSON.stringify($scope.objFilter)
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.lstContent = ctJson.lstContent;
                $scope.totalPage = ctJson.totalPage;
                $scope.totalRow = ctJson.totalRow;
                //$scope.objFilter.pageIndex = pageIndex;
                //$scope.objFilter.categoryID = categoryID;

                if ($scope.objFilter.contentID != '-1') $scope.loadFormContent($scope.objFilter.contentID);

                // scroll top
                $('body,html').animate({ scrollTop: 0 }, 100);
            }
        });
    };

    //#endregion

    //#region Set Primary Image

    $scope.setPrimaryImage = function (imageID, imageName) {
        if ($scope.isSending) return;

        var objData = {
            imageID: imageID,
            imageName: imageName,
            contentID: $scope.curContent.ContentID,
            contentImage: $scope.curContent.PrimaryImage
        };

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'set-primary-image',
                data: JSON.stringify(objData)
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                var prevCurContent = angular.copy($scope.curContent);

                if (prevCurContent.PrimaryImage != 'default.png') {
                    var objImage = workWithList.getObjectByID($scope.curLstImg, imageID, 'ContentID');
                    if (objImage != null) {
                        objImage.PrimaryImage = prevCurContent.PrimaryImage;
                        workWithList.AddObjectToList($scope.curLstImg, objImage, 'ContentID');
                    } else {
                        objImage = workWithList.getObjectByID($scope.curLstContentImage, imageID, 'ContentID');
                        objImage.PrimaryImage = prevCurContent.PrimaryImage;
                        workWithList.AddObjectToList($scope.curLstContentImage, objImage, 'ContentID');
                    }
                } else {
                    workWithList.removeObjectByID($scope.curLstImg, imageID, 'ContentID');
                    workWithList.removeObjectByID($scope.curLstContentImage, imageID, 'ContentID');
                }

                $scope.curContent.PrimaryImage = imageName;
                workWithList.AddObjectToList($scope.lstContent, $scope.curContent, 'ContentID');
            }
        });
    };

    //#endregion

    //#region Re-create All Thumbs

    $scope.recreateThumbs = function () {
        if ($scope.isSending) return;

        if (!confirm('Recreate all thumbs ?')) return;

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'recreate-thumbs',
                data: JSON.stringify({ type: $scope.objFilter.type, thumbsSize: $scope.settings.thumbsSize })
            }
        }).success(function (result) {
            $scope.isSending = true;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                appHelpers.alertCall(ctJson.message, 60000);
            }
        });
    };

    //#endregion

    //#region Load Form Content

    $scope.loadFormContent = function (contentID) {
        if (contentID != '-1') {
            $scope.curContent = workWithList.getObjectByID($scope.lstContent, contentID, 'ContentID');

            appHelpers.setHash('ctid', contentID);
        } else {
            var curCat = $scope.objFilter.categoryID == 'all' ? '-1' : $scope.objFilter.categoryID;
            $scope.curContent = { ContentID: '-1', CategoryID: curCat, ContentType: $scope.objFilter.type, Priority: 0 };
        }

        // #region Header For Desktop & Mobile

        // desktop
        var headerString = '<span class="btn btn-link toggle-content-image hidden-xs" ng-show="settings.contentImage" ng-click="toggleContentImageForm(\'extends\')">' + shopText.mContent.btnExtendsImage + '</span>';
        headerString += '<span class="btn btn-link toggle-content-image hidden-xs" ng-show="settings.detailsImage" ng-click="toggleContentImageForm(\'details\')">' + shopText.mContent.btnDetailsImage + '</span>';
        headerString += '<span class="btn btn-link btn-save hidden-xs" ng-click="saveFormContent()">' + shopText.global.btnSave + '</span>';

        // mobile
        headerString += '<span class="btn-btn-link dropdown settings-icon visible-xs">';
        headerString += '<a class="dropdown-toggle" data-toggle="dropdown" href="bootstrap-elements.html" data-target="#"></a>';
        headerString += '<ul class="dropdown-menu">';
        headerString += '<li ng-click="saveFormContent()"><a>' + shopText.global.btnSave + '</a></li>';
        headerString += '<li ng-show="settings.contentImage" ng-click="toggleContentImageForm(\'extends\')"><a>' + shopText.mContent.btnExtendsImage + '</a></li>';
        headerString += '<li ng-show="settings.detailsImage" ng-click="toggleContentImageForm(\'details\')"><a>' + shopText.mContent.btnDetailsImage + '</a></li>';
        headerString += '</ul>';
        headerString += '</span>';

        //#endregion

        $scope.objLB = $('body').ndtLightbox({
            title: shopText.mContent.lightboxTitle,
            allowFullScreen: true,
            headerContent: headerString,
            //fixWidth: '70%',
            //fixHeight: '600',
            outerClass: 'form-manage-content',
            content: $scope.tplFormContent,
            onComplete: function onComplete() {
                setTimeout(function () {
                    $scope.loadCkEditor();
                    if ($scope.settings.showCategory) {
                        $scope.objLB.find('.ndt-lb-ctn .my-dropdown').dropdown();
                    }
                }, 500);
            },
            onSetContent: function onSetContent() {
                if ($scope.settings.showCategory) {
                    setTimeout(function () {
                        $scope.objLB.find('.ndt-lb-ctn .my-dropdown').dropdown();
                    }, 500);
                }
            },
            onDestroy: function onDestroy() {
                $scope.objLBImage.data('ndtLightbox').close();
                $scope.objLBContentImage.data('ndtLightbox').close();

                appHelpers.removeHash('ctid');
                $scope.objFilter.contentID = '-1';
            }
        }, $compile, $scope);

        if (contentID == '-1') contentID = $scope.genContentID;

        $scope.loadFormDetailsImage(contentID);
        $scope.loadFormContentImage(contentID);
    };

    //#endregion

    //#region Save Form Content

    $scope.saveFormContent = function () {
        if ($scope.isSending) return;

        var valMsg = appHelpers.validate($scope.objLB);
        if (valMsg != 'passed') {
            appHelpers.alertCall(valMsg, 5000, 'alert-danger');
            return;
        }

        if ($scope.curContent.ContentID == '-1') $scope.curContent.ContentID = $scope.genContentID;

        if ($scope.ckeDetails) {
            $scope.curContent.Details = $scope.ckeDetails.getData();
        }

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'update-content',
                data: JSON.stringify($scope.curContent)
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.genContentID = ctJson.genContentID;
                $scope.curContent = ctJson.newContent;

                if ($scope.objLB.find('.fulPrimaryImage').length > 0 && $scope.objLB.find('.fulPrimaryImage').val() != '') {
                    $scope.uploadFiles($scope.objLB.find('.fulPrimaryImage'), $scope.curContent.ContentID, 'PrimaryImage');
                } else {
                    workWithList.AddObjectToList($scope.lstContent, $scope.curContent, 'ContentID');
                }

                $scope.objLB.data('ndtLightbox').close();
                appHelpers.alertCall(shopText.mContent.updateSuccess, 5000);
            }
        });
    };

    //#endregion

    //#region Delete Content

    $scope.deleteContent = function (contentID) {
        if (!confirm(shopText.mContent.confirmDelete)) return;

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'delete-content',
                data: contentID
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                appHelpers.alertCall(shopText.mContent.deleteComplete, 5000);
                workWithList.removeObjectByID($scope.lstContent, contentID, 'ContentID');
            }
        });
    };

    //#endregion

    //#region Load Form Content Image

    $scope.loadFormContentImage = function (contentID) {
        //$scope.curContent = workWithList.getObjectByID($scope.lstContent, contentID, 'ContentID');

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'load-content-image',
                data: contentID
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.curLstContentImage = ctJson.curLstImg;

                $scope.objLBContentImage = $('body').ndtLightbox({
                    title: shopText.mContent.extendsImageTitle,
                    //fixWidth: 200,
                    //fixHeight: '50%',
                    //dragable: true,
                    headerContent: '<span class="btn btn-link visible-xs" ng-click="toggleContentImageForm(\'extends\')">Close</span>',
                    withOverlay: false,
                    allowFullScreen: true,
                    outerClass: 'form-content-image',
                    content: $scope.tplFormImage,
                    onComplete: function onComplete() {}
                }, $compile, $scope);

                /*
                $scope.objLB = $('body').ndtLightbox({
                    title: shopText.mContent.extendsImageTitle,
                    outerClass: 'form-content-image',
                    content: $scope.tplFormImage,
                    onComplete: function () {
                      }
                }, $compile, $scope);
                */
            }
        });
    };

    //#endregion

    //#region Upload Content Image

    $scope.uploadContentImage = function () {
        var fileSelect = $scope.objLBContentImage.find('.fulContentImage');
        if (fileSelect.val() == '') {
            appHelpers.alertCall(shopText.mProduct.requireSelectFile, 5000, 'alert-danger');
            return;
        }

        var ContentID = $scope.curContent.ContentID;
        var GroupName = 'global';
        if (ContentID == '-1') {
            ContentID = $scope.genContentID;
            GroupName = 'temp';
        }

        $scope.uploadFiles(fileSelect, ContentID, 'ContentImage', GroupName);
    };

    //#endregion

    //#region Delete Content Image

    $scope.deleteContentImage = function (contentID) {
        if (!confirm(shopText.mProduct.confirmDeleteImage)) return;

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'delete-content-image',
                data: contentID
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                appHelpers.alertCall(shopText.mContent.deleteImageComplete, 5000);
                workWithList.removeObjectByID($scope.curLstContentImage, contentID, 'ContentID');
            }
        });
    };

    //#endregion

    //#region Load Form Content Details Image

    $scope.loadFormDetailsImage = function (contentID) {
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'load-content-details-image',
                data: contentID
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.curLstImg = ctJson.curLstImg;

                $scope.objLBImage = $('body').ndtLightbox({
                    title: shopText.mContent.btnDetailsImage,
                    //fixWidth: 200,
                    //fixHeight: '50%',
                    //dragable: true,
                    headerContent: '<span class="btn btn-link visible-xs" ng-click="toggleContentImageForm(\'details\')">Close</span>',
                    withOverlay: false,
                    allowFullScreen: true,
                    outerClass: 'form-content-details-image',
                    content: $scope.tplFormDetailsImage,
                    onComplete: function onComplete() {}
                }, $compile, $scope);
            }
        });
    };

    //#endregion

    //#region Upload Content Details Image

    $scope.uploadContentDetailsImage = function () {
        var fileSelect = $scope.objLBImage.find('.fulContentImage');
        if (fileSelect.val() == '') {
            appHelpers.alertCall(shopText.mContent.requireSelectFile, 5000, 'alert-danger');
            return;
        }

        var ContentID = $scope.curContent.ContentID;
        var GroupName = 'global';
        if (ContentID == '-1') {
            ContentID = $scope.genContentID;
            GroupName = 'temp';
        }

        $scope.uploadFiles(fileSelect, ContentID, 'ContentDetailsImage', GroupName);
    };

    //#endregion

    //#region Delete Content Details Image

    $scope.deleteContentDetailsImage = function (contentID) {
        if (!confirm(shopText.mContent.confirmDeleteImage)) return;

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'delete-content-details-image',
                data: contentID
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                appHelpers.alertCall(shopText.mContent.deleteImageComplete, 5000);
                workWithList.removeObjectByID($scope.curLstImg, contentID, 'ContentID');
            }
        });
    };

    //#endregion

    //#region Upload Files

    // fileType: PrimaryImage | ContentImage | ContentDetailsImage
    $scope.uploadFiles = function (objSelectFile, contentID, fileType, groupName) {
        if ($scope.isSending) return;

        if (!groupName) groupName = 'global';
        var objQuery = {
            contentID: contentID,
            fileType: fileType,
            groupName: groupName,
            thumbsSize: $scope.settings.thumbsSize
        };

        var objForm = new FormData();
        objForm.append('act', 'upload-files');
        objForm.append('data', JSON.stringify(objQuery));

        var listFiles = objSelectFile[0].files;

        if (listFiles.length > 0) {
            for (var i = 0; i < listFiles.length; i++) {
                objForm.append('file' + i, listFiles[i]);
            }
        }

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $.ajax({
            url: window.location.href,
            type: 'POST',
            contentType: false,
            processData: false,
            data: objForm,
            success: function success(result) {
                var ctJson = angular.fromJson(result);
                if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                    if (fileType == 'PrimaryImage') {
                        $scope.curContent.PrimaryImage = ctJson.imgName;
                        workWithList.AddObjectToList($scope.lstContent, $scope.curContent, 'ContentID');
                    } else if (fileType == 'ContentImage') {
                        appHelpers.alertCall(shopText.mContent.uploadImageSuccess, 5000);
                        var lstUploaded = ctJson.lstImage;

                        angular.forEach(lstUploaded, function (objImg) {
                            workWithList.AddObjectToList($scope.curLstContentImage, objImg, 'ContentID');
                        });
                    } else if (fileType == 'ContentDetailsImage') {
                        appHelpers.alertCall(shopText.mContent.uploadImageSuccess, 5000);
                        var lstUploaded = ctJson.lstImage;

                        angular.forEach(lstUploaded, function (objImg) {
                            workWithList.AddObjectToList($scope.curLstImg, objImg, 'ContentID');
                        });
                    }

                    $scope.$apply();

                    objSelectFile.parents('.input-group').find('.browse-file-name').val('');
                    objSelectFile.val('');
                }

                $scope.isSending = false;
                appHelpers.removeOverlayLoading();
            }
        });
    };

    //#endregion

    $scope.init();
}]);


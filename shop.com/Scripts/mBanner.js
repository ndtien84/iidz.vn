﻿'use strict';

ndtApp.controller('mbCtrl', ['$scope', '$http', '$compile', '$filter', 'workWithList', 'dateHelpers', function ($scope, $http, $compile, $filter, workWithList, dateHelpers) {
    $scope.ctn = $('.manage-banner-ctn');
    $scope.objLB;

    $scope.lstBanner = [];
    $scope.isSending = false;

    $scope.i18n = shopText.mBanner.template;

    //#region Init & Utilities

    $scope.init = function () {
        $('.admin-header-ctn .page-title').html(shopText.mBanner.bannerPageTitle);
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'load-init'
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.lstBanner = ctJson.lstBanner;
            }
        });
    };

    //#endregion

    //#region Update Banner

    $scope.updateBanner = function (contentID) {
        if ($scope.isSending) return;

        var itemCtn = $scope.ctn.find('.list-group-item[data-id="' + contentID + '"]');
        var txtPriority = itemCtn.find('.txt-priority');
        var objSelectFile = itemCtn.find('.fulPrimaryImage');

        var bannerData = { ContentID: contentID, Priority: 0 };

        if (txtPriority.val().trim() != '' && !isNaN(txtPriority.val())) bannerData.Priority = txtPriority.val();

        var objForm = new FormData();
        objForm.append('act', 'update-banner');
        objForm.append('data', JSON.stringify(bannerData));

        var listFiles = objSelectFile[0].files;

        if (listFiles.length > 0) {
            for (var i = 0; i < listFiles.length; i++) {
                objForm.append('file' + i, listFiles[i]);
            }
        } else if (listFiles.length == 0 && contentID == '-1') {
            appHelpers.alertCall(shopText.mBanner.requireSelectFile, 5000, 'alert-danger');
            return;
        }

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $.ajax({
            url: window.location.href,
            type: 'POST',
            contentType: false,
            processData: false,
            data: objForm,
            success: function success(result) {
                var ctJson = angular.fromJson(result);
                if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                    var lstBanner = ctJson.lstBanner;
                    angular.forEach(lstBanner, function (banner) {
                        workWithList.AddObjectToList($scope.lstBanner, banner, 'ContentID');
                    });

                    $scope.$apply();

                    objSelectFile.val('');
                    itemCtn.find('.browse-file-name').val('');
                    if (contentID == '-1') txtPriority.val('0');
                    appHelpers.alertCall(shopText.mBanner.updateSuccess, 5000);
                }

                $scope.isSending = false;
                appHelpers.removeOverlayLoading();
            }
        });
    };

    //#endregion

    //#region Delete Banner

    $scope.deleteBanner = function (contentID) {
        if (!confirm(shopText.mBanner.confirmDelete)) return;

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'delete-banner',
                data: contentID
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                workWithList.removeObjectByID($scope.lstBanner, contentID, 'ContentID');
                appHelpers.alertCall(shopText.mBanner.deleteComplete, 5000);
            }
        });
    };

    //#endregion

    $scope.init();
}]);


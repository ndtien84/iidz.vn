﻿'use strict';

ndtApp.controller('odCtrl', ['$scope', '$http', '$compile', '$filter', 'workWithList', 'dateHelpers', function ($scope, $http, $compile, $filter, workWithList, dateHelpers) {
    $scope.ctn = $('.manage-order-ctn');
    $scope.objLB;
    $scope.isSending = false;

    $scope.objFilter = {
        type: 'date-range', // date-range | search
        keyword: '',
        colName: 'Email',
        startDate: '',
        endDate: '',
        pageSize: 10,
        pageIndex: 1
    };

    $scope.totalPage = 1;
    $scope.totalRow = 0;

    $scope.lstStatus = [];

    $scope.tplOrderDetails = '';
    $scope.lstOrder = [];
    $scope.curOrderDetails = [];
    $scope.total = {};

    $scope.i18n = shopText.mOrders.template;

    //#region Init & Utilities

    $scope.init = function () {
        $('.admin-header-ctn .page-title').html(shopText.mOrders.pageTitle);

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'load-init'
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 10000, 'alert-danger');else {
                $scope.tplOrderDetails = ctJson.tplOrderDetails;
                $scope.lstStatus = ctJson.lstStatus;
            }
        });

        $scope.objFilter.startDate = dateHelpers.getCurDateString('date', -10);
        $scope.objFilter.endDate = dateHelpers.getCurDateString();

        if (appHelpers.queryHash('filtype')) {
            $scope.objFilter.type = appHelpers.queryHash('filtype');
        }
        if (appHelpers.queryHash('col')) {
            $scope.objFilter.colName = appHelpers.queryHash('col');
        }
        if (appHelpers.queryHash('keyword')) {
            $scope.objFilter.keyword = decodeURIComponent(appHelpers.queryHash('keyword'));
        }
        if (appHelpers.queryHash('sd')) {
            $scope.objFilter.startDate = appHelpers.queryHash('sd');
        }
        if (appHelpers.queryHash('ed')) {
            $scope.objFilter.endDate = appHelpers.queryHash('ed');
        }
        if (appHelpers.queryHash('pg')) {
            $scope.objFilter.pageIndex = appHelpers.queryHash('pg');
        }

        $scope.getOrder(null, $scope.objFilter.pageIndex);
    };

    $scope.getPageArray = function () {
        return new Array($scope.totalPage);
    };

    $scope.checkKeywordEnter = function (e) {
        var evt = e || window.event;
        if (evt.keyCode === 13) {
            $scope.getOrder('search', 1);
            return false;
        }
    };

    //#endregion

    //#region Get List Orders

    $scope.getOrder = function (getType, pageIndex) {
        if ($scope.isSending) return;

        if (getType) $scope.objFilter.type = getType;
        if (pageIndex) $scope.objFilter.pageIndex = pageIndex;

        var hashString = '';
        if ($scope.objFilter.type == 'date-range') {
            hashString = '#filtype=' + $scope.objFilter.type + '&sd=' + $scope.objFilter.startDate + '&ed=' + $scope.objFilter.endDate + '&pg=' + $scope.objFilter.pageIndex;
        } else if ($scope.objFilter.type == 'search') {
            hashString = '#filtype=' + $scope.objFilter.type + '&col=' + $scope.objFilter.colName + '&pg=' + $scope.objFilter.pageIndex + '&keyword=' + encodeURIComponent($scope.objFilter.keyword);
            if ($scope.objFilter.keyword.trim() == '') {
                appHelpers.alertCall(shopText.mOrders.requiredKeyword, 7000, 'alert-danger');
                $scope.ctn.find('.txt-keyword').focus();
                return;
            }
        }

        window.location.href = window.location.pathname + '?type=orders' + hashString;

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: "POST",
            url: window.location.href,
            data: {
                act: 'get-order',
                data: JSON.stringify($scope.objFilter)
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 10000, 'alert-danger');else {
                $scope.lstOrder = ctJson.lstOrder;
                $scope.totalPage = ctJson.totalPage;
                $scope.totalRow = ctJson.totalRow;
            }
        });
    };

    //#endregion

    //#region View Order Details

    $scope.viewOrderDetail = function (orderID) {
        $scope.total = { Quantity: 0, TotalPrice: 0 };

        $scope.curOrderDetails = workWithList.getObjectByID($scope.lstOrder, orderID, 'OrderID').ListDetails;

        angular.forEach($scope.curOrderDetails, function (details) {
            $scope.total.Quantity += details.Quantity;
            $scope.total.TotalPrice += details.Price * details.Quantity;
        });

        $scope.objLB = $('body').ndtLightbox({
            title: shopText.mOrders.lightboxTitle,
            //fixWidth: 600,
            //fixHeight: 500,
            //bindMaterial: false,
            //showHeader: false,
            outerClass: 'form-admin-order-details',
            //overlayClickClose: true,
            content: $scope.tplOrderDetails
        }, $compile, $scope);
    };

    //#endregion

    //#region Update Order Status

    $scope.updateStatus = function (orderID, statusID) {
        if ($scope.isSending) return;

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: "POST",
            url: window.location.href,
            data: {
                act: 'update-status',
                data: JSON.stringify({ OrderID: orderID, StatusID: statusID })
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 10000, 'alert-danger');else {
                var newObj = ctJson.newObj;
                workWithList.AddObjectToList($scope.lstOrder, newObj, 'OrderID');
                appHelpers.alertCall('Status updated', 6000);
            }
        });
    };

    //#endregion

    //#region Delete Order

    $scope.deleteOrder = function (orderID) {
        if ($scope.isSending) return;

        if (!confirm('Are you sure to delete this order ?')) return;

        $scope.isSending = true;
        appHelpers.callOverlayLoading();

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'delete-order',
                data: orderID
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                workWithList.removeObjectByID($scope.lstOrder, orderID, 'OrderID');
                appHelpers.alertCall('Order deleted', 7000);
            }
        });
    };

    //#endregion

    $scope.init();
}]);


﻿'use strict';

ndtApp.controller('mcsCtrl', ['$scope', '$http', '$compile', '$filter', 'workWithList', 'dateHelpers', function ($scope, $http, $compile, $filter, workWithList, dateHelpers) {
    $scope.ctn = $('.multi-content-simple-ctn');
    $scope.objLB;

    $scope.curContent = { Priority: 0 };
    $scope.lstContent = [];
    $scope.isSending = false;

    $scope.contentType = '';
    $scope.pageSize = 10;
    $scope.pageIndex = 1;
    $scope.totalPage = 0;
    $scope.totalRow = 0;
    $scope.orderColumn = 'Priority';
    $scope.orderDirection = 'desc'; // desc | asc

    $scope.setting = {
        confirmDelete: 'Delete content ?',
        deleteSuccess: 'Content deleted',
        updateSuccess: 'Content updated',

        thumbsSize: 500
    };

    //#region Init & Utilities

    $scope.init = function () {
        if (appHelpers.queryString('type')) {
            $scope.contentType = appHelpers.queryString('type');
        } else return;

        if (appHelpers.queryHash('pg')) $scope.pageIndex = appHelpers.queryHash('pg');

        //#region Settings For Each Content Type

        if ($scope.contentType == 'youtube') {
            $('.admin-header-ctn .page-title').html('Quản trị Link Youtube');

            $scope.setting.confirmDelete = 'Xóa youtube link ?';
            $scope.setting.deleteSuccess = 'Xóa youtube link thành công';
            $scope.setting.updateSuccess = 'Cập nhật youtube link thành công';
        } else if ($scope.contentType == 'ext-link') {
            $('.admin-header-ctn .page-title').html('Quản trị link liên kết');

            $scope.setting.confirmDelete = 'Xóa link liên kết ?';
            $scope.setting.deleteSuccess = 'Xóa link liên kết thành công';
            $scope.setting.updateSuccess = 'Cập nhật link liên kết thành công';
        } else if ($scope.contentType == 'team') {
            $('.admin-header-ctn .page-title').html('Quản trị Team');

            $scope.setting.confirmDelete = 'Xóa ?';
            $scope.setting.deleteSuccess = 'Xóa thành công';
            $scope.setting.updateSuccess = 'Cập nhật thành công';
        }

        // #endregion

        $scope.getListContent($scope.pageIndex);
    };

    $scope.getListContent = function (pageIndex) {
        if ($scope.isSending) return;

        window.location.href = window.location.pathname + '?type=' + $scope.contentType + '#pg=' + pageIndex;

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'load-list',
                data: JSON.stringify({ ContentType: $scope.contentType, PageSize: $scope.pageSize, PageIndex: pageIndex, OrderColumn: $scope.orderColumn, OrderDirection: $scope.orderDirection })
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.lstContent = ctJson.lstContent;
                $scope.totalRow = ctJson.totalRow;
                $scope.totalPage = ctJson.totalPage;
                $scope.pageIndex = pageIndex;
            }
        });
    };

    $scope.getPageArray = function () {
        return new Array($scope.totalPage);
    };

    //#endregion

    //#region Re-create All Thumbs

    $scope.recreateThumbs = function () {
        if ($scope.isSending) return;

        if (!confirm('Recreate all thumbs ?')) return;

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'recreate-thumbs',
                data: JSON.stringify({ type: $scope.contentType, thumbsSize: $scope.setting.thumbsSize })
            }
        }).success(function (result) {
            $scope.isSending = true;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                appHelpers.alertCall(ctJson.message, 60000);
            }
        });
    };

    //#endregion

    //#region Update Content

    $scope.updateContent = function (contentID) {
        if ($scope.isSending) return;

        var itemCtn = $scope.ctn.find('[data-id="' + contentID + '"]');

        if ($scope.contentType == 'test-1') {
            // do something for this contentType
        } else if ($scope.contentType == 'home-banner') {
                // do something special contentType
            }

        if (contentID != '-1') $scope.curContent = workWithList.getObjectByID($scope.lstContent, contentID, 'ContentID');else {
            $scope.curContent.ContentID = '-1';
            $scope.curContent.ContentType = $scope.contentType;
        }

        var valMsg = appHelpers.validate(itemCtn);
        if (valMsg != 'passed') {
            appHelpers.alertCall(valMsg, 7000, 'alert-danger');
            return;
        }

        //alert('uplodatedr');
        //return;

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'update-content',
                data: JSON.stringify($scope.curContent)
            }
        }).success(function (result) {
            $scope.isSending = false;

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                var newContent = ctJson.newContent;
                $scope.curContent = { Priority: 0 };

                // upload image if exists
                var objFile = itemCtn.find('input[type="file"]');
                if (objFile.length > 0 && objFile.val() != '') {
                    // do upload
                    $scope.uploadFiles(objFile, newContent.ContentID, 'PrimaryImage');
                    // clear data
                    objFile.parents('.input-group').find('input[type="text"]').val('');
                    objFile.val('');
                } else {
                    workWithList.AddObjectToList($scope.lstContent, newContent, 'ContentID');
                    workWithList.sortList($scope.lstContent, $scope.orderColumn, $scope.orderDirection);

                    appHelpers.removeOverlayLoading();
                    appHelpers.alertCall($scope.setting.updateSuccess, 7000);
                }
            }
        });
    };

    //#endregion

    //#region Delete Content

    $scope.deleteContent = function (contentID) {
        if ($scope.isSending) return;
        if (!confirm($scope.setting.confirmDelete)) return;

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'delete-content',
                data: contentID
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                workWithList.removeObjectByID($scope.lstContent, contentID, 'ContentID');
                appHelpers.alertCall($scope.setting.deleteSuccess, 5000);
            }
        });
    };

    //#endregion

    //#region Upload Files

    // fileType: PrimaryImage
    $scope.uploadFiles = function (objSelectFile, contentID, fileType) {
        if ($scope.isSending) return;

        var objQuery = {
            ContentID: contentID,
            FileType: fileType,
            ThumbsSize: $scope.setting.thumbsSize
        };

        var objForm = new FormData();
        objForm.append('act', 'upload-files');
        objForm.append('data', JSON.stringify(objQuery));

        var listFiles = objSelectFile[0].files;

        if (listFiles.length > 0) {
            for (var i = 0; i < listFiles.length; i++) {
                objForm.append('file' + i, listFiles[i]);
            }
        }

        $scope.isSending = true;
        $.ajax({
            url: window.location.href,
            type: 'POST',
            contentType: false,
            processData: false,
            data: objForm,
            success: function success(result) {
                $scope.isSending = false;
                appHelpers.removeOverlayLoading();

                var ctJson = angular.fromJson(result);
                if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                    if (fileType == 'PrimaryImage') {
                        var newContent = ctJson.newContent;
                        workWithList.AddObjectToList($scope.lstContent, newContent, 'ContentID');

                        workWithList.sortList($scope.lstContent, $scope.orderColumn, $scope.orderDirection);
                    }

                    $scope.$apply();
                    appHelpers.alertCall($scope.setting.updateSuccess, 7000);
                }
            }
        });
    };

    //#endregion

    $scope.init();
}]);


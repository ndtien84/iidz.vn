﻿'use strict';

$(document).ready(function () {
    navDrawerFront.init();
    navDrawerAdmin.init();
    mCategory.init();

    ctUs.init();
    sProfile.init();

    appHelpers.init();
});

//#region Mobile Category

var mCategory = {
    ctn: '',

    init: function init() {
        this.ctn = $('.left-category-ctn');

        $('.left-category-ctn .item.header').bind('click', function () {
            if ($(this).hasClass('expand')) {
                mCategory.ctn.find('.category-item').hide(200);
                $(this).removeClass('expand');
            } else {
                mCategory.ctn.find('.category-item').show(200);
                $(this).addClass('expand');
            }
        });
    }
};

//#endregion

//#region Nav Drawer Font End

var navDrawerFront = {
    drawer: null,
    btnDrawer: null,

    headerHeight: 50,
    isShow: false,
    mnWidth: 270,

    init: function init() {
        if ($('body.font-end').length <= 0) return;

        this.drawer = $('body.font-end .nav-drawer-ctn');
        this.btnDrawer = $('body.font-end .nav-drawer-icon');
        this.headerHeight = $('body.font-end .header-ctn').outerHeight();

        this.btnDrawer.bind('click', function () {
            if (navDrawerFront.drawer.is(':animated')) return;

            if (navDrawerFront.isShow) {
                navDrawerFront.close();
            } else {
                navDrawerFront.open();
            }
        });
    },

    open: function open() {
        this.drawer.height($(window).height() + 60);
        this.drawer.animate({ left: 0 }, 200);
        this.btnDrawer.addClass('selected');
        $('body').css('overflow-y', 'hidden');
        this.isShow = true;

        $('<div>').attr('class', 'nav-drawer-overlay').click(function () {
            navDrawerFront.close();
        }).appendTo($('body'));
    },

    close: function close() {
        this.drawer.animate({ left: -this.mnWidth }, 200);
        $('body').css('overflow-y', 'auto');
        this.btnDrawer.removeClass('selected');
        this.isShow = false;
        $('.nav-drawer-overlay').fadeOut(100).remove();
    }
};

//#endregion

//#region Nav Drawer Admin

var navDrawerAdmin = {
    drawer: null,
    btnDrawer: null,

    headerHeight: 50,
    isShow: false,
    mnWidth: 270,

    init: function init() {
        if ($('body.admin').length <= 0) return;

        this.drawer = $('body.admin .nav-drawer-ctn');
        this.btnDrawer = $('body.admin .nav-drawer-icon');
        this.headerHeight = $('body.admin .header-ctn').outerHeight();

        this.btnDrawer.bind('click', function () {
            if (navDrawerAdmin.drawer.is(':animated')) return;

            if (navDrawerAdmin.isShow) {
                navDrawerAdmin.close();
            } else {
                navDrawerAdmin.open();
            }
        });
    },

    open: function open() {
        this.drawer.height($(window).height() - this.headerHeight);
        this.drawer.animate({ left: 0 }, 200);
        this.btnDrawer.addClass('selected');
        $('body').css('overflow-y', 'hidden');
        this.isShow = true;

        $('<div>').attr('class', 'nav-drawer-overlay').click(function () {
            navDrawerAdmin.close();
        }).appendTo($('body'));
    },

    close: function close() {
        this.drawer.animate({ left: -this.mnWidth }, 200);
        $('body').css('overflow-y', 'auto');
        this.btnDrawer.removeClass('selected');
        this.isShow = false;
        $('.nav-drawer-overlay').fadeOut(100).remove();
    }
};

//#endregion

//#region Contact Us

var ctUs = {
    ctn: null,
    form: null,
    valDOM: null,
    isSending: false,

    init: function init() {
        if ($('.contact-ctn').length <= 0) return;

        this.ctn = $('.contact-ctn');
        this.form = this.ctn.find('.form-ctn');
        this.valDOM = this.ctn.find('.validate-msg');

        this.ctn.find('.button-send').bind('click', function () {
            if (ctUs.isSending) return;

            var valMsg = appHelpers.validate(ctUs.form);
            if (valMsg != 'passed') {
                ctUs.ctn.find('.validate-msg').html(valMsg);
                return;
            }

            var data = appHelpers.getFormData(ctUs.form);

            ctUs.isSending = true;
            ctUs.valDOM.html('').addClass('loading');
            $.ajax({
                url: window.location.href,
                type: 'POST',
                data: {
                    formContent: JSON.stringify(data)
                },
                success: function success(result) {
                    ctUs.valDOM.removeClass('loading');

                    if (result.error) ctUs.ctn.find('.validate-msg').html(result.error);else {
                        ctUs.ctn.find('.validate-msg').html('Thank you!');
                        ctUs.isSending = false;

                        appHelpers.clearFormData(ctUs.form);
                    }
                }
            });
        });
    }
};

//#endregion

//#region Update Profiles

var sProfile = {
    ctn: null,
    form: null,
    valDOM: null,
    isSending: false,

    init: function init() {
        if ($('.user-profile-ctn').length <= 0) return;

        this.ctn = $('.user-profile-ctn');
        this.form = this.ctn.find('.profile-form');
        this.valDOM = this.ctn.find('.validate-msg');

        this.ctn.find('.button-send').bind('click', function () {
            if (sProfile.isSending) return;

            var valMsg = appHelpers.validate(sProfile.form);
            if (valMsg != 'passed') {
                sProfile.valDOM.html(valMsg);
                return;
            }

            var data = {
                DisplayName: sProfile.form.find('.txt-display-name').val(),
                CellPhone: sProfile.form.find('.txt-cellphone').val(),
                Address: sProfile.form.find('.txt-address').val()
            };

            sProfile.isSending = true;
            sProfile.valDOM.html('').addClass('loading');
            $.ajax({
                url: window.location.href,
                type: 'POST',
                data: {
                    userProfile: JSON.stringify(data)
                },
                success: function success(result) {
                    sProfile.isSending = false;
                    sProfile.valDOM.removeClass('loading');

                    if (result.error) sProfile.valDOM.html(result.error);else {
                        sProfile.valDOM.html('Update profile success');
                    }
                }
            });
        });
    }
};

//#endregion

//#region App Helpers

var appHelpers = {
    culture: $('html').attr('lang').toLowerCase(),
    alertShow: false,
    alertTimer: 0,

    dtPickerOps: {
        animationDuration: 0,
        setButtonContent: "OK",
        clearButtonContent: "RESET",

        formatHumanDate: function formatHumanDate(date) {
            if (appHelpers.culture == 'vi-vn') return date.dayShort + ", " + date.dd + " " + date.month + ", " + date.yyyy;else return date.dayShort + ", " + date.month + " " + date.dd + ", " + date.yyyy;
        }
    },

    init: function init() {
        if (appHelpers.culture == 'vi-vn') try {
            $.extend(this.dtPickerOps, shopText.datePickerFormat);
        } catch (e) {} else {
            this.dtPickerOps.dateTimeFormat = 'MM-dd-yyyy HH:mm:ss';
            this.dtPickerOps.dateFormat = 'MM-dd-yyyy';
        }
    },

    //#region Overlay loading

    callOverlayLoading: function callOverlayLoading() {
        if ($('.ndt-gb-overlay-loading').length > 0) return;
        $('<div>').attr('class', 'ndt-gb-overlay-loading').appendTo('body');
    },

    removeOverlayLoading: function removeOverlayLoading() {
        $('.ndt-gb-overlay-loading').remove();
    },

    //#endregion

    //#region Alert

    alertCall: function alertCall(content, lifeTime, typeClass) {
        if (!typeClass) typeClass = 'alert-info';

        var template = '<div class="ndt-gb-alert alert alert-dismissable ' + typeClass + '">';
        template += '<button type="button" class="close" onclick="appHelpers.alertClose()" data-dismiss="alert">x</button>';
        template += content;
        template += '</div>';

        $('.ndt-gb-alert').remove();
        $(template).prependTo('body').fadeIn(500, function () {
            appHelpers.alertTimer = setTimeout("appHelpers.alertClose()", lifeTime);
        });
    },

    alertClose: function alertClose() {
        clearTimeout(this.alertTimer);
        this.alertShow = false;
        $('.ndt-gb-alert').fadeOut(500, function () {
            $(this).remove();
        });
    },

    //#endregion

    //#region Validate Form

    validate: function validate(formCtn) {
        var Passed = true;
        var RegExpString = '';
        var ValidateMsg = 'passed';

        var ListTextBox = formCtn.find('input[type=text],input[type=file], textarea, select');

        ListTextBox.each(function (index) {
            if (typeof $(this).attr('data-regexp') != 'undefined' || typeof $(this).attr('min-length') != 'undefined' || typeof $(this).attr('max-length') != 'undefined' || typeof $(this).attr('numberic') != 'undefined' || typeof $(this).attr('email') != 'undefined' || typeof $(this).attr('ddl-select') != 'undefined') {
                $(this).focus();
            }

            if (typeof $(this).attr('data-msg') != 'undefined') {
                ValidateMsg = $(this).attr('data-msg');
            }

            if (typeof $(this).attr('ddl-select') != 'undefined') {
                if ($(this)[0].selectedIndex == 0) {
                    Passed = false;
                    return false;
                }
            }

            if (typeof $(this).attr('data-regexp') != 'undefined') {
                RegExpString = new RegExp($(this).attr('data-regexp'));
                if (!RegExpString.test($(this).val())) {
                    Passed = false;
                    return false;
                }
            }
            if (typeof $(this).attr('min-length') != 'undefined') {
                if ($(this).val().length < parseInt($(this).attr('min-length'))) {
                    Passed = false;
                    return false;
                }
            }
            if (typeof $(this).attr('max-length') != 'undefined') {
                if ($(this).val().length > parseInt($(this).attr('max-length'))) {
                    Passed = false;
                    return false;
                }
            }
            if (typeof $(this).attr('numberic') != 'undefined') {
                if (isNaN($(this).val()) || $(this).val().trim() == '') {
                    Passed = false;
                    return false;
                }
            }
            if (typeof $(this).attr('email') != 'undefined') {
                RegExpString = /^(([\-\w]+)\.?)+@(([\-\w]+)\.?)+\.[a-zA-Z]{2,4}$/;
                if (!RegExpString.test($(this).val())) {
                    Passed = false;
                    return false;
                }
            }
        });

        if (Passed) {
            ListTextBox.blur();
            return 'passed';
        } else return ValidateMsg;
    },

    //#endregion

    //#region Get Form Data

    getFormData: function getFormData(formCtn) {
        var listCtrl = formCtn.find('input[type=text], textarea, select');
        var fieldName;
        var returnData = {};

        listCtrl.each(function (index) {
            fieldName = $(this).attr('name');
            returnData[fieldName] = $(this).val();
        });

        return returnData;
    },

    //#endregion

    //#region Clear Form Data

    clearFormData: function clearFormData(formCtn) {
        formCtn.find('input[type=text], textarea').val('');
        formCtn.find('select option').eq(0).prop('selected', true);
    },

    //#endregion

    //#region QueryString & Hash

    queryString: function queryString(sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }

        return null;
    },

    queryHash: function queryHash(sParam) {
        var sPageURL = window.location.hash;
        var sURLVariables = sPageURL.substring(1, sPageURL.length).split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }

        return null;
    },

    removeHash: function removeHash(hashName) {
        var sPageURL = window.location.hash;
        var sURLVariables = sPageURL.substring(1, sPageURL.length).split('&');

        var newHashString = '#';

        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] != hashName) {
                newHashString += sParameterName[0] + '=' + sParameterName[1] + '&';
            }
        }

        newHashString = newHashString.substr(0, newHashString.length - 1);
        window.location.hash = newHashString;
    },

    setHash: function setHash(hashName, hashValue) {
        var sPageURL = window.location.hash;
        var isDup = false;
        var newHashString = '#';

        if (sPageURL.length > 2) {
            var sURLVariables = sPageURL.substring(1, sPageURL.length).split('&');

            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] != hashName) {
                    newHashString += sParameterName[0] + '=' + sParameterName[1] + '&';
                } else {
                    newHashString += sParameterName[0] + '=' + hashValue + '&';
                    isDup = true;
                }
            }

            newHashString = newHashString.substr(0, newHashString.length - 1);
        }

        if (!isDup) {
            if (newHashString.length > 2) newHashString += '&';

            newHashString += hashName + '=' + hashValue;
        }

        window.location.hash = newHashString;
    }

    //#endregion
};

//#endregion

//#region Angular

if ($('html').attr('ng-app')) {
    var ndtApp = angular.module('ndtApp', []);

    ndtApp.config(['$httpProvider', '$sceProvider', function ($httpProvider, $sceProvider) {
        //$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
        // allow embed youtube iframe
        $sceProvider.enabled(false);
    }]);
    /*
    ndtApp.directive('bindMaterial', function () {
        return function (scope, element, attrs) {
            if (scope.$last) {
                setTimeout(function () {
                    $.material.init();
                }, 500);
            }
        };
    });
      ndtApp.directive('scaleImage', function () {
        return function (scope, element, attrs) {
            if (scope.$last) {
                $('.img-scale').imageScale();
            }
        }
    });
    */

    // #region Service & Factory

    ndtApp.service('appSettings', function () {
        this.mediaLink = function () {
            return 'http://' + window.location.host + '/s-media/';
        };
    });

    ndtApp.service('workWithList', function () {

        this.sortList = function (lstObject, fieldName, reverse) {
            if (reverse) {
                if (reverse.toString().toLowerCase() == 'true' || reverse.toString().toLowerCase() == 'desc') reverse = true;else reverse = false;
            }

            lstObject.sort(function (a, b) {
                return a[fieldName] > b[fieldName] ? 1 : -1;
            });
            if (reverse) lstObject.reverse();
        };

        //#region Get object by ID

        this.getObjectByID = function (lstObject, objID, idFieldName) {
            var isGet = false;
            var objGet = null;

            angular.forEach(lstObject, function (objLoop) {
                if (!isGet) {
                    if (objLoop[idFieldName] == objID) {
                        // copy: prevent reference to obj lstCamp
                        objGet = angular.copy(objLoop);
                        isGet = true;
                    }
                }
            });

            return objGet;
        };

        //#endregion

        //#region Remove object by id

        this.removeObjectByID = function (lstObject, objID, idFieldName) {
            var isRemoved = false;
            var loopIndex = 0;

            angular.forEach(lstObject, function (objLoop) {
                if (!isRemoved) {
                    if (objLoop[idFieldName] == objID) {
                        lstObject.splice(loopIndex, 1);
                        isRemoved = true;
                    }
                    loopIndex++;
                }
            });
        };

        //#endregion

        //#region Add object to list (check duplicate)

        this.AddObjectToList = function (lstObject, objAdd, idFieldName) {
            var isDup = false;
            var loopIndex = 0;

            objAdd = angular.copy(objAdd);

            angular.forEach(lstObject, function (objLoop) {
                if (!isDup) {
                    if (objLoop[idFieldName] == objAdd[idFieldName]) {
                        lstObject[loopIndex] = objAdd;
                        isDup = true;
                    }
                    loopIndex++;
                }
            });

            if (!isDup) {
                lstObject.push(objAdd);
            }
        };

        //#endregion
    });

    ndtApp.service('dateHelpers', function () {

        //#region Delete Date Field of an Object
        /*
        this.deleteDateField = function (objJson) {
            var dateReg = /\/Date\((.*?)\)\//;
            $.each(objJson, function (key, value) {
                if (dateReg.test(value)) delete objJson[key];
            });
            return objJson;
        }
        */
        //#endregion

        //#region Get Current Date String

        // datePart         : month | date | year
        // datePartValue    : value to add/minus
        // ex: datePart = date, datePartValue = -10 : return current date, minus 10 days
        this.getCurDateString = function (datePart, datePartValue) {
            var date = new Date();

            if (datePart && datePartValue) {
                datePartValue = parseInt(datePartValue);

                if (datePart == 'month') date.setMonth(date.getMonth() + datePartValue);else if (datePart == 'date') date.setDate(date.getDate() + datePartValue);else if (datePart == 'year') date.setFullYear(date.getFullYear() + datePartValue);
            }

            var d = date.getDate().toString();
            var m = (date.getMonth() + 1).toString();

            if (m.length == 1) m = '0' + m;
            if (d.length == 1) d = '0' + d;

            if (appHelpers.culture == 'vi-vn') return d + '-' + m + '-' + date.getFullYear();else return m + '-' + d + '-' + date.getFullYear();
        };

        //#endregion

        //#region Get Past Date String

        this.getPastDateString = function (pastDay) {
            var date = new Date();
            date.setDate(date.getDate() - pastDay);
            var d = date.getDate().toString();
            var m = (date.getMonth() + 1).toString();

            if (m.length == 1) m = '0' + m;
            if (d.length == 1) d = '0' + d;

            return m + '-' + d + '-' + date.getFullYear();
        };

        //#endregion
    });

    //#endregion

    //#region Filter

    // filter youtube id
    ndtApp.filter('getYoutubeEmbedLink', function () {
        return function (videoLink) {
            var parameterValue = RegExp('[?&]v=([^&]*)').exec(videoLink);
            var videoID = parameterValue && decodeURIComponent(parameterValue[1].replace(/\+/g, ' '));
            return 'http://www.youtube.com/embed/' + videoID;
        };
    });
    /*
    ndtApp.filter('getMediaLink', function () {
        return function (fileName) {
            if (!fileName) fileName = 'default.png';
            return 'http://' + window.location.host + '/n-media/' + fileName;
        }
    });
    
    ndtApp.filter('getMediaName', function () {
        return function (fileName) {
            try {
                fileName = fileName.split('_')[1];
            } catch (e) {
              }
              return fileName;
        }
    });
    */
    // get product label
    ndtApp.filter('getProductLabel', function () {
        return function (labelID) {
            if (labelID == 'sell') return 'Sell Off';else if (labelID == 'new') return 'New arrival';
        };
    });

    // filter date from json
    ndtApp.filter("formatDateFromJson", function () {
        return function (x) {
            if (x) {
                if (x.indexOf('-') > 0) {
                    return x;
                } else {
                    return new Date(parseInt(x.replace(/\/Date\((.*?)\)\//gi, "$1")));
                }
            } else {
                var date = new Date();
                var d = date.getDate().toString();
                var m = (date.getMonth() + 1).toString();
                if (m.length == 1) m = '0' + m;
                if (d.length == 1) d = '0' + d;

                return d + '-' + m + '-' + date.getFullYear();
            }
        };
    });

    // filter VND
    ndtApp.filter('formatVND', function () {
        return function (inputMoney) {
            if (!inputMoney) return '0';

            inputMoney = inputMoney.toString();

            if (inputMoney.charAt(0) == '0') inputMoney = inputMoney.substring(1, inputMoney.length);
            inputMoney = inputMoney.replace(/\./g, '').replace(/\,/g, '').replace(/\ /g, '');

            var outputMoney = "";
            var tempOutputMoney = "";
            var charArray = inputMoney.toString().split('');
            var j = 0;

            for (var i = charArray.length - 1; i >= 0; i--) {
                if (j % 3 == 0 && j != 0) {
                    tempOutputMoney += "." + charArray[i];
                } else {
                    tempOutputMoney += charArray[i];
                }
                j++;
            }

            tempOutputMoney = tempOutputMoney.toString().split('');

            for (var i = tempOutputMoney.length - 1; i >= 0; i--) {
                outputMoney += tempOutputMoney[i];
            }

            return outputMoney;
        };
    });

    /*
    http://justinklemm.com/angularjs-filter-ordering-objects-ngrepeat/
    custom order by (working with object - default of angular order by only work with array)
    this is NOT WORKING with 'track by $index'
    */
    /*
    ndtApp.filter('orderObjectBy', function () {
        return function (items, field, reverse) {
            if (reverse) {
                if (reverse.toString().toLowerCase() == 'true') reverse = true;
                else reverse = false;
            }
              var filtered = [];
            angular.forEach(items, function (item) {
                filtered.push(item);
            });
            filtered.sort(function (a, b) {
                return (a[field] > b[field] ? 1 : -1);
            });
            if (reverse) filtered.reverse();
            return filtered;
        };
    });
    */
    ndtApp.filter('removeFirstBr', function () {
        return function (inputString) {
            if (inputString.substring(0, 4) == '<br>') {
                return inputString.substring(4, inputString.length);
            } else if (inputString.substring(0, 5) == '<br/>') {
                return inputString.substring(5, inputString.length);
            } else {
                return inputString;
            }
        };
    });

    //#endregion
}

//#endregion


﻿'use strict';
ndtApp.controller('mpCtrl', ['$scope', '$http', '$compile', '$filter', 'workWithList', 'appSettings', 'dateHelpers', function ($scope, $http, $compile, $filter, workWithList, appSettings, dateHelpers) {
    $scope.ctn = $('.manage-product-ctn');
    //$scope.mediaLink = appSettings.mediaLink();

    $scope.objLB;
    $scope.objLBExtendsImages;
    $scope.objLBDetailsImage;

    $scope.ckeDetails;

    $scope.isSending = false;

    $scope.tplFormProduct = '';
    $scope.tplFormExtendsImage = '';
    $scope.tplFormDetailsImage = '';

    $scope.lstCategory = [];

    $scope.lstProduct = [];
    $scope.curProduct = {};

    $scope.totalPage = 0;
    $scope.totalRow = 0;

    $scope.genProductID = '-1';
    $scope.curLstExtendsImg = [];
    $scope.curLstDetailsImage = [];

    $scope.objFilter = {
        type: 'bycategory', // bycategory | search
        categoryID: 'all',
        productID: '-1',
        keyword: '',
        keywordColumn: 'Name,Code',
        orderColumn: 'Priority',
        orderDirection: 'desc', // desc | asc
        pageIndex: 1
    };

    $scope.settings = {
        productImage: true,
        detailsImage: true,
        showCategory: true,
        showSearch: true
    };

    $scope.i18n = shopText.mProduct.template;

    //#region Init & Utilities

    $scope.init = function () {
        if (appHelpers.queryHash('filtype')) $scope.objFilter.type = appHelpers.queryHash('filtype');
        if (appHelpers.queryHash('cat')) $scope.objFilter.categoryID = appHelpers.queryHash('cat');
        if (appHelpers.queryHash('proid')) $scope.objFilter.productID = appHelpers.queryHash('proid');
        if (appHelpers.queryHash('pg')) $scope.objFilter.pageIndex = appHelpers.queryHash('pg');
        if (appHelpers.queryHash('keyword')) $scope.objFilter.keyword = decodeURIComponent(appHelpers.queryHash('keyword'));

        $('.admin-header-ctn .page-title').html(shopText.mProduct.pageTitle);

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'load-init'
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.lstCategory = ctJson.lstCategory;
                $scope.tplFormProduct = ctJson.tplFormProduct;
                $scope.tplFormExtendsImage = ctJson.tplFormExtendsImage;
                $scope.tplFormDetailsImage = ctJson.tplFormDetailsImage;
                $scope.genProductID = ctJson.genProductID;

                //console.log($scope.genProductID);

                setTimeout(function () {
                    $scope.ctn.find('.my-dropdown').dropdown();
                }, 500);

                $scope.loadProductByCategory(null, $scope.objFilter.pageIndex, $scope.objFilter.categoryID);
            }
        });
    };

    $scope.getPageArray = function () {
        return new Array($scope.totalPage);
    };

    $scope.checkKeywordEnter = function (e) {
        var evt = e || window.event;
        if (evt.keyCode === 13) {
            $scope.loadProductByCategory('search', 1, null);
            return false;
        }
    };

    $scope.loadCkEditor = function () {
        var config = {
            baseFloatZIndex: 100002,
            enterMode: CKEDITOR.ENTER_BR,
            shiftEnterMode: CKEDITOR.ENTER_P
        };

        if ($scope.ckeDetails) {
            try {
                $scope.ckeDetails.destroy();
            } catch (ex) {}

            $scope.ckeDetails = null;
        }

        var content = $('#ckeDetailsHTML').text();

        $scope.ckeDetails = CKEDITOR.appendTo('ckeDetails', config, content);
    };

    // formType: extends | details
    $scope.toggleDetailsImageForm = function (formType) {
        var subLBClass = formType == 'extends' ? '.form-extends-images' : '.form-details-image';

        if ($('body').find(subLBClass).is(':visible')) {
            $('body').find(subLBClass).fadeOut(1);
        } else {
            setTimeout(function () {
                $('body').find(subLBClass).show();
            }, 300);
        }

        $scope.objLB.find('.form-manage-product').toggleClass('has-image-' + formType);
    };

    $scope.recreateThumbs = function () {
        if ($scope.isSending) return;

        if (!confirm('Recreate all thumbs ?')) return;

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'recreate-thumbs'
            }
        }).success(function (result) {
            $scope.isSending = true;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                appHelpers.alertCall(ctJson.message, 60000);
            }
        });
    };

    $scope.setPrimaryImage = function (imageID, imageName) {
        if ($scope.isSending) return;

        var objData = {
            imageID: imageID,
            imageName: imageName,
            productID: $scope.curProduct.ProductID,
            productImage: $scope.curProduct.PrimaryImage
        };

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'set-primary-image',
                data: JSON.stringify(objData)
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                var prevCurProduct = angular.copy($scope.curProduct);

                if (prevCurProduct.PrimaryImage != 'default.png') {
                    var objImage = workWithList.getObjectByID($scope.curLstExtendsImg, imageID, 'ContentID');
                    if (objImage != null) {
                        objImage.PrimaryImage = prevCurProduct.PrimaryImage;
                        workWithList.AddObjectToList($scope.curLstExtendsImg, objImage, 'ContentID');
                    } else {
                        objImage = workWithList.getObjectByID($scope.curLstDetailsImage, imageID, 'ContentID');
                        objImage.PrimaryImage = prevCurProduct.PrimaryImage;
                        workWithList.AddObjectToList($scope.curLstDetailsImage, objImage, 'ContentID');
                    }
                } else {
                    workWithList.removeObjectByID($scope.curLstExtendsImg, imageID, 'ContentID');
                    workWithList.removeObjectByID($scope.curLstDetailsImage, imageID, 'ContentID');
                }

                $scope.curProduct.PrimaryImage = imageName;
                workWithList.AddObjectToList($scope.lstProduct, $scope.curProduct, 'ProductID');
            }
        });
    };

    //#endregion

    //#region Load List Products

    $scope.loadProductByCategory = function (getType, pageIndex, categoryID) {
        //window.location.href = window.location.pathname + '#cat=' + categoryID + '&pg=' + pageIndex;
        // ?type=product: just for work on IE9
        if ($scope.isSending) return;

        if (getType) $scope.objFilter.type = getType;
        if (pageIndex) $scope.objFilter.pageIndex = pageIndex;
        if (categoryID) $scope.objFilter.categoryID = categoryID;

        var hashString = '';
        if ($scope.objFilter.type == 'bycategory') {
            hashString = '#filtype=' + $scope.objFilter.type + '&cat=' + $scope.objFilter.categoryID + '&pg=' + $scope.objFilter.pageIndex;
        } else if ($scope.objFilter.type == 'search') {
            if ($scope.objFilter.keyword == '') {
                appHelpers.alertCall(shopText.mProduct.requiredKeyword, 7000, 'alert-danger');
                $scope.ctn.find('.txt-keyword').focus();
                return;
            }
            hashString = '#filtype=' + $scope.objFilter.type + '&pg=' + $scope.objFilter.pageIndex + '&keyword=' + encodeURIComponent($scope.objFilter.keyword);
        }

        window.location.href = window.location.pathname + '?type=product' + hashString;

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'load-list-product',
                data: JSON.stringify($scope.objFilter)
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.lstProduct = ctJson.lstProduct;
                $scope.totalPage = ctJson.totalPage;
                $scope.totalRow = ctJson.totalRow;
                //$scope.objFilter.categoryID = categoryID;
                //$scope.objFilter.pageIndex = pageIndex;

                if ($scope.objFilter.productID != '-1') $scope.loadFormProduct($scope.objFilter.productID);

                // scroll top
                $('body,html').animate({ scrollTop: 0 }, 100);
            }
        });
    };

    //#endregion

    //#region Load Form Product

    $scope.loadFormProduct = function (productID) {
        if (productID != '-1') {
            $scope.curProduct = workWithList.getObjectByID($scope.lstProduct, productID, 'ProductID');
            appHelpers.setHash('proid', productID);
        } else {
            var curCat = $scope.objFilter.categoryID == 'all' ? '-1' : $scope.objFilter.categoryID;
            $scope.curProduct = { ProductID: '-1', CategoryID: curCat, Priority: 0, Label: '', OldPrice: 0, CurrentPrice: 0 };
        }

        $scope.objFilter.productID = productID;

        // #region Header For Desktop & Mobile

        // desktop
        var headerString = '<span class="btn btn-link toggle-content-image hidden-xs" ng-show="settings.productImage" ng-click="toggleDetailsImageForm(\'extends\')">' + shopText.mProduct.btnExtendsImages + '</span>';
        headerString += '<span class="btn btn-link toggle-content-image hidden-xs" ng-show="settings.detailsImage" ng-click="toggleDetailsImageForm(\'details\')">' + shopText.mProduct.btnDetailsImage + '</span>';
        headerString += '<span class="btn btn-link btn-save hidden-xs" ng-click="saveFormProduct()">' + shopText.global.btnSave + '</span>';

        // mobile
        headerString += '<span class="btn-btn-link dropdown settings-icon visible-xs">';
        headerString += '<a class="dropdown-toggle" data-toggle="dropdown" href="bootstrap-elements.html" data-target="#"></a>';
        headerString += '<ul class="dropdown-menu">';
        headerString += '<li ng-click="saveFormProduct()"><a>' + shopText.global.btnSave + '</a></li>';
        headerString += '<li ng-show="settings.productImage" ng-click="toggleDetailsImageForm(\'extends\')"><a>' + shopText.mProduct.btnExtendsImages + '</a></li>';
        headerString += '<li ng-show="settings.detailsImage" ng-click="toggleDetailsImageForm(\'details\')"><a>' + shopText.mProduct.btnDetailsImage + '</a></li>';
        headerString += '</ul>';
        headerString += '</span>';

        //#endregion

        $scope.objLB = $('body').ndtLightbox({
            title: shopText.mProduct.formProductTitle,
            headerContent: headerString,
            //fixWidth: '70%',
            //fixHeight: '600',
            allowFullScreen: true,
            outerClass: 'form-manage-product',
            content: $scope.tplFormProduct,
            onComplete: function onComplete() {
                setTimeout(function () {
                    $scope.objLB.find('.ndt-lb-ctn .my-dropdown').dropdown();
                    $scope.loadCkEditor();
                }, 500);
            },
            onSetContent: function onSetContent() {
                setTimeout(function () {
                    $scope.objLB.find('.ndt-lb-ctn .my-dropdown').dropdown();
                }, 500);
            },
            onDestroy: function onDestroy() {
                $scope.objLBDetailsImage.data('ndtLightbox').close();
                $scope.objLBExtendsImages.data('ndtLightbox').close();
                appHelpers.removeHash('proid');
                $scope.objFilter.productID = '-1';
            }
        }, $compile, $scope);

        if (productID == '-1') productID = $scope.genProductID;

        $scope.loadFormDetailsImage(productID);
        $scope.loadFormExtendsImages(productID);
    };

    //#endregion

    //#region Save Form Product

    $scope.saveFormProduct = function () {
        if ($scope.isSending) return;

        var valMsg = appHelpers.validate($scope.objLB);
        if (valMsg != 'passed') {
            appHelpers.alertCall(valMsg, 5000, 'alert-danger');
            return;
        }

        if ($scope.curProduct.ProductID == '-1') $scope.curProduct.ProductID = $scope.genProductID;

        if ($scope.ckeDetails) {
            $scope.curProduct.Details = $scope.ckeDetails.getData();
        }

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'update-product',
                data: JSON.stringify($scope.curProduct)
            }
        }).success(function (result) {
            $scope.isSending = false;
            appHelpers.removeOverlayLoading();

            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.genProductID = ctJson.genProductID;
                $scope.curProduct = ctJson.newProduct;

                if ($scope.objLB.find('.fulPrimaryImage').length > 0 && $scope.objLB.find('.fulPrimaryImage').val() != '') {
                    $scope.uploadFiles($scope.objLB.find('.fulPrimaryImage'), $scope.curProduct.ProductID, 'PrimaryImage');
                } else {
                    workWithList.AddObjectToList($scope.lstProduct, $scope.curProduct, 'ProductID');
                }

                $scope.objLB.data('ndtLightbox').close();
                appHelpers.alertCall(shopText.mProduct.updateSuccess, 5000);
            }
        });
    };

    //#endregion

    //#region Delete Product

    $scope.deleteProduct = function (productID) {
        if (!confirm(shopText.mProduct.confirmDelete)) return;

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'delete-product',
                data: productID
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                appHelpers.alertCall(shopText.mProduct.deleteComplete, 5000);
                workWithList.removeObjectByID($scope.lstProduct, productID, 'ProductID');
            }
        });
    };

    //#endregion

    //#region Load Form Extends Images

    $scope.loadFormExtendsImages = function (productID) {
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'load-product-image',
                data: productID
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.curLstExtendsImg = ctJson.curLstExtendsImg;

                $scope.objLBExtendsImages = $('body').ndtLightbox({
                    title: shopText.mProduct.extendsImageTitle,
                    //fixWidth: 200,
                    //fixHeight: '50%',
                    //dragable: true,
                    headerContent: '<span class="btn btn-link visible-xs" ng-click="toggleDetailsImageForm(\'extends\')">Close</span>',
                    withOverlay: false,
                    allowFullScreen: true,
                    outerClass: 'form-extends-images',
                    content: $scope.tplFormExtendsImage,
                    onComplete: function onComplete() {}
                }, $compile, $scope);
            }
        });
    };

    //#endregion

    //#region Upload Extends Image

    $scope.uploadExtendsImage = function () {
        var fileSelect = $scope.objLBExtendsImages.find('.fulProductImage');
        if (fileSelect.val() == '') {
            appHelpers.alertCall(shopText.mProduct.requireSelectFile, 5000, 'alert-danger');
            return;
        }

        var ProductID = $scope.curProduct.ProductID;
        var GroupName = 'global';

        if (ProductID == "-1") {
            ProductID = $scope.genProductID;
            GroupName = 'temp';
        }

        $scope.uploadFiles(fileSelect, ProductID, 'ProductImage', GroupName);
    };

    //#endregion

    //#region Delete Extends Image

    $scope.deleteExtendsImage = function (contentID) {
        if (!confirm(shopText.mProduct.confirmDeleteImage)) return;

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'delete-product-image',
                data: contentID
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                appHelpers.alertCall(shopText.mProduct.deleteImageComplete, 5000);
                workWithList.removeObjectByID($scope.curLstExtendsImg, contentID, 'ContentID');
            }
        });
    };

    //#endregion

    //#region Load Form Details Image

    $scope.loadFormDetailsImage = function (productID) {
        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'load-details-image',
                data: productID
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                $scope.curLstDetailsImage = ctJson.curLstDetailsImage;

                $scope.objLBDetailsImage = $('body').ndtLightbox({
                    title: shopText.mProduct.btnDetailsImage,
                    //fixWidth: 200,
                    //fixHeight: '50%',
                    //dragable: true,
                    headerContent: '<span class="btn btn-link visible-xs" ng-click="toggleDetailsImageForm(\'details\')">Close</span>',
                    withOverlay: false,
                    allowFullScreen: true,
                    outerClass: 'form-details-image',
                    content: $scope.tplFormDetailsImage,
                    onComplete: function onComplete() {}
                }, $compile, $scope);
            }
        });
    };

    //#endregion

    //#region Upload Details Image

    $scope.uploadDetailsImage = function () {
        var fileSelect = $scope.objLBDetailsImage.find('.fulContentImage');
        if (fileSelect.val() == '') {
            appHelpers.alertCall(shopText.mProduct.requireSelectFile, 5000, 'alert-danger');
            return;
        }

        var ProductID = $scope.curProduct.ProductID;
        var GroupName = 'global';

        if (ProductID == "-1") {
            ProductID = $scope.genProductID;
            GroupName = 'temp';
        }

        $scope.uploadFiles(fileSelect, ProductID, 'DetailsImage', GroupName);
    };

    //#endregion

    //#region Delete Details Image

    $scope.deleteDetailsImage = function (contentID) {
        if (!confirm(shopText.mProduct.confirmDeleteImage)) return;

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'delete-details-image',
                data: contentID
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                appHelpers.alertCall(shopText.mProduct.deleteImageComplete, 5000);
                workWithList.removeObjectByID($scope.curLstDetailsImage, contentID, 'ContentID');
            }
        });
    };

    //#endregion

    //#region Upload Files

    // fileType: PrimaryImage | ProductImage | DetailsImage
    $scope.uploadFiles = function (objSelectFile, productID, fileType, groupName) {
        if ($scope.isSending) return;

        if (!groupName) groupName = 'global';

        var objQuery = {
            productID: productID,
            fileType: fileType,
            groupName: groupName
        };

        var objForm = new FormData();
        objForm.append('act', 'upload-files');
        objForm.append('data', JSON.stringify(objQuery));

        var listFiles = objSelectFile[0].files;

        if (listFiles.length > 0) {
            for (var i = 0; i < listFiles.length; i++) {
                objForm.append('file' + i, listFiles[i]);
            }
        }

        $scope.isSending = true;
        appHelpers.callOverlayLoading();
        $.ajax({
            url: window.location.href,
            type: 'POST',
            contentType: false,
            processData: false,
            data: objForm,
            success: function success(result) {
                var ctJson = angular.fromJson(result);
                if (ctJson.error) appHelpers.alertCall(ctJson.error, 15000, 'alert-danger');else {
                    if (fileType == 'PrimaryImage') {
                        $scope.curProduct.PrimaryImage = ctJson.imgName;
                        workWithList.AddObjectToList($scope.lstProduct, $scope.curProduct, 'ProductID');
                    } else if (fileType == 'ProductImage') {
                        appHelpers.alertCall('Upload file thành công', 5000);
                        var lstUploaded = ctJson.lstImage;

                        angular.forEach(lstUploaded, function (objImg) {
                            workWithList.AddObjectToList($scope.curLstExtendsImg, objImg, 'ContentID');
                        });
                    } else if (fileType == 'DetailsImage') {
                        appHelpers.alertCall(shopText.mProduct.uploadImageSuccess, 5000);
                        var lstUploaded = ctJson.lstImage;

                        angular.forEach(lstUploaded, function (objImg) {
                            workWithList.AddObjectToList($scope.curLstDetailsImage, objImg, 'ContentID');
                        });
                    }

                    $scope.$apply();

                    objSelectFile.parents('.input-group').find('.browse-file-name').val('');
                    objSelectFile.val('');
                }

                $scope.isSending = false;
                appHelpers.removeOverlayLoading();
            }
        });
    };

    //#endregion

    $scope.init();
}]);


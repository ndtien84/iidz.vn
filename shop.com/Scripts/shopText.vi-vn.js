﻿var shopText = {
    // global
    global: {
        btnSave: 'Lưu',
        btnDelete: 'Xóa'
    },

    // manageUser.js
    manageUser: {
        updateSuccess: 'Cập nhật User thành công',
        confirmDelete: 'Xóa user ?',
        deleteComplete: 'Xóa user thành công',
        requireKeyword: 'Bạn chưa nhập từ khóa',
        template: {
            headerTitle: 'Danh sách user',
            resetPassPlaceholder: 'Reset password, để trống nếu không reset',
            btnDelete: 'Xóa',
            btnUpdate: 'Cập nhật',
            noUser: 'Không có user',
            searchBy: 'Tìm theo',
            keywordPlaceHolder: 'Nhập từ khóa tìm kiếm'
        }
    },

    //mBanner.js
    mBanner: {
        bannerPageTitle: 'Quản trị banner',
        requireSelectFile: 'Bạn chưa chọn file',
        updateSuccess: 'Cập nhật banner thành công',
        confirmDelete: 'Xóa hình ảnh ?',
        deleteComplete: 'Xóa thành công',
        template: {
            imageSize: 'Chọn hình ảnh (kích thước tốt nhất 1280 x 400 pixel)',
            priority: 'Thứ tự sắp xếp',
            selectImage: 'Chọn hình ảnh',
            btnDelete: 'Xóa',
            btnUpdate: 'Cập nhật'
        }
    },

    // mCategory.js
    mCategory: {
        productTitle: 'Quản trị danh mục sản phẩm',
        newsTitle: 'Quản trị danh mục tin tức',
        lightboxTitle: 'Danh mục',
        updateSuccess: 'Cập nhật danh mục thành công',
        confirmDelete: 'Xóa danh mục ?',
        deleteComplete: 'Xóa danh mục thành công',
        template: {
            btnCreate: 'Thêm mới Danh Mục',
            headerName: 'Tên danh mục',
            btnDelete: 'Xóa',
            btnEdit: 'Sửa'
        }
    },

    // mContent.js
    mContent: {
        newsTitle: 'Quản trị tin tức',
        lightboxTitle: 'Thông tin',
        btnExtendsImage: 'Hình ảnh phụ',
        btnDetailsImage: 'Hình ảnh nội dung',
        updateSuccess: 'Cập nhật thành công',
        confirmDelete: 'Xóa nội dung ?',
        deleteComplete: 'Xóa thành công',
        extendsImageTitle: 'Hình ảnh phụ',
        requireSelectFile: 'Bạn chưa chọn file',
        extendsImageTitle: 'Hình ảnh phụ',
        confirmDeleteImage: 'Xóa hình ảnh ?',
        deleteImageComplete: 'Xóa hình ảnh thành công',
        uploadImageSuccess: 'Upload hình ảnh thành công',
        requiredKeyword: 'Nhập từ khóa để tìm kiếm',
        template: {
            btnCreate: 'Thêm mới',
            headerImage: 'Hình ảnh',
            headerInfo: 'Thông tin',
            priority: 'Thứ tự sắp xếp',
            btnDelete: 'Xóa',
            btnEdit: 'Chỉnh sửa',
            btnImage: 'Hình ảnh',
            btnSetPrimary: 'Set primary',
            noCategory: 'Không có danh mục'
        }
    },

    // mOrders.js
    mOrders: {
        pageTitle: 'Quản trị đơn hàng',
        lightboxTitle: 'Chi tiết Đơn hàng',
        requiredKeyword: 'Nhập từ khóa để tìm kiếm',
        template: {
            fromDate: 'Từ ngày',
            toDate: 'Đến ngày',
            btnGetByDate: 'Xem theo ngày',
            noOrders: 'Không có đơn hàng nào',
            order: 'Đơn hàng',
            ordersInfo: 'Thông tin đơn hàng',
            createDate: 'Ngày',
            totalPrice: 'Giá tiền',
            searchBy: 'Tìm theo',
            keyword: 'Từ khóa',
            keywordPlaceHolder: 'Nhập từ khóa để tìm',
            name: 'Tên',
            phone: 'Điện thoại',
            notes: 'Ghi chú',
            orderid: 'Mã đơn hàng',
            btnViewDetails: 'Chi tiết'
        }
    },

    // mProduct.js
    mProduct: {
        pageTitle: 'Quản trị sản phẩm',
        formProductTitle: 'Thông tin sản phẩm',
        btnExtendsImages: 'Hình ảnh phụ',
        btnDetailsImage: 'Hình ảnh nội dung',
        updateSuccess: 'Cập nhật sản phẩm thành công',
        confirmDelete: 'Xóa sản phẩm ?',
        deleteComplete: 'Xóa sản phẩm thành công',
        extendsImageTitle: 'Hình ảnh phụ',
        requireSelectFile: 'Chưa chọn file',
        confirmDeleteImage: 'Xóa hình ảnh ?',
        deleteImageComplete: 'Xóa hình ảnh thành công',
        uploadImageSuccess: 'Upload hình ảnh thành công',
        requiredKeyword: 'Nhập từ khóa để tìm kiếm',
        template: {
            btnCreate: 'Thêm mới Sản Phẩm',
            headerImage: 'Hình ảnh',
            headerInfo: 'Thông tin',
            oldPrice: 'Giá cũ',
            currentPrice: 'Giá hiện tại',
            priority: 'Thứ tự sắp xếp',
            btnEdit: 'Sửa',
            btnDelete: 'Xóa',
            searchBy: 'Tìm kiếm',
            keywordPlaceHolder: 'Nhập tên hoặc mã sản phẩm',
            btnSetPrimary: 'Set primary',
            allProducts: 'Tất cả sản phẩm',
            noCategory: 'Không có danh mục'
        }
    },

    // siteSettings.js
    siteSettings: {
        updateSuccess: 'Cập nhật thông tin thành công',
        requireSelectLogo: 'Chọn logo để upload',
        template: {
            headerTitle: 'Chỉnh sửa thông tin website',
            selectLogo: 'Chọn logo để upload',
            btnUploadLogo: 'Upload Logo',
            companyName: 'Tên công ty',
            title: 'Tiêu đề website',
            address: 'Địa chỉ',
            phone: 'Điện thoại',
            cellPhone: 'Di động',
            btnSave: 'Lưu'
        }
    },

    // singleContent.js
    singleContent: {
        aboutTitle: 'Giới thiệu',
        template: {
            btnDetailsImage: 'Hình ảnh cho nội dung'
        }
    },

    // datetime picker format (available for vi-vn only)
    datePickerFormat: {
        dateTimeFormat: "dd-MM-yyyy HH:mm:ss",
        dateFormat: "dd-MM-yyyy",
        shortDayNames: ["Chủ Nhật", "Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7"],
        fullDayNames: ["Chủ Nhật", "Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7"],
        shortMonthNames: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
        fullMonthNames: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
        titleContentDateTime: "Chọn ngày giờ",
        titleContentDate: "Chọn ngày",
        titleContentTime: "Chọn giờ"
    }
};
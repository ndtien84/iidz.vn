﻿var shopText = {
    // global
    global: {
        btnSave: 'Save',
        btnDelete: 'Delete'
    },

    // manageUser.js
    manageUser: {
        updateSuccess: 'Update user success',
        confirmDelete: 'Delete this user ?',
        deleteComplete: 'Delete user success',
        requireKeyword: 'Please enter keyword',
        template: {
            headerTitle: 'List users',
            resetPassPlaceholder: 'Reset password, leave blank if not reset',
            btnDelete: 'Delete',
            btnUpdate: 'Update',
            noUser: 'No User found',
            searchBy: 'Search by',
            keywordPlaceHolder: 'Enter keyword to search'
        }
    },

    //mBanner.js
    mBanner: {
        bannerPageTitle: 'Manage banner',
        requireSelectFile: 'Please select image',
        updateSuccess: 'Update banner success',
        confirmDelete: 'Delete image ?',
        deleteComplete: 'Delete success',
        template: {
            imageSize: 'Select image (best size 1280 x 400 pixel)',
            priority: 'Priority',
            selectImage: 'Select image',
            btnDelete: 'Delete',
            btnUpdate: 'Update'
        }
    },

    // mCategory.js
    mCategory: {
        productTitle: 'Manage product category',
        newsTitle: 'Manage news category',
        lightboxTitle: 'Category',
        updateSuccess: 'Update category success',
        confirmDelete: 'Delete category ?',
        deleteComplete: 'Delete category success',
        template: {
            btnCreate: 'Create new category',
            headerName: 'Category name',
            btnDelete: 'Delete',
            btnEdit: 'Edit'
        }
    },

    // mContent.js
    mContent: {
        newsTitle: 'Manage news',
        lightboxTitle: 'Information',
        btnExtendsImage: 'Extends images',
        btnDetailsImage: 'Content images',
        updateSuccess: 'Update success',
        confirmDelete: 'Delete content ?',
        deleteComplete: 'Delete success',
        extendsImageTitle: 'Extends images',
        requireSelectFile: 'Please select image',
        extendsImageTitle: 'Extends images',
        confirmDeleteImage: 'Delete image ?',
        deleteImageComplete: 'Delete image success',
        uploadImageSuccess: 'Upload image success',
        requiredKeyword: 'Enter keyword',
        template: {
            btnCreate: 'Create new',
            headerImage: 'Image',
            headerInfo: 'Information',
            priority: 'Priority',
            btnDelete: 'Delete',
            btnEdit: 'Edit',
            btnImage: 'Images',
            btnSetPrimary: 'Set primary',
            noCategory: 'No Category'
        }
    },

    // mOrders.js
    mOrders: {
        pageTitle: 'Manage orders',
        lightboxTitle: 'Orders details',
        requiredKeyword: 'Enter keyword',
        template: {
            fromDate: 'From date',
            toDate: 'To date',
            btnGetByDate: 'Get by date',
            noOrders: 'No orders found',
            order: 'Order',
            ordersInfo: 'Information',
            createDate: 'Create date',
            totalPrice: 'Total price',
            searchBy: 'Search by',
            keyword: 'Keyword',
            keywordPlaceHolder: 'Enter keyword ...',
            name: 'Name',
            phone: 'Phone',
            notes: 'Notes',
            orderid: 'Invoice / Order ID',
            btnViewDetails: 'Details'
        }
    },

    // mProduct.js
    mProduct: {
        pageTitle: 'Manage product',
        formProductTitle: 'Product information',
        btnExtendsImages: 'Extends images',
        btnDetailsImage: 'Content images',
        updateSuccess: 'Update product success',
        confirmDelete: 'Delete product ?',
        deleteComplete: 'Delete product success',
        extendsImageTitle: 'Extends images',
        requireSelectFile: 'Please select image',
        confirmDeleteImage: 'Delete image ?',
        deleteImageComplete: 'Delete image success',
        uploadImageSuccess: 'Upload image success',
        requiredKeyword: 'Enter keyword',
        template: {
            btnCreate: 'Create new product',
            headerImage: 'Image',
            headerInfo: 'Information',
            oldPrice: 'Old price',
            currentPrice: 'Current price',
            priority: 'Priority',
            btnEdit: 'Edit',
            btnDelete: 'Delete',
            searchBy: 'Search',
            keywordPlaceHolder: 'Enter product name or SKU',
            btnSetPrimary: 'Set primary',
            allProducts: 'All Products',
            noCategory: 'No Category'
        }
    },

    // siteSettings.js
    siteSettings: {
        updateSuccess: 'Update success',
        requireSelectLogo: 'Please select logo to upload',
        template: {
            headerTitle: 'Website information',
            selectLogo: 'Select logo from your computer',
            btnUploadLogo: 'Upload Logo',
            companyName: 'Company name',
            title: 'Title',
            address: 'Address',
            phone: 'Phone',
            cellPhone: 'Cellphone',
            btnSave: 'Save'
        }
    },

    // singleContent.js
    singleContent: {
        aboutTitle: 'About us',
        template: {
            btnDetailsImage: 'Details images'
        }
    }
};
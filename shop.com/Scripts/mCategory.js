﻿'use strict';

ndtApp.controller('mcCtrl', ['$scope', '$http', '$compile', '$filter', 'workWithList', 'dateHelpers', function ($scope, $http, $compile, $filter, workWithList, dateHelpers) {
    $scope.ctn = $('.manage-category-ctn');
    $scope.objLB;

    $scope.tplFormCategory = '';
    $scope.lstCategory = [];
    $scope.curCat = {};

    $scope.curType = '';
    $scope.i18n = shopText.mCategory.template;

    //#region Init & Utilities

    $scope.init = function () {
        $scope.curType = appHelpers.queryString('type');
        if (!$scope.curType) $scope.curType = 'product';

        if ($scope.curType == 'product') {
            $('.admin-header-ctn .page-title').html(shopText.mCategory.productTitle);
        } else if ($scope.curType == "news") {
            $('.admin-header-ctn .page-title').html(shopText.mCategory.newsTitle);
        }

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'load-init',
                data: $scope.curType
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) console.log(ctJson.error);else {
                $scope.tplFormCategory = ctJson.tplFormCategory;
                $scope.lstCategory = ctJson.lstCategory;
            }
        });
    };

    $scope.getLevelClass = function (level) {
        var className = '';

        if (level == 1) className = 'list-group-item-success level-1';else if (level == 2) className = 'list-group-item-info level-2';else if (level == 3) className = 'list-group-item-warning level-3';else if (level == 4) className = 'llist-group-item-danger level-4';

        return className;
    };

    //#endregion

    //#region Load Form Category

    $scope.loadFormCategory = function (categoryID) {
        if (categoryID != '-1') {
            $scope.curCat = workWithList.getObjectByID($scope.lstCategory, categoryID, 'CategoryID');
            //$scope.curCat = dateHelpers.deleteDateField($scope.curCat);
        } else {
                $scope.curCat = { CategoryID: '-1', ParentsID: '-1', Priority: 0 };
            }

        $scope.objLB = $('body').ndtLightbox({
            title: shopText.mCategory.lightboxTitle,
            headerContent: '<span class="btn btn-link btn-save" ng-click="saveFormCategory()">' + shopText.global.btnSave + '</span>',
            fixWidth: 600,
            fixHeight: 500,
            dragable: true,
            outerClass: 'form-manage-category',
            withTab: false,
            content: $scope.tplFormCategory,
            onComplete: function onComplete() {
                setTimeout(function () {
                    $scope.objLB.find('.my-dropdown').dropdown();
                }, 500);
            },
            onSetContent: function onSetContent() {
                setTimeout(function () {
                    $scope.objLB.find('.my-dropdown').dropdown();
                }, 500);
            }
        }, $compile, $scope);
    };

    //#endregion

    //#region Save Form Category

    $scope.saveFormCategory = function () {
        var valMesg = appHelpers.validate($scope.objLB);
        if (valMesg != 'passed') {
            appHelpers.alertCall(valMesg, 5000, 'alert-danger');
            return;
        }

        $scope.curCat.CategoryType = $scope.curType;

        //console.log(angular.toJson($scope.curCat, true));

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'update-category',
                data: JSON.stringify($scope.curCat)
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) console.log(ctJson.error);else {
                //console.log(angular.toJson(ctJson.model, true));

                // return a whole list (BAD)
                $scope.lstCategory = ctJson.lstCategory;
                appHelpers.alertCall(shopText.mCategory.updateSuccess, 5000);

                if ($scope.curCat.CategoryID != '-1') $scope.objLB.data('ndtLightbox').close();else {
                    $scope.curCat = { CategoryID: '-1', ParentsID: '-1', Priority: 0 };
                    $scope.objLB.data('ndtLightbox').setContent(null, null, $scope.tplFormCategory);
                }
            }
        });
    };

    //#endregion

    //#region Delete Category

    $scope.deleteCategory = function (categoryID) {
        if (!confirm(shopText.mCategory.confirmDelete)) return;

        $http({
            method: 'POST',
            url: window.location.href,
            data: {
                act: 'delete-category',
                data: categoryID
            }
        }).success(function (result) {
            var ctJson = angular.fromJson(result);
            if (ctJson.error) console.log(ctJson.error);else {
                // return whole list (BAD)
                $scope.lstCategory = ctJson.lstCategory;
                appHelpers.alertCall(shopText.mCategory.deleteComplete, 5000);
            }
        });
    };

    //#endregion

    $scope.init();
}]);


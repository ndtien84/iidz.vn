﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Reflection;

using System.Text.RegularExpressions;

namespace shop.com.Services
{
    public class CBO
    {
        #region Propeties & Constructor

        public string ConStr { get; set; }

        public CBO()
        {
            this.ConStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
        }

        #endregion

        public List<Hashtable> GetQuery(string SqlCommand)
        {
            IDataReader dr = SqlHelper.ExecuteReader(ConStr, CommandType.Text, SqlCommand);

            List<Hashtable> ListReturn = new List<Hashtable>();

            Hashtable ObjR;

            if (dr != null)
            {
                while (dr.Read())
                {
                    ObjR = new Hashtable();

                    for (int i = 0; i < dr.FieldCount; i++)
                    {
                        ObjR.Add(dr.GetName(i), dr.GetValue(i));
                    }

                    ListReturn.Add(ObjR);
                }
            }

            dr.Close();

            return ListReturn;
        }

        public void CreatePro(string SqlCommand)
        {
            // use regex.split to ignore case
            // \\bGO\\b : regex pattern : match exact GO
            // select cate[go]ry : not match go
            // select .... go : match

            string[] ArrCommand = Regex.Split(SqlCommand, "\\bGO\\b", RegexOptions.IgnoreCase); 
            foreach (string SingleCommand in ArrCommand)
            {
                if(SingleCommand.Trim() != "")
                {
                    SqlHelper.ExecuteNonQuery(ConStr, CommandType.Text, SingleCommand);
                }
            }
        }

        #region Support Method

        private int[] GetOrdinals(ArrayList objProperties, IDataReader dr)
        {
            int[] arrOrdinals = new int[objProperties.Count + 1];

            if (dr != null)
            {
                for (int intProperty = 0; intProperty < objProperties.Count; intProperty++)
                {
                    arrOrdinals[intProperty] = -1;
                    try
                    {
                        arrOrdinals[intProperty] = dr.GetOrdinal(((PropertyInfo)objProperties[intProperty]).Name);
                    }
                    catch (Exception ex)
                    {
                        // property does not exist in datareader                        
                    }
                }
            }

            return arrOrdinals;
        }

        private ArrayList GetPropertyInfo(Type objType)
        {
            ArrayList objProperties = new ArrayList();
            foreach (PropertyInfo objProperty in objType.GetProperties())
            {
                objProperties.Add(objProperty);
            }

            return objProperties;
        }

        private T CreateObject<T>(IDataReader dr)
        {
            T objObject = Activator.CreateInstance<T>();

            // get properties for type
            ArrayList objProperties = GetPropertyInfo(objObject.GetType());

            // get ordinal positions in datareader
            int[] arrOrdinals = GetOrdinals(objProperties, dr);

            // fill object with values from datareader
            for (int intProperty = 0; intProperty <= objProperties.Count - 1; intProperty++)
            {
                PropertyInfo objPropertyInfo = (PropertyInfo)objProperties[intProperty];
                if (objPropertyInfo.CanWrite)
                {
                    try
                    {
                        objPropertyInfo.SetValue(objObject, dr.GetValue(arrOrdinals[intProperty]), null);
                    }
                    catch (Exception ex) { }
                }
            }

            return objObject;
        }

        #endregion


        #region Get Object

        public T GetObject<T>(string ProcName, params object[] paras)
        {
            T ObjReturn = default(T);
            IDataReader dr = SqlHelper.ExecuteReader(ConStr, ProcName, paras);

            if (dr != null)
            {
                while (dr.Read())
                {
                    ObjReturn = CreateObject<T>(dr);
                }
            }

            dr.Close();

            return ObjReturn;
        }

        #endregion

        #region Get List Object

        public List<T> GetListObjects<T>(string ProcName, params object[] paras)
        {
            List<T> ListReturn = new List<T>();
            IDataReader dr = SqlHelper.ExecuteReader(ConStr, ProcName, paras);

            if (dr != null)
            {
                while (dr.Read())
                {
                    T objFillObject = CreateObject<T>(dr);
                    ListReturn.Add(objFillObject);
                }
            }

            dr.Close();

            return ListReturn;
        }

        #endregion

        #region Execute Non Query

        public int ExecuteNonQuery(string ProcName, params object[] paras)
        {
            return SqlHelper.ExecuteNonQuery(ConStr, ProcName, paras);
        }

        #endregion
    }
}
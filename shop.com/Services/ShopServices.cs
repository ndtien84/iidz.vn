﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data;
using System.Linq;

using shop.com.Models;
using System.Collections;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Dynamic;

using System.Web;
using shop.com.Utils;

namespace shop.com.Services
{
    public class ShopServices: Controller
    {
        private int TotalRow = 0;
        private int TotalPage = 0;
        private CBO DBCtrl = new CBO();
        private SContext DBContext = new SContext();
        private CartContext CartDB = new CartContext();

        public string UserID { get; set; }
        public HttpRequestBase CurrentRequest { get; set; }

        public ShopServices()
        {
            UserID = "-1";
        }


        #region Utilities

        #region Move CartTemp & Order to new Login or Register User

        public void Uti_MoveDataUserTemp(string TempUserID, string NewUserID)
        {
            // Cart Temp
            List<S_CartTempModel> ListCartTemp = CartDB.ListCartTemp.Where(ct => ct.UserID == TempUserID).ToList();
            if (ListCartTemp.Count > 0)
            {
                foreach (S_CartTempModel item in ListCartTemp)
                {
                    item.UserID = NewUserID;
                    S_CartTemp_Update(item);
                }
            }

            // Orders
            //List<S_OrderModel> ListOrders = S_Order_GetList(TempUserID, 1000, 1, "all", "all", "all", DateTime.Now, DateTime.Now, "no", "CreateDate desc", ref TotalRow, ref TotalPage, false);
            List<S_OrderModel> ListOrders = S_Order_GetList(
                TotalPage: ref TotalPage,
                TotalRow: ref TotalRow,
                FromDate: DateTime.Now,
                ToDate: DateTime.Now,
                PageSize: 1000,
                UserID: TempUserID,
                WithListDetails: false
            );

            if (ListOrders.Count > 0)
            {
                foreach (S_OrderModel item in ListOrders)
                {
                    item.UserID = NewUserID;
                    S_Order_Update(item);
                }
            }
        }

        #endregion

        #endregion

        #region S_Settings (USING ENTITY FRAMEWORK)

        public void S_Settings_Update(S_SettingsModel Model)
        {
            //using (SContext MyCtx = new SContext())
            //{
            //    if (MyCtx.ListSettings.Where(s => s.SKey == Model.SKey && s.SType == Model.SType).Count() > 0)
            //    {
            //        MyCtx.Entry(Model).State = EntityState.Modified;
            //    }
            //    else
            //    {
            //        MyCtx.ListSettings.Add(Model);
            //    }

            //    MyCtx.SaveChanges();
            //}

            if (DBContext.ListSettings.Where(s => s.SKey == Model.SKey && s.SType == Model.SType).Count() > 0)
            {
                DBContext.Entry(Model).State = EntityState.Modified;
            }
            else
            {
                DBContext.ListSettings.Add(Model);
            }

            DBContext.SaveChanges();
        }
        public void S_Settings_Delete(string SKey, string SType)
        {
            //using (SContext MyCtx = new SContext())
            //{
            //    MyCtx.ListSettings.Remove(
            //    MyCtx.ListSettings.Where(s => s.SKey == SKey && s.SType == SType).First());
            //    MyCtx.SaveChanges();
            //}

            DBContext.ListSettings.Remove(
                DBContext.ListSettings.Where(s => s.SKey == SKey && s.SType == SType).First()
            );

            DBContext.SaveChanges();
        }
        public S_SettingsModel S_Settings_GetByID(string SKey, string SType)
        {
            //using (SContext MyCtx = new SContext())
            //{
            //    var Query = MyCtx.ListSettings.Where(s => s.SKey == SKey && s.SType == SType);
            //    if (Query.Count() > 0)
            //    {
            //        return Query.First();
            //    }
            //    else
            //    {
            //        return null;
            //    }
            //}

            var Query = DBContext.ListSettings.Where(s => s.SKey == SKey && s.SType == SType);
            if (Query.Count() > 0)
            {
                return Query.First();
            }
            else
            {
                return null;
            }

            //return DBContext.ListSettings.Where(s => s.SKey == SKey && s.SType == SType).First();
        }
        public Hashtable S_Settings_GetList(string SType)
        {
            //using (SContext MyCtx = new SContext())
            //{
            //    Hashtable STable = new Hashtable();
            //    List<S_SettingsModel> ListS = MyCtx.ListSettings.Where(s => s.SType == SType).ToList();

            //    if (ListS != null && ListS.Count > 0)
            //    {
            //        foreach (S_SettingsModel item in ListS)
            //        {
            //            STable.Add(item.SKey, item.SValue);
            //        }
            //    }

            //    return STable;
            //}

            Hashtable STable = new Hashtable();
            List<S_SettingsModel> ListS = DBContext.ListSettings.Where(s => s.SType == SType).ToList();

            if (ListS != null && ListS.Count > 0)
            {
                foreach (S_SettingsModel item in ListS)
                {
                    STable.Add(item.SKey, item.SValue);
                }
            }

            return STable;
        }

        #endregion

        #region S_Category

        public List<S_CategoryModel> S_Category_Update(S_CategoryModel Model)
        {
            if (Model.CategoryID == "-1") Model.CategoryID = Helpers.GenerateID();
            Model.UrlKey = Helpers.ConvertASCIIChar(Model.Name);
            return DBCtrl.GetListObjects<S_CategoryModel>("S_Category_Update", Model.CategoryID, Model.CultureCode, Model.CategoryType, Model.GroupName, Model.ParentsID, Model.PrimaryImage, Model.SecondaryImage, Model.PrimaryDocument, Model.SecondaryDocument, Model.JsonGlobal, Model.Label, Model.Priority, Model.Visible, Model.IsDeleted, Model.StartDate, Model.EndDate, Model.CreateBy, Model.LastUpdateBy, Model.Name, Model.UrlKey, Model.JsonCulture, Model.Overview, Model.Information, Model.Summary, Model.Details);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CategoryID"></param>
        /// <param name="Permanant"></param>
        /// <returns></returns>
        public int S_Category_DeleteByID(string CategoryID, bool Permanant)
        {
            return DBCtrl.ExecuteNonQuery("S_Category_DeleteByID", CategoryID, Permanant);
        }

        public void S_Category_DeleteByIDAndAllSub(string CategoryID, bool Permanant)
        {
            DBCtrl.ExecuteNonQuery("S_Category_DeleteByID", CategoryID, Permanant);

            List<S_CategoryModel> ListChild = S_Category_GetList(ParentsID: CategoryID);
            if (ListChild != null && ListChild.Count > 0)
            {
                foreach (S_CategoryModel item in ListChild)
                {
                    S_Category_DeleteByIDAndAllSub(item.CategoryID, Permanant);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CategoryID"></param>
        /// <param name="CultureCode">all | vi-vn | en-us | ...</param>
        /// <returns></returns>
        public List<S_CategoryModel> S_Category_GetByID(string CategoryID, string CultureCode)
        {
            return DBCtrl.GetListObjects<S_CategoryModel>("S_Category_GetByID", CategoryID, CultureCode);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="GetType">full : get all Column | short : get CategoryID, CultureCode, Name</param>
        /// <param name="CultureCode">all | CultureCode</param>
        /// <param name="CategoryType">all | product | news | ... </param>
        /// <param name="GroupName">all | GroupName</param>
        /// <param name="CreateBy">all | UserID</param>
        /// <param name="ParentsID">all | ParentsID</param>
        /// <param name="Label">all | Label</param>
        /// <param name="Visible">all | True | False</param>
        /// <param name="IsDeleted">all | True | False</param>
        /// <param name="ByDateRange">True : get by StartDate Now EndDate</param>
        /// <param name="OrderBy">ColumnName with desc/asc</param>
        /// <returns></returns>
        public List<S_CategoryModel> S_Category_GetList(GetListType GetType = GetListType.full, string CultureCode = "default", CategoryType CategoryType = CategoryType.product, string GroupName = "all", string CreateBy = "all", string ParentsID = "all", string Label = "all", Visible Visible = Visible.True, IsDeleted IsDeleted = IsDeleted.False, bool ByDateRange = false, CategoryColumns OrderByColumn = CategoryColumns.Priority, OrderDirection Direction = OrderDirection.desc)
        {
            if (CultureCode == "default") CultureCode = Helpers.DefaultCultureCode;

            return DBCtrl.GetListObjects<S_CategoryModel>("S_Category_GetList", GetType.ToString(), CultureCode, CategoryType.ToString(), GroupName, CreateBy, ParentsID, Label, Visible.ToString(), IsDeleted.ToString(), ByDateRange, OrderByColumn.ToString() + " " + Direction.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="GetType">full : get all Column | short : get CategoryID, CultureCode, Name</param>
        /// <param name="CultureCode">all | CultureCode</param>
        /// <param name="CategoryType">all | product | news | ... </param>
        /// <param name="GroupName">all | GroupName</param>
        /// <param name="CreateBy">all | UserID</param>
        /// <param name="ParentsID">all | ParentsID</param>
        /// <param name="Label">all | Label</param>
        /// <param name="Visible">all | True | False</param>
        /// <param name="IsDeleted">all | True | False</param>
        /// <param name="ByDateRange">True : get by StartDate Now EndDate</param>
        /// <param name="OrderBy">ColumnName with desc/asc</param>
        /// <param name="ListCategory">can be null</param>
        /// <param name="Level">init : -1</param>
        /// <param name="CurrentCategoryID">selected CategoryID</param>
        /// <returns></returns>
        public List<S_CategoryModel> S_Category_GetListAllLevel(GetListType GetType = GetListType.full, string CultureCode = "default", CategoryType CategoryType = CategoryType.product, string GroupName = "all", string CreateBy = "all", string ParentsID = "all", string Label = "all", Visible Visible = Visible.True, IsDeleted IsDeleted = IsDeleted.False, bool ByDateRange = false, CategoryColumns OrderByColumn = CategoryColumns.Priority, OrderDirection Direction = OrderDirection.desc, List<S_CategoryModel> ListCategory = null, int Level = 0, string CurrentCategoryID = "-1")
        {
            if (ListCategory == null) ListCategory = new List<S_CategoryModel>();
            string ClassName = "";

            List<S_CategoryModel> ListTemp = S_Category_GetList(
                GetType: GetType,
                CultureCode: CultureCode,
                CategoryType: CategoryType,
                GroupName: GroupName,
                CreateBy: CreateBy,
                ParentsID: ParentsID,
                Label: Label,
                Visible: Visible,
                IsDeleted: IsDeleted,
                ByDateRange: ByDateRange,
                OrderByColumn: OrderByColumn,
                Direction: Direction
            );
            
            if (ListTemp != null && ListTemp.Count > 0)
            {
                if (ListCategory.Count > 0)
                {
                    ListCategory[ListCategory.Count - 1].ClassName += " has-child";
                }

                int SiblingIndex = 0;
                Level++;

                foreach (S_CategoryModel item in ListTemp)
                {
                    #region Calculate ClassName

                    ClassName = "category-item category-level-" + Level.ToString() + " ";

                    if (SiblingIndex == 0)
                    {
                        ClassName += "item-first ";
                    }
                    else if (SiblingIndex == ListTemp.Count - 1)
                    {
                        ClassName += "item-last ";
                    }

                    if (SiblingIndex % 2 == 0)
                    {
                        ClassName += "item-even ";
                    }
                    else
                    {
                        ClassName += "item-odd ";
                    }

                    ClassName += "item-" + SiblingIndex.ToString();

                    if (item.CategoryID == CurrentCategoryID)
                    {
                        ClassName += " item-selected";
                    }

                    #endregion

                    item.Level = Level;
                    item.SiblingIndex = SiblingIndex;
                    item.ClassName = ClassName;

                    ListCategory.Add(item);
                    SiblingIndex++;

                    S_Category_GetListAllLevel(
                        GetType: GetType,
                        CultureCode: CultureCode,
                        CategoryType: CategoryType,
                        GroupName: GroupName,
                        CreateBy: CreateBy,
                        ParentsID: item.CategoryID,
                        Label: Label,
                        Visible: Visible,
                        ByDateRange: ByDateRange,
                        OrderByColumn: OrderByColumn,
                        Direction: Direction,
                        ListCategory: ListCategory,
                        Level: Level,
                        CurrentCategoryID: CurrentCategoryID
                    );

                    //S_Category_GetListAllLevel(GetType, CultureCode, CategoryType, GroupName, CreateBy, item.CategoryID, Label, Visible, ByDateRange, OrderBy, ListCategory, Level, CurrentCategoryID);
                }
            }

            return ListCategory;
        }

        #endregion

        #region S_Product

        public List<S_ProductModel> S_Product_Update(S_ProductModel Model)
        {
            bool IsAdd = true;

            if (Model.ProductID == "-1") Model.ProductID = Helpers.GenerateID();
            Model.UrlKey = Helpers.ConvertASCIIChar(Model.Name);


            List<S_ProductModel> PrevModel = S_Product_GetByID(Model.ProductID, Model.CultureCode, false);
            if (PrevModel != null && PrevModel.Count > 0)
            {
                IsAdd = false;
            }

            List<S_ProductModel> NewModel = DBCtrl.GetListObjects<S_ProductModel>("S_Product_Update", Model.ProductID, Model.CultureCode, Model.CategoryID, Model.GroupName, Model.PrimaryImage, Model.SecondaryImage, Model.ReserveImage, Model.PrimaryDocument, Model.SecondaryDocument, Model.ReserveDocument, Model.JsonGlobal, Model.Label, Model.Priority, Model.Visible, Model.IsDeleted, Model.IsOutOfStock, Model.ProductCode, Model.Quantity, Model.ViewCount, Model.LikeCount, Model.OldPrice, Model.CurrentPrice, Model.PriceString, Model.Tag, Model.Link, Model.StartDate, Model.EndDate, Model.CreateBy, Model.LastUpdateBy, Model.Name, Model.UrlKey, Model.JsonCulture, Model.Source, Model.PostedBy, Model.Description, Model.Overview, Model.Information, Model.Summary, Model.Details);

            // create log
            if (IsAdd)
            {
                S_Log_Create(
                    LogType: S_LogType.ProductAdd,
                    details: NewModel[0]
                );
            }
            else
            {
                S_Log_Create(
                    LogType: S_LogType.ProductUpdate,
                    details: new object[] { PrevModel[0], NewModel[0] }
                );
            }

            return NewModel;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ProductID"></param>
        /// <param name="CountType">view | like</param>
        public void S_Product_UpdateCount(string ProductID, string CountType)
        {
            var Query = DBContext.ListProduct.Where(p => p.ProductID == ProductID);
            if (Query != null)
            {
                if (CountType == "view")
                {
                    Query.ToList().ForEach(p => p.ViewCount += 1);
                }
                else if (CountType == "like")
                {
                    Query.ToList().ForEach(p => p.LikeCount += 1);
                }

                DBContext.SaveChanges();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ProductID"></param>
        /// <param name="Permanant"></param>
        /// <returns></returns>
        public int S_Product_DeleteByID(string ProductID, bool Permanant)
        {
            S_ProductModel ProductModel = S_Product_GetByID(ProductID, "all", false)[0];

            if (Permanant)
            {
                // delete primary image
                if (ProductModel.PrimaryImage != "default.png")
                {
                    Helpers.DeleteMediaFile(ProductModel.PrimaryImage);
                }
                // delete all list child content images
                List<S_ContentModel> ListChild = S_Content_GetList(TotalRow: ref TotalRow, TotalPage: ref TotalPage, CultureCode: "default", ContentType: "all", ParentsID: ProductID, ParentsTable: "all", PageSize: 100);

                if (ListChild != null && ListChild.Count > 0)
                {
                    foreach (S_ContentModel item in ListChild)
                    {
                        if(item.PrimaryImage!="default.png") Helpers.DeleteMediaFile(item.PrimaryImage);
                        // only need to delete images files
                        // delete in database is already in procedure: S_Product_DeleteByID
                    }
                }
            }

            // create log
            S_Log_Create(
                LogType: S_LogType.ProductDelete,
                details: ProductModel
            );

            return DBCtrl.ExecuteNonQuery("S_Product_DeleteByID", ProductID, Permanant);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ProductID"></param>
        /// <param name="CultureCode">all | vi-vn | en-us | ...</param>
        /// <returns></returns>
        public List<S_ProductModel> S_Product_GetByID(string ProductID, string CultureCode, bool WithListImage)
        {
            List<S_ProductModel> ListModel = DBCtrl.GetListObjects<S_ProductModel>("S_Product_GetByID", ProductID, CultureCode);

            if (WithListImage)
            {
                //List<S_ContentModel> ListImage = S_Content_GetList("full", CultureCode, "all", "-1", "all", "all", "all", "product-image", ListModel[0].ProductID, "S_Product", 100, 1, "True", false, "", "", "Priority desc", ref TotalRow, ref TotalPage, false);
                List<S_ContentModel> ListImage = S_Content_GetList(TotalRow: ref TotalRow, TotalPage: ref TotalPage, CultureCode: CultureCode, ContentType: "product-image", ParentsID: ListModel[0].ProductID, ParentsTable: "S_Product", PageSize: 100);
                for (int i = 0; i < ListModel.Count; i++)
                {
                    ListModel[i].ListImage = ListImage;
                }
            }

            return ListModel;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="GetType">full : get all Column | short : get ProductID, CultureCode, Name</param>
        /// <param name="CultureCode">all | CultureCode</param>
        /// <param name="GroupName">all | GroupName</param>
        /// <param name="CategoryID">all | CategoryID</param>
        /// <param name="ExceptProductID">-1 | ProductID : except by ProductID: case get related product</param>
        /// <param name="CreateBy">all | UserID</param>
        /// <param name="Label">all | Label</param>
        /// <param name="Visible">all | True | False</param>
        /// <param name="IsDeleted">all | True | False</param>
        /// <param name="IsOutOfStock">all | True | False</param>
        /// <param name="ByDateRange">True : get by StartDate  Now  EndDate</param>
        /// <param name="Keyword">Search by keyword, leave blank if not search</param>
        /// <param name="KeywordColumn">all (search in column Name, Tag) | name of the column which search in</param>
        /// <param name="PriceRangeColumn">no | OldPrice | CurrentPrice</param>
        /// <param name="StartPrice"></param>
        /// <param name="EndPrice"></param>
        /// <param name="PageSize"></param>
        /// <param name="PageIndex"></param>
        /// <param name="OrderBy">ColumnName with desc/asc</param>
        /// <param name="TotalRow"></param>
        /// <param name="TotalPage"></param>
        /// <param name="WithListImage"></param>
        /// <param name="FromSubCategory">get product from sub-category of al level (if input CategoryID != all and -1)</param>
        /// <returns></returns>
        public List<S_ProductModel> S_Product_GetList(ref int TotalRow, ref int TotalPage, GetListType GetType = GetListType.full, string CultureCode = "default", string GroupName = "all", string CategoryID = "all", string ExceptProductID = "-1", string CreateBy = "all", string Label = "all", Visible Visible = Visible.True, IsDeleted IsDeleted = IsDeleted.False, IsOutOfStock IsOutOfStock = IsOutOfStock.all, bool ByDateRange = false, string Keyword = "", ProductColumns KeywordColumn = ProductColumns.Name, ProductPriceRangeColumns PriceRangeColumn = ProductPriceRangeColumns.no, decimal StartPrice = 0, decimal EndPrice = 0, int PageSize = 20, int PageIndex = 1, ProductColumns OrderByColumn = ProductColumns.Priority, OrderDirection Direction = OrderDirection.desc, bool WithListImage = false, bool FromSubCategory = false)
        {
            if (CultureCode == "default") CultureCode = Helpers.DefaultCultureCode;

            #region Get List Sub Category All Level

            string LstCatID = "all";

            if (CategoryID != "all")
            {
                LstCatID = "'" + CategoryID + "'";
            }

            if (FromSubCategory && CategoryID != "all" && CategoryID != "-1")
            {
                LstCatID = "'" + CategoryID + "',";

                List<S_CategoryModel> ListCategory = S_Category_GetListAllLevel(
                    GetType: GetListType.simple,
                    CategoryType: CategoryType.product,
                    ParentsID: CategoryID
                );

                if (ListCategory.Count > 0)
                {
                    foreach (S_CategoryModel Cat in ListCategory)
                    {
                        LstCatID += "'" + Cat.CategoryID + "',";
                    }
                }

                LstCatID = LstCatID.Substring(0, LstCatID.Length - 1);
            }

            #endregion

            int StartRow = (PageIndex - 1) * PageSize + 1;
            int EndRow = StartRow + PageSize - 1;

            List<S_ProductModel> ListReturn = DBCtrl.GetListObjects<S_ProductModel>("S_Product_GetList", GetType.ToString(), CultureCode, GroupName, LstCatID, ExceptProductID, CreateBy, Label, Visible.ToString(), IsDeleted.ToString(), IsOutOfStock.ToString(), ByDateRange, Keyword, KeywordColumn.ToString(), PriceRangeColumn.ToString(), StartPrice, EndPrice, StartRow, EndRow, OrderByColumn.ToString() + " " + Direction.ToString());

            TotalRow = 0;
            TotalPage = 0;
            if (ListReturn.Count > 0)
            {
                TotalRow = ListReturn[0].TotalRow;
                if (TotalRow % PageSize == 0) TotalPage = TotalRow / PageSize;
                else TotalPage = TotalRow / PageSize + 1;

                if (WithListImage)
                {
                    for (int i = 0; i < ListReturn.Count; i++)
                    {
                        ListReturn[i].ListImage = S_Content_GetList(TotalRow: ref TotalRow, TotalPage: ref TotalPage, CultureCode: CultureCode, ContentType: "product-image", ParentsID: ListReturn[i].ProductID, ParentsTable: "S_Product", PageSize: 100);
                    }
                }
            }

            return ListReturn;
        }

        #endregion

        #region S_ProductCategory (USING ENTITY FRAMEWORK)

        public void S_ProductCategory_Insert(string ProductID, string CategoryID)
        {
            S_ProductCategoryModel Model = new S_ProductCategoryModel();
            Model.ProductID = ProductID;
            Model.CategoryID = CategoryID;

            DBContext.ListProductCategory.Add(Model);

            DBContext.SaveChanges();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ByColumn">CategoryID, ProductID</param>
        /// <param name="ColumnID"></param>
        /// <returns></returns>
        public void S_ProductCategory_DeleteByID(string ByColumn, string ColumnID)
        {
            if(ByColumn == "CategoryID")
            {
                DBContext.ListProductCategory.RemoveRange(
                    DBContext.ListProductCategory.Where(pc => pc.CategoryID == ColumnID)
                );
            }
            else if(ByColumn == "ProductID")
            {
                DBContext.ListProductCategory.RemoveRange(
                    DBContext.ListProductCategory.Where(pc => pc.ProductID == ColumnID)
                );
            }

            DBContext.SaveChanges();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ByColumn">CategoryID, ProductID</param>
        /// <param name="ColumnID"></param>
        /// <returns></returns>
        public List<S_ProductCategoryModel> S_ProductCategory_GetListByID(string ByColumn, string ColumnID)
        {
            List<S_ProductCategoryModel> ListReturn = new List<S_ProductCategoryModel>();

            if(ByColumn == "CategoryID")
            {
                ListReturn = DBContext.ListProductCategory.Where(pc => pc.CategoryID == ColumnID).ToList();
            }
            else if(ByColumn == "ProductID")
            {
                ListReturn = DBContext.ListProductCategory.Where(pc => pc.ProductID == ColumnID).ToList();
            }

            return ListReturn;
        }

        #endregion

        #region S_Content

        public List<S_ContentModel> S_Content_Update(S_ContentModel Model)
        {
            bool IsAdd = true;

            if (Model.ContentID == "-1") Model.ContentID = Helpers.GenerateID();
            Model.UrlKey = Helpers.ConvertASCIIChar(Model.Name);


            List<S_ContentModel> PrevModel = S_Content_GetByID(Model.ContentID, Model.CultureCode);
            if (PrevModel != null && PrevModel.Count > 0)
            {
                IsAdd = false;
            }

            List<S_ContentModel> NewModel = DBCtrl.GetListObjects<S_ContentModel>("S_Content_Update", Model.ContentID, Model.CultureCode, Model.CategoryID, Model.GroupName, Model.ContentType, Model.ParentsID, Model.ParentsTable, Model.PrimaryImage, Model.SecondaryImage, Model.ReserveImage, Model.PrimaryDocument, Model.SecondaryDocument, Model.ReserveDocument, Model.JsonGlobal, Model.Label, Model.Link, Model.Tag, Model.Priority, Model.ViewCount, Model.LikeCount, Model.Visible, Model.IsDeleted, Model.StartDate, Model.EndDate, Model.CreateBy, Model.LastUpdateBy, Model.Name, Model.UrlKey, Model.JsonCulture, Model.Description, Model.Overview, Model.Information, Model.Summary, Model.Details);

            // create log
            if (IsAdd)
            {
                S_Log_Create(
                    LogType: S_LogType.ContentAdd,
                    details: NewModel[0]
                );
            }
            else
            {
                S_Log_Create(
                    LogType: S_LogType.ContentUpdate,
                    details: new object[] { PrevModel[0], NewModel[0] }
                );
            }

            return NewModel;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ContentID"></param>
        /// <param name="CountType">view | like</param>
        public void S_Content_UpdateCount(string ContentID, string CountType)
        {
            var Query = DBContext.ListContent.Where(c => c.ContentID == ContentID);
            if (Query != null)
            {
                if (CountType == "view")
                {
                    Query.ToList().ForEach(c => c.ViewCount += 1);
                }
                else if (CountType == "like")
                {
                    Query.ToList().ForEach(c => c.LikeCount += 1);
                }

                DBContext.SaveChanges();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ContentID"></param>
        /// <param name="Permanant"></param>
        /// <returns></returns>
        public int S_Content_DeleteByID(string ContentID, bool Permanant)
        {
            S_ContentModel Model = S_Content_GetByID(ContentID, "all")[0];

            if (Permanant)
            {
                if (Model.PrimaryImage != "default.png")
                {
                    Helpers.DeleteMediaFile(Model.PrimaryImage);
                }

                // delete child content & images
                List<S_ContentModel> ListChild = S_Content_GetList(TotalRow: ref TotalRow, TotalPage: ref TotalPage, CultureCode: "default", ParentsID: ContentID, PageSize: 100, ParentsTable: "all");
                if (ListChild != null && ListChild.Count > 0)
                {
                    foreach (S_ContentModel item in ListChild)
                    {
                        if (item.PrimaryImage != "default.png") Helpers.DeleteMediaFile(item.PrimaryImage);
                    }
                }
            }

            // Create Log
            S_Log_Create(
                LogType: S_LogType.ContentDelete,
                details: Model
            );

            return DBCtrl.ExecuteNonQuery("S_Content_DeleteByID", ContentID, Permanant);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ContentID"></param>
        /// <param name="CultureCode">all | vi-vn | en-us | ...</param>
        /// <returns></returns>
        public List<S_ContentModel> S_Content_GetByID(string ContentID, string CultureCode)
        {
            return DBCtrl.GetListObjects<S_ContentModel>("S_Content_GetByID", ContentID, CultureCode);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="GetType">full : get all Column | short : get ContentID, CultureCode, Name</param>
        /// <param name="CultureCode">all | CultureCode</param>
        /// <param name="CategoryID">all | CategoryID</param>
        /// <param name="ExceptContentID">-1 | ContentID : except by ContentID: case get related content</param>
        /// <param name="GroupName">all | GroupName</param>
        /// <param name="CreateBy">all | UserID</param>
        /// <param name="Label">all | Label</param>
        /// <param name="ContentType">all | product-image | category-image | banner | news | about-us</param>
        /// <param name="ParentsID">all | -1 | ParentsID</param>
        /// <param name="ParentsTable">all | -1 | S_Category | S_Product | S_Content</param>
        /// <param name="PageSize"></param>
        /// <param name="PageIndex"></param>
        /// <param name="Visible">all | True | False</param>
        /// <param name="IsDeleted">all | True | False</param>
        /// <param name="ByDateRange">True : get by StartDate Now EndDate</param>
        /// <param name="Keyword">Search by keyword, leave blank if not search</param>
        /// <param name="KeywordColumn">all (search in column Name, Tag) | name of the column which search in</param>
        /// <param name="OrderBy">column name (CreateDate, Priority, ...) with asc or desc</param>
        /// <param name="TotalRow"></param>
        /// <param name="TotalPage"></param>
        /// <param name="FromSubCategory">get content from sub-category of al level (if input CategoryID != all and -1)</param>
        /// <returns></returns>
        public List<S_ContentModel> S_Content_GetList(ref int TotalRow, ref int TotalPage, GetListType GetType = GetListType.full, string CultureCode = "default", string CategoryID = "all", string ExceptContentID = "-1", string GroupName = "all", string CreateBy = "all", string Label = "all", string ContentType = "all", string ParentsID = "-1", string ParentsTable = "-1", int PageSize = 20, int PageIndex = 1, Visible Visible = Visible.True, IsDeleted IsDeleted = IsDeleted.False, bool ByDateRange = false, string Keyword = "", ContentColumns KeywordColumn = ContentColumns.Name, ContentColumns OrderByColumn = ContentColumns.Priority, OrderDirection Direction = OrderDirection.desc, bool FromSubCategory = false)
        {
            if (CultureCode == "default") CultureCode = Helpers.DefaultCultureCode;

            #region Get List Sub Category All Level

            string LstCatID = "all";

            if (CategoryID != "all")
            {
                LstCatID = "'" + CategoryID + "'";
            }

            if (FromSubCategory && CategoryID != "all" && CategoryID != "-1")
            {
                LstCatID = "'" + CategoryID + "',";

                List<S_CategoryModel> ListCategory = S_Category_GetListAllLevel(
                    GetType: GetListType.simple,
                    CategoryType: (CategoryType)Enum.Parse(typeof(CategoryType), ContentType),
                    ParentsID: CategoryID
                );

                if (ListCategory.Count > 0)
                {
                    foreach (S_CategoryModel Cat in ListCategory)
                    {
                        LstCatID += "'" + Cat.CategoryID + "',";
                    }
                }

                LstCatID = LstCatID.Substring(0, LstCatID.Length - 1);
            }

            #endregion

            int StartRow = (PageIndex - 1) * PageSize + 1;
            int EndRow = StartRow + PageSize - 1;

            List<S_ContentModel> ListReturn = DBCtrl.GetListObjects<S_ContentModel>("S_Content_GetList", GetType.ToString(), CultureCode, LstCatID, ExceptContentID, GroupName, CreateBy, Label, ContentType, ParentsID, ParentsTable, StartRow, EndRow, Visible.ToString(), IsDeleted.ToString(), ByDateRange, Keyword, KeywordColumn.ToString(), OrderByColumn.ToString() + " " + Direction.ToString());

            TotalRow = 0;
            TotalPage = 0;
            if (ListReturn.Count > 0)
            {
                TotalRow = ListReturn[0].TotalRow;
                if (TotalRow % PageSize == 0) TotalPage = TotalRow / PageSize;
                else TotalPage = TotalRow / PageSize + 1;
            }

            return ListReturn;
        }

        #endregion


        #region S_ContentCategory (USING ENTITY FRAMEWORK)

        public void S_ContentCategory_Insert(string ContentID, string CategoryID)
        {
            S_ContentCategoryModel Model = new S_ContentCategoryModel();
            Model.ContentID = ContentID;
            Model.CategoryID = CategoryID;

            DBContext.ListContentCategory.Add(Model);
            DBContext.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ByColumn">CategoryID, ContentID</param>
        /// <param name="ColumnID"></param>
        public void S_ContentCategory_DeleteByID(string ByColumn, string ColumnID)
        {
            if(ByColumn == "CategoryID")
            {
                DBContext.ListContentCategory.RemoveRange(
                    DBContext.ListContentCategory.Where(cc => cc.CategoryID == ColumnID)
                );
            }
            else if(ByColumn == "ContentID")
            {
                DBContext.ListContentCategory.RemoveRange(
                    DBContext.ListContentCategory.Where(cc => cc.ContentID == ColumnID)
                );
            }

            DBContext.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ByColumn">CategoryID, ContentID</param>
        /// <param name="ColumnID"></param>
        /// <returns></returns>
        public List<S_ContentCategoryModel> S_ContentCategory_GetListByID(string ByColumn, string ColumnID)
        {
            List<S_ContentCategoryModel> ListReturn = new List<S_ContentCategoryModel>();

            if(ByColumn == "CategoryID")
            {
                ListReturn = DBContext.ListContentCategory.Where(cc => cc.CategoryID == ColumnID).ToList();
            }
            else if(ByColumn == "ContentID")
            {
                ListReturn = DBContext.ListContentCategory.Where(cc => cc.ContentID == ColumnID).ToList();
            }

            return ListReturn;
        }

        #endregion


        #region S_OrderStatus (USING ENTITY FRAMEWORK)

        public S_OrderStatusModel S_OrderStatus_Update(S_OrderStatusModel Model)
        {
            if(Model.StatusID == "-1")
            {
                Model.StatusID = Helpers.GenerateID();
                CartDB.ListOrderStatus.Add(Model);
            }
            else
            {
                CartDB.Entry(Model).State = EntityState.Modified;
            }

            CartDB.SaveChanges();
            return Model;
        }
        public void S_OrderStatus_DeleteByID(string StatusID)
        {
            if(CartDB.ListOrder.Where(o => o.StatusID == StatusID).Count() > 0)
            {
                // if this StatusID is used by some Order, set IsDelete = true
                S_OrderStatusModel Model = CartDB.ListOrderStatus.Where(os => os.StatusID == StatusID).First();
                if (Model != null)
                {
                    Model.IsDelete = true;
                    CartDB.Entry(Model).State = EntityState.Modified;
                }
            }
            else
            {
                // if this StatusID is not used by any Order, delete it
                CartDB.ListOrderStatus.Remove(
                    CartDB.ListOrderStatus.Where(os => os.StatusID == StatusID).First()
                );
            }

            CartDB.SaveChanges();
        }
        public S_OrderStatusModel S_OrderStatus_GetByID(string StatusID)
        {
            return CartDB.ListOrderStatus.Where(os => os.StatusID == StatusID).First();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="IsDelete">all | True | False</param>
        /// <param name="Visible">all | True | False</param>
        /// <returns></returns>
        public List<S_OrderStatusModel> S_OrderStatus_GetList(string IsDelete, string Visible)
        {
            List<S_OrderStatusModel> Query = CartDB.ListOrderStatus.OrderByDescending(os => os.Priority).ToList();

            if (IsDelete != "all") Query = Query.Where(os => os.IsDelete == bool.Parse(IsDelete)).ToList();
            if (Visible != "all") Query = Query.Where(os => os.Visible == bool.Parse(Visible)).ToList();

            return Query;
        }

        #endregion

        #region S_CartTemp (USING ENTITY FRAMEWORK)

        public S_CartTempModel S_CartTemp_Update(S_CartTempModel Model)
        {
            if (Model.CartTempID == "-1") Model.CartTempID = Helpers.GenerateID();

            if (CartDB.ListCartTemp.Where(ct => ct.CartTempID == Model.CartTempID).Count() > 0)
            {
                // update cart temp
                CartDB.Entry(Model).State = EntityState.Modified;
            }
            else if(CartDB.ListCartTemp.Where(ct => ct.UserID == Model.UserID && ct.ProductID == Model.ProductID && ct.TempType == "cart").Count() > 0)
            {
                // update cart quantity
                S_CartTempModel PrevCart = CartDB.ListCartTemp.Where(ct => ct.UserID == Model.UserID && ct.ProductID == Model.ProductID && ct.TempType == "cart").First();
                PrevCart.Quantity += Model.Quantity;
                CartDB.Entry(PrevCart).State = EntityState.Modified;
                Model = PrevCart;
            }
            else
            {
                // insert cart temp
                CartDB.ListCartTemp.Add(Model);
            }

            CartDB.SaveChanges();

            return Model;
        }
        public void S_CartTemp_UpdateQuantity(string CartTempID, int Quantity)
        {
            CartDB.ListCartTemp.Where(ct => ct.CartTempID == CartTempID).First().Quantity = Quantity;
            CartDB.SaveChanges();
        }
        public int S_CartTemp_DeleteByID(string CartTempID)
        {
            CartDB.ListCartTemp.Remove(
                CartDB.ListCartTemp.Where(ct => ct.CartTempID == CartTempID).First()
            );
            CartDB.SaveChanges();

            return 1;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="TempType">all | cart | wishlist | compare</param>
        /// <returns></returns>
        public int S_CartTemp_DeleteByUser(string UserID, string TempType)
        {
            var Query = CartDB.ListCartTemp.Where(ct => ct.UserID == UserID);
            if (TempType != "all") Query.Where(ct => ct.TempType == TempType);

            CartDB.ListCartTemp.RemoveRange(Query);

            CartDB.SaveChanges();

            return 1;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="TempType">all | cart | wishlist | compare</param>
        /// <returns></returns>
        public int S_CartTemp_CountByUserID(string UserID, string TempType)
        {
            var Query = CartDB.ListCartTemp.Where(ct => ct.UserID == UserID);

            if (TempType != "all") Query.Where(ct => ct.TempType == TempType);

            return Query.Count();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CartTempID"></param>
        /// <param name="CultureCode">must specify (ex: vi-vn, en-us, ...) to get the ProductName</param>
        /// <returns></returns>
        public S_CartTempModel S_CartTemp_GetByID(string CartTempID, string CultureCode)
        {
            return DBCtrl.GetObject<S_CartTempModel>("S_CartTemp_GetByID", CartTempID, CultureCode);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="TempType">all | cart | wishlist | compare</param>
        /// <param name="CultureCode">must specify (ex: vi-vn, en-us, ...) to get the ProductName</param>
        /// <returns></returns>
        public List<S_CartTempModel> S_CartTemp_GetListByUser(string UserID, string CultureCode, CartTempTypes TempType = CartTempTypes.cart)
        {
            return DBCtrl.GetListObjects<S_CartTempModel>("S_CartTemp_GetListByUser", UserID, TempType.ToString(), CultureCode);
        }

        #endregion

        #region S_Order (USING ENTITY FRAMEWORK)

        public S_OrderModel S_Order_Update(S_OrderModel Model)
        {
            bool IsAdd = true;
            S_OrderModel PrevOrder = null;


            if(Model.OrderID == "-1")
            {
                Model.OrderID = Helpers.GenerateID();
                CartDB.ListOrder.Add(Model);
            }
            else
            {
                var Entry = CartDB.Entry(Model);
                Entry.State = EntityState.Modified;
                Entry.Property("CreateDate").IsModified = false;

                IsAdd = false;
                PrevOrder = S_Order_GetByID(Model.OrderID, false);
            }

            CartDB.SaveChanges();

            Model = S_Order_GetByID(Model.OrderID, false);

            // create log
            if (IsAdd)
            {
                S_Log_Create(
                    LogType: S_LogType.OrderCreate,
                    details: Model
                );
            }
            else
            {
                S_Log_Create(
                    LogType: S_LogType.OrderUpdate,
                    details: new object[] { PrevOrder, Model }
                );
            }

            return Model;
        }
        public int S_Order_DeleteByID(string OrderID)
        {
            // create log
            S_OrderModel Model = S_Order_GetByID(OrderID, true);
            S_Log_Create(
                LogType: S_LogType.OrderDelete,
                details: Model
            );

            // delete order
            CartDB.ListOrder.Remove(
                CartDB.ListOrder.Where(o => o.OrderID == OrderID).First()
            );

            // delete order details
            CartDB.ListOrderDetails.RemoveRange(
                CartDB.ListOrderDetails.Where(od => od.OrderID == OrderID).ToList()
            );

            CartDB.SaveChanges();

            return 1;
        }
        public S_OrderModel S_Order_GetByID(string OrderID, bool WithListDetails)
        {
            S_OrderModel Model = DBCtrl.GetObject<S_OrderModel>("S_Order_GetByID", OrderID);
            if (WithListDetails)
            {
                Model.ListDetails = S_OrderDetails_GetByOrderID(OrderID);
            }

            return Model;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserID">all | UserID</param>
        /// <param name="PageSize"></param>
        /// <param name="PageIndex"></param>
        /// <param name="StatusID">all | StatusID</param>
        /// <param name="Approved">all | True | False</param>
        /// <param name="IsDelete">all | True | False</param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="DateColumn">no : disable FromDate -> ToDate | CreateDate | UpdateDate | AprrovedDate</param>
        /// <param name="Keyword">Leave blank if not search</param>
        /// <param name="KeywordColumn">all : search in alot of Column, slow | ColumnName</param>
        /// <param name="OrderBy">Column Name with desc/asc</param>
        /// <param name="TotalRow"></param>
        /// <param name="TotalPage"></param>
        /// <returns></returns>
        public List<S_OrderModel> S_Order_GetList(ref int TotalRow, ref int TotalPage, DateTime FromDate, DateTime ToDate, string UserID = "all", int PageSize = 20, int PageIndex = 1, string StatusID = "all", OrdersApproved Approved = OrdersApproved.all, IsDeleted IsDelete = IsDeleted.False, OrderColumns DateColumn = OrderColumns.no, string Keyword = "", OrderColumns KeywordColumn = OrderColumns.Name, OrderColumns OrderByColumn = OrderColumns.UpdateDate, OrderDirection Direction = OrderDirection.desc, bool WithListDetails = true)
        {
            int StartRow = (PageIndex - 1) * PageSize + 1;
            int EndRow = StartRow + PageSize - 1;

            List<S_OrderModel> ListReturn = DBCtrl.GetListObjects<S_OrderModel>("S_Order_GetList", UserID, StartRow, EndRow, StatusID, Approved.ToString(), IsDelete.ToString(), FromDate, ToDate, DateColumn.ToString(), Keyword, KeywordColumn.ToString(), OrderByColumn.ToString() + " " + Direction.ToString());
            if (WithListDetails)
            {
                for (int i = 0; i < ListReturn.Count; i++)
                {
                    ListReturn[i].ListDetails = S_OrderDetails_GetByOrderID(ListReturn[i].OrderID);
                }
            }


            TotalRow = 0;
            TotalPage = 0;
            if (ListReturn.Count > 0)
            {
                TotalRow = ListReturn[0].TotalRow;
                if (TotalRow % PageSize == 0) TotalPage = TotalRow / PageSize;
                else TotalPage = TotalRow / PageSize + 1;
            }

            return ListReturn;
        }

        #endregion

        #region S_OrderDetails (USING ENTITY FRAMEWORK)

        public S_OrderDetailsModel S_OrderDetails_Update(S_OrderDetailsModel Model)
        {
            if(Model.DetailsID == "-1")
            {
                Model.DetailsID = Helpers.GenerateID();
                CartDB.ListOrderDetails.Add(Model);
            }
            else
            {
                CartDB.Entry(Model).State = EntityState.Modified;
            }

            CartDB.SaveChanges();

            return Model;
        }
        public int S_OrderDetails_DeleteByID(string DetailsID)
        {
            CartDB.ListOrderDetails.Remove(
                CartDB.ListOrderDetails.Where(od => od.DetailsID == DetailsID).First()
            );

            CartDB.SaveChanges();

            return 1;
        }
        public S_OrderDetailsModel S_OrderDetails_GetByID(string DetailsID)
        {
            return DBCtrl.GetObject<S_OrderDetailsModel>("S_OrderDetails_GetByID", DetailsID);
        }
        public List<S_OrderDetailsModel> S_OrderDetails_GetByOrderID(string OrderID)
        {
            return DBCtrl.GetListObjects<S_OrderDetailsModel>("S_OrderDetails_GetByOrderID", OrderID);
        }

        #endregion


        #region Users

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Role">all | name or id or role (depends on RoleType)</param>
        /// <param name="RoleType">name | id</param>
        /// <param name="PageSize"></param>
        /// <param name="PageIndex"></param>
        /// <param name="Keyword"></param>
        /// <param name="KeywordColumn">all | ColumnName</param>
        /// <param name="OrderBy">ColumnName (Id, UserName, Email, DisplayName ...)</param>
        /// <param name="TotalRow"></param>
        /// <param name="TotalPage"></param>
        /// <param name="WithListRoles"></param>
        /// <returns></returns>
        public List<S_UsersModel> S_Users_GetList(ref int TotalRow, ref int TotalPage, string Role = "all", string RoleType = "id", string Keyword = "", string KeywordColumn = "Email", int PageSize = 20, int PageIndex = 1, string OrderBy = "CreateDate desc", bool WithListRoles = true)
        {
            int StartRow = (PageIndex - 1) * PageSize + 1;
            int EndRow = StartRow + PageSize - 1;

            List<S_UsersModel> ListReturn = DBCtrl.GetListObjects<S_UsersModel>("Users_GetList", Role, RoleType, Keyword, KeywordColumn, StartRow, EndRow, OrderBy);

            if (WithListRoles)
            {
                for (int i = 0; i < ListReturn.Count; i++)
                {
                    ListReturn[i].ListRoles = S_Roles_GetListByUser(ListReturn[i].Id);
                }
            }

            TotalRow = 0;
            TotalPage = 0;
            if (ListReturn.Count > 0)
            {
                TotalRow = ListReturn[0].TotalRow;
                if (TotalRow % PageSize == 0) TotalPage = TotalRow / PageSize;
                else TotalPage = TotalRow / PageSize + 1;
            }

            return ListReturn;
        }

        #endregion

        #region Roles

        public int S_Roles_Update(S_RolesModel Model)
        {
            if (Model.Id == "-1") Model.Id = Helpers.GenerateID();
            return DBCtrl.ExecuteNonQuery("Roles_Update", Model.Id, Model.Name);
        }
        public int S_Roles_AddUserRole(string InputUserID, string RoleID)
        {
            return DBCtrl.ExecuteNonQuery("Roles_AddUserRole", InputUserID, RoleID);
        }
        public int S_Roles_DeleteUserRole(string InputUserID)
        {
            return DBCtrl.ExecuteNonQuery("Roles_DeleteUserRole", InputUserID);
        }
        public int S_Roles_DeleteByID(string RoleID)
        {
            return DBCtrl.ExecuteNonQuery("Roles_DeleteByID", RoleID);
        }
        public S_RolesModel S_Roles_GetByID(string RoleID)
        {
            return DBCtrl.GetObject<S_RolesModel>("Roles_GetByID", RoleID);
        }
        public List<S_RolesModel> S_Roles_GetListByUser(string InputUserID)
        {
            return DBCtrl.GetListObjects<S_RolesModel>("Roles_GetListByUser", InputUserID);
        }
        public List<S_RolesModel> S_Roles_GetList()
        {
            return DBCtrl.GetListObjects<S_RolesModel>("Roles_GetList");
        }

        #endregion

        #region S_Log

        public S_LogModel S_Log_Create(S_LogType LogType, string UserID = "-1", string Summary = "", HttpRequestBase CurRequest = null, params object[] details)
        {
            if (!Helpers.SiteSettings.EnableLog) return null;


            S_LogModel Model = new S_LogModel();
            Model.LogID = Guid.NewGuid().ToString();
            Model.LogType = LogType;
            Model.Summary = Summary;

            if (UserID != "-1")
            {
                Model.UserID = UserID;
            }
            else
            {
                Model.UserID = this.UserID;
            }

            try
            {
                if (details != null)
                {
                    dynamic objDetails = new ExpandoObject();

                    if (CurRequest != null)
                    {
                        objDetails.IP = CurRequest.UserHostAddress;
                        objDetails.Agent = CurRequest.UserAgent;
                    }
                    else if (CurrentRequest != null)
                    {
                        objDetails.IP = CurrentRequest.UserHostAddress;
                        objDetails.Agent = CurrentRequest.UserAgent;
                    }

                    switch (LogType)
                    {
                        case S_LogType.UserLogin:
                            break;
                        case S_LogType.UserRegister:
                            objDetails.UserInfo = details[0];
                            break;
                        case S_LogType.UserUpdateProfile:
                            objDetails.OldUser = details[0];
                            objDetails.NewUser = details[1];
                            break;
                        case S_LogType.UserCreateByAdmin:
                            objDetails.UserInfo = details[0];
                            break;
                        case S_LogType.UserUpdateByAdmin:
                            objDetails.OldUser = details[0];
                            objDetails.NewUser = details[1];
                            break;
                        case S_LogType.UserDelete:
                            objDetails.UserInfo = details[0];
                            break;

                        case S_LogType.ProductAdd:
                            objDetails.ProductInfo = details[0];
                            break;
                        case S_LogType.ProductUpdate:
                            objDetails.OldProduct = details[0];
                            objDetails.NewProduct = details[1];
                            break;
                        case S_LogType.ProductDelete:
                            objDetails.ProductInfo = details[0];
                            break;

                        case S_LogType.ContentAdd:
                            objDetails.ContentInfo = details[0];
                            break;
                        case S_LogType.ContentUpdate:
                            objDetails.OldContent = details[0];
                            objDetails.NewContent = details[1];
                            break;
                        case S_LogType.ContentDelete:
                            objDetails.ContentInfo = details[0];
                            break;

                        case S_LogType.OrderCreate:
                            objDetails.OrderInfo = details[0];
                            break;
                        case S_LogType.OrderUpdate:
                            objDetails.OldOrder = details[0];
                            objDetails.NewOrder = details[1];
                            break;
                        case S_LogType.OrderDelete:
                            objDetails.OrderInfo = details[0];
                            break;

                        default:
                            break;
                    }

                    Model.Details = JsonConvert.SerializeObject(objDetails);
                }
            }
            catch (Exception ex)
            {
                Model.LogType = S_LogType.CreateLogError;
                Model.Details = JsonConvert.SerializeObject(new
                {
                    LogType = LogType,
                    LogName = LogType.ToString(),
                    Error = ex
                });
            }

            DBContext.ListLog.Add(Model);
            DBContext.SaveChanges();

            return Model;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TotalRow"></param>
        /// <param name="TotalPage"></param>
        /// <param name="LogType"></param>
        /// <param name="UserID"></param>
        /// <param name="DateColumn">disable | CreateDate</param>
        /// <param name="PageSize"></param>
        /// <param name="PageIndex"></param>
        /// <param name="OrderBy"></param>
        /// <returns></returns>
        public List<S_LogModel> S_Log_GetList(ref int TotalRow, ref int TotalPage, S_LogType LogType = S_LogType.All, string UserID = "all", string DateColumn = "disable", DateTime? FromDate = null, DateTime? ToDate = null, int PageSize = 20, int PageIndex = 1, string OrderBy = "CreateDate desc")
        {
            int StartRow = (PageIndex - 1) * PageSize + 1;
            int EndRow = StartRow + PageSize - 1;

            List<S_LogModel> ListReturn = DBCtrl.GetListObjects<S_LogModel>("S_Log_GetList", LogType, UserID, DateColumn, FromDate, ToDate, StartRow, EndRow, OrderBy);

            TotalRow = 0;
            TotalPage = 0;
            if (ListReturn.Count > 0)
            {
                TotalRow = ListReturn[0].TotalRow;
                if (TotalRow % PageSize == 0) TotalPage = TotalRow / PageSize;
                else TotalPage = TotalRow / PageSize + 1;
            }

            return ListReturn;
        }

        #endregion
    }


    public class SContext : DbContext
    {
        public SContext() : base("DefaultConnection")
        {

        }

        public DbSet<S_SettingsModel> ListSettings { get; set; }
        public DbSet<S_CategoryModel> ListCategory { get; set; }
        public DbSet<S_ProductModel> ListProduct { get; set; }
        public DbSet<S_ContentModel> ListContent { get; set; }
        public DbSet<S_ContentCategoryModel> ListContentCategory { get; set; }
        public DbSet<S_ProductCategoryModel> ListProductCategory { get; set; }
        public DbSet<S_LogModel> ListLog { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }

    public class CartContext: DbContext
    {
        public CartContext() : base("DefaultConnection")
        {
            
        }

        public DbSet<S_OrderStatusModel> ListOrderStatus { get; set; }
        public DbSet<S_CartTempModel> ListCartTemp { get; set; }
        public DbSet<S_OrderModel> ListOrder { get; set; }
        public DbSet<S_OrderDetailsModel> ListOrderDetails { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
﻿create table dbo.S_OrderStatus
(
	StatusID nvarchar(50) primary key,
	Name nvarchar(100),
	Summary nvarchar(2000),
	Priority int,
	Visible bit,
	IsDelete bit
)
go

/*
- TempType		: cart | wishlist | compare
*/
create table dbo.S_CartTemp
(
	CartTempID nvarchar(50) primary key,
	TempType nvarchar(20),
	UserID nvarchar(50),
	ProductID nvarchar(50),
	Quantity int,
	Notes nvarchar(2000),
	Overview nvarchar(2000),
	Summary nvarchar(4000),
	CreateDate datetime,
	ExpiredDate datetime
)
go



create table dbo.S_Order
(
	OrderID nvarchar(50) primary key,
	UserID nvarchar(50),
	
	Name nvarchar(100),
	Phone nvarchar(50),
	CellPhone nvarchar(50),
	Email nvarchar(100),
	BillAddress nvarchar(200),
	ShipAddress nvarchar(200),
	
	PaymentType nvarchar(50),
	StatusID nvarchar(50),
	
	Overview nvarchar(2000),
	Summary nvarchar(2000),
	[Description] nvarchar(2000),
	Details nvarchar(max),
	
	Approved bit,
	IsDelete bit,
	
	CreateDate datetime,
	UpdateDate datetime,
	AprrovedDate datetime
)
go

create table dbo.S_OrderDetails
(
	DetailsID nvarchar(50) primary key,
	OrderID nvarchar(50),
	ProductID nvarchar(50),
	ProductName nvarchar(250),
	Quantity int,
	Price money,
	Notes nvarchar(2000),
	Overview nvarchar(2000),
	Summary nvarchar(4000)
)
go


/* S_CartTemp */

create procedure dbo.S_CartTemp_Update
@CartTempID nvarchar(50),
@TempType nvarchar(20),
@UserID nvarchar(50),
@ProductID nvarchar(50),
@Quantity int,
@Notes nvarchar(2000),
@Overview nvarchar(2000),
@Summary nvarchar(4000)
as
	IF exists (select * from S_CartTemp where CartTempID = @CartTempID)
		BEGIN
			update S_CartTemp set
				TempType = @TempType,
				ProductID = @ProductID,
				Quantity = @Quantity,
				Notes = @Notes,
				Overview = @Overview,
				Summary = @Summary,
				UserID = @UserID
			where CartTempID = @CartTempID
		END
	ELSE IF exists (select * from S_CartTemp where UserID = @UserID and ProductID = @ProductID and TempType = 'cart')
		BEGIN
			update S_CartTemp set
				Quantity = Quantity + @Quantity
			where UserID = @UserID and ProductID = @ProductID and Summary = @Summary and TempType = 'cart'
			
			SET @CartTempID = (select top 1 CartTempID from S_CartTemp where UserID = @UserID and ProductID = @ProductID and TempType = 'cart')
		END
	ELSE
		BEGIN
			insert into S_CartTemp values
			(
				@CartTempID,
				@TempType,
				@UserID,
				@ProductID,
				@Quantity,
				@Notes,
				@Overview,
				@Summary,
				GETDATE(),
				DATEADD(year, 1, GETDATE())
			)
		END
	
	update S_CartTemp set ExpiredDate = DATEADD(year, 1, GETDATE()) where UserID = @UserID
	
	select * from S_CartTemp where CartTempID = @CartTempID
go

create procedure dbo.S_CartTemp_DeleteByID
@CartTempID nvarchar(50)
as
	delete from S_CartTemp where CartTempID = @CartTempID
go	

/*
- TempType		: all | cart | wishlist | compare
*/
create procedure dbo.S_CartTemp_DeleteByUser
@UserID nvarchar(50),
@TempType nvarchar(20)
as
	IF @TempType <> 'all'
		delete from S_CartTemp where UserID = @UserID and TempType = @TempType
	ELSE
		delete from S_CartTemp where UserID = @UserID
go

/*
- TempType		: all | cart | wishlist | compare
*/
create procedure dbo.S_CartTemp_CountByUserID
@UserID nvarchar(50),
@TempType nvarchar(20)
as
	IF @TempType <> 'all'
		select SUM(Quantity) as Quantity from S_CartTemp where UserID = @UserID and TempType = @TempType
	ELSE
		select SUM(Quantity) as Quantity from S_CartTemp where UserID = @UserID
go


create procedure dbo.S_CartTemp_GetByID
@CartTempID nvarchar(50),
@CultureCode nvarchar(10)
as
	BEGIN
		select 
			CT.*,
			P.Name as ProductName,
			P.CurrentPrice,
			P.PrimaryImage
		from S_CartTemp CT left outer join S_Product P
			on CT.ProductID = P.ProductID and P.CultureCode = @CultureCode
		where CT.CartTempID = @CartTempID
	END
go

/*
- TempType			: all | cart | wishlist | compare
*/
create procedure dbo.S_CartTemp_GetListByUser
@UserID nvarchar(50),
@TempType nvarchar(20),
@CultureCode nvarchar(10)
as
	BEGIN
		DECLARE @Query nvarchar(500)
		
		SET @Query = 'select '
		
		SET @Query += 'CT.*, '
		SET @Query += 'P.Name as ProductName, '
		SET @Query += 'P.CurrentPrice, '
		SET @Query += 'P.PrimaryImage '
		
		SET @Query += 'from S_CartTemp CT left outer join S_Product P '
		SET @Query += 'on CT.ProductID = P.ProductID and P.CultureCode = ''' + @CultureCode + ''' '
		
		SET @Query += 'where CT.UserID = ''' + @UserID + ''' '
		
		IF @TempType <> 'all'
			SET @Query += 'and CT.TempType = ''' + @TempType + ''' '
		
		exec sp_executesql @Query
	END
go


/* S_OrderStatus */

create procedure dbo.S_OrderStatus_Update
@StatusID nvarchar(50),
@Name nvarchar(100),
@Summary nvarchar(2000),
@Priority int,
@Visible bit,
@IsDelete bit
as
	BEGIN
		IF not exists ( select * from S_OrderStatus where StatusID = @StatusID)
			BEGIN
				insert into S_OrderStatus values
				(
					@StatusID,
					@Name,
					@Summary,
					@Priority,
					@Visible,
					@IsDelete
				)
			END
		ELSE
			BEGIN
				update S_OrderStatus set
					Name = @Name,
					Summary = @Summary,
					Priority = @Priority,
					Visible = @Visible,
					IsDelete = @IsDelete
				where StatusID = @StatusID					
			END
			
		select * from S_OrderStatus where StatusID = @StatusID			
	END
go

create procedure dbo.S_OrderStatus_DeleteByID
@StatusID int
as
	IF not exists (select top 1 * from S_Order where StatusID = @StatusID)
		delete from S_OrderStatus where StatusID = @StatusID
	ELSE
		update S_OrderStatus set IsDelete = 'True' where StatusID = @StatusID
go

create procedure dbo.S_OrderStatus_GetByID
@StatusID int
as
	select * from S_OrderStatus where StatusID = @StatusID
go	

/*
- IsDelete		: all | True | False
- Visible		: all | True | False
*/
create procedure dbo.S_OrderStatus_GetList
@IsDelete nvarchar(10), @Visible nvarchar(10)
as
	DECLARE @Query nvarchar(500)
	SET @Query = 'select * from S_OrderStatus where '
	
	IF @IsDelete <> 'all'
		SET @Query += 'IsDelete = ''' + @IsDelete + ''' '
		
	IF @Visible <> 'all'
		SET @Query += 'and Visible = ''' + @Visible + ''' '
		
	SET @Query += 'order by Priority desc'		
go


/* BTS_Order */

create procedure dbo.S_Order_Update
@OrderID nvarchar(50),
@UserID nvarchar(50),
@Name nvarchar(100),
@Phone nvarchar(50),
@CellPhone nvarchar(50),
@Email nvarchar(100),
@BillAddress nvarchar(200),
@ShipAddress nvarchar(200),
@PaymentType nvarchar(50),
@StatusID nvarchar(50),
@Overview nvarchar(2000),
@Summary nvarchar(2000),
@Description nvarchar(2000),
@Details nvarchar(max),
@Approved bit,
@IsDelete bit,
@AprrovedDate datetime
as
	BEGIN
		IF not exists (select * from S_Order where OrderID = @OrderID)
			BEGIN
				insert into S_Order values
				(
					@OrderID,
					@UserID,
					@Name,
					@Phone,
					@CellPhone,
					@Email,
					@BillAddress,
					@ShipAddress,
					@PaymentType,
					@StatusID,
					@Overview,
					@Summary,
					@Description,
					@Details,
					@Approved,
					@IsDelete,
					GETDATE(),
					GETDATE(),
					GETDATE()
				)
			END
		ELSE
			BEGIN
				update S_Order set
					UserID = @UserID,
					Name = @Name,
					Phone = @Phone,
					CellPhone = @CellPhone,
					Email = @Email,
					BillAddress = @BillAddress,
					ShipAddress = @ShipAddress,
					PaymentType = @PaymentType,
					StatusID = @StatusID,
					Overview = @Overview,
					Summary = @Summary,
					[Description] = @Description,
					Details = @Details,
					Approved = @Approved,
					IsDelete = @IsDelete,
					UpdateDate = GETDATE(),
					AprrovedDate = @AprrovedDate
				where OrderID = @OrderID
			END
		
		select * from S_Order where OrderID = @OrderID			
	END
go

create procedure dbo.S_Order_DeleteByID
@OrderID nvarchar(50)
as
	delete from S_Order where OrderID = @OrderID
	delete from S_OrderDetails where OrderID = @OrderID
go

create procedure dbo.S_Order_GetByID
@OrderID nvarchar(50)
as
	select 
		O.*,
		OS.Name as StatusName,
		(select SUM(Price*Quantity) from S_OrderDetails where OrderID = @OrderID) as TotalPrice
	from S_Order O
		left outer join 
			S_OrderStatus OS on O.StatusID = OS.StatusID
	where O.OrderID = @OrderID
go

/*
- UserID		: all | UserID
- StatusID		: all | StatusID
- Approved		: all | True | False
- IsDelete		: all | True | False
- DateColumn	: no : disable FromDate -> ToDate | CreateDate | UpdateDate | AprrovedDate
- Orderby		: Column Name with desc/asc
*/
create procedure dbo.S_Order_GetList
@UserID nvarchar(50), 
@StartRow int, 
@EndRow int, 
@StatusID nvarchar(50), 
@Approved nvarchar(10), 
@IsDelete nvarchar(10),
@FromDate datetime, 
@ToDate datetime, 
@DateColumn nvarchar(50),
@Orderby nvarchar(20)
as
	BEGIN
		DECLARE @TotalRow int
		DECLARE @CountQuery nvarchar(500)
		DECLARE @Query nvarchar(1500)
		
		-- Count Row
		SET @CountQuery = 'select @TotalRow = COUNT(*) from S_Order where 1 = 1 '

		IF @UserID <> 'all'
			SET @CountQuery += 'AND UserID = ''' + @UserID + ''' '
			
		IF @StatusID <> 'all'
			SET @CountQuery += 'AND StatusID = ' + CAST(@StatusID As nvarchar(10)) + ' '
		
		IF @Approved <> 'all'
			SET @CountQuery += 'AND Approved = ''' + @Approved + ''' '
		
		IF @IsDelete <> 'all'
			SET @CountQuery += 'AND IsDelete = ''' + @IsDelete + ''' '			
		
		IF @DateColumn <> 'no'
			BEGIN
				SET @CountQuery += 'AND ''' + CAST(@FromDate As nvarchar(25)) + ''' <= ' + @DateColumn + ' '
				SET @CountQuery += 'AND ''' + CAST(@ToDate As nvarchar(25)) + ''' >= ' + @DateColumn
			END
			
		exec sp_executesql @CountQuery, N'@TotalRow int out', @TotalRow out
		-- End Count Row
		
		-- Get List
		SET @Query	=	'select * from ('
		SET @Query +=	'select ROW_NUMBER() over (order by O.' + @OrderBy + ') as Row, '
		
		SET @Query +=	'O.*, '
		SET @Query +=	'OS.Name as StatusName, '
		SET @Query +=	CAST(@TotalRow as nvarchar(10)) + ' as TotalRow, '
		SET @Query +=	'(select SUM(Quantity) from S_OrderDetails where OrderID = O.OrderID) as TotalDetails, '
		SET @Query +=	'(select SUM(Price*Quantity) from S_OrderDetails where OrderID = O.OrderID) as TotalPrice '
		
		SET @Query +=	'from S_Order O '
		SET @Query +=	'left outer join S_OrderStatus OS on O.StatusID = OS.StatusID '

		SET @Query +=	'where 1 = 1'
		
		IF @UserID <> 'all'
			SET @Query += 'AND O.UserID = ''' + @UserID + ''' '
			
		IF @StatusID <> 'all'
			SET @Query += 'AND O.StatusID = ' + CAST(@StatusID As nvarchar(10)) + ' '
		
		IF @Approved <> 'all'
			SET @Query += 'AND O.Approved = ''' + @Approved + ''' '
		
		IF @IsDelete <> 'all'
			SET @Query += 'AND O.IsDelete = ''' + @IsDelete + ''' '			
		
		IF @DateColumn <> 'no'
			BEGIN
				SET @Query += 'AND ''' + CAST(@FromDate As nvarchar(25)) + ''' <= O.' + @DateColumn + ' '
				SET @Query += 'AND ''' + CAST(@ToDate As nvarchar(25)) + ''' >= O.' + @DateColumn
			END
		
		SET @Query += ') Temtable where Row >= ' + CAST(@StartRow As nvarchar(10)) + ' and Row <= ' + CAST(@EndRow As nvarchar(10))
		
		exec sp_executesql @Query
		-- End Get List
	END
go	


/* S_OrderDetails */

create procedure dbo.S_OrderDetails_Update
@DetailsID nvarchar(50),
@OrderID nvarchar(50),
@ProductID nvarchar(50),
@ProductName nvarchar(250),
@Quantity int,
@Price money,
@Notes nvarchar(2000),
@Overview nvarchar(2000),
@Summary nvarchar(4000)
as
	BEGIN
		IF not exists (select * from S_OrderDetails where DetailsID = @DetailsID)
			BEGIN
				insert into S_OrderDetails values
				(
					@DetailsID,
					@OrderID,
					@ProductID,
					@ProductName,
					@Quantity,
					@Price,
					@Notes,
					@Overview,
					@Summary
				)
			END
		ELSE
			BEGIN
				update S_OrderDetails set
					OrderID = @OrderID,
					ProductID = @ProductID,
					ProductName = @ProductName,
					Quantity = @Quantity,
					Price = @Price,
					Notes = @Notes,
					Overview = @Overview,
					Summary = @Summary
				where DetailsID = @DetailsID					
			END
		
		select * from S_OrderDetails where DetailsID = @DetailsID			
	END
go

create procedure dbo.S_OrderDetails_DeleteByID
@DetailsID nvarchar(50)
as
	delete from S_OrderDetails where DetailsID = @DetailsID
go

create procedure dbo.S_OrderDetails_GetByID
@DetailsID nvarchar(50)
as
	select OD.*, 
		(select top 1 PrimaryImage from S_Product where ProductID = OD.ProductID) as PrimaryImage
	from S_OrderDetails OD where OD.DetailsID = @DetailsID
go

create procedure dbo.S_OrderDetails_GetByOrderID
@OrderID nvarchar(50)
as
	select OD.*, 
		(select top 1 PrimaryImage from S_Product where ProductID = OD.ProductID) as PrimaryImage
	from S_OrderDetails OD where OD.OrderID = @OrderID
go
﻿create table dbo.S_OrderStatus
(
	StatusID nvarchar(50) primary key,
	Name nvarchar(100),
	Summary nvarchar(2000),
	Priority int,
	Visible bit,
	IsDelete bit
)
go

/*
- TempType		: cart | wishlist | compare
*/
create table dbo.S_CartTemp
(
	CartTempID nvarchar(50) primary key,
	TempType nvarchar(20),
	UserID nvarchar(50),
	ProductID nvarchar(50),
	Quantity int,
	Notes nvarchar(2000),
	Overview nvarchar(2000),
	Summary nvarchar(4000),
	CreateDate datetime,
	ExpiredDate datetime
)
go

create table dbo.S_Order
(
	OrderID nvarchar(50) primary key,
	InvoiceID nvarchar(50),
	UserID nvarchar(50),
	
	Name nvarchar(100),
	Phone nvarchar(50),
	CellPhone nvarchar(50),
	Email nvarchar(100),
	Country nvarchar(50),
	[State] nvarchar(50),
	City nvarchar(50),
	[Address] nvarchar(200),
	ShipInfo nvarchar(4000),
	
	Tax money,
	ShipCost money,
	AdditionAmount money,
	PromoteInfo nvarchar(2000),
	
	PaymentType nvarchar(50),
	StatusID nvarchar(50),
	
	Overview nvarchar(2000),
	Summary nvarchar(2000),
	[Description] nvarchar(2000),
	Details nvarchar(max),
	
	Approved bit,
	IsDelete bit,
	
	CreateDate datetime,
	UpdateDate datetime,
	AprrovedDate datetime
)
go

create table dbo.S_OrderDetails
(
	DetailsID nvarchar(50) primary key,
	OrderID nvarchar(50),
	ProductID nvarchar(50),
	ProductName nvarchar(250),
	ProductCode nvarchar(50),
	Quantity int,
	Price money,
	Notes nvarchar(2000),
	Overview nvarchar(2000),
	Summary nvarchar(4000)
)
go


/* S_CartTemp */

create procedure dbo.S_CartTemp_GetByID
@CartTempID nvarchar(50),
@CultureCode nvarchar(10)
as
	BEGIN
		select 
			CT.*,
			P.Name as ProductName,
			P.CurrentPrice,
			P.PrimaryImage,
			P.ProductCode
		from S_CartTemp CT left outer join S_Product P
			on CT.ProductID = P.ProductID and P.CultureCode = @CultureCode
		where CT.CartTempID = @CartTempID
	END
go

/*
- TempType			: all | cart | wishlist | compare
*/
create procedure dbo.S_CartTemp_GetListByUser
@UserID nvarchar(50),
@TempType nvarchar(20),
@CultureCode nvarchar(10)
as
	BEGIN
		DECLARE @Query nvarchar(500)
		
		SET @Query = 'select '
		
		SET @Query += 'CT.*, '
		SET @Query += 'P.Name as ProductName, '
		SET @Query += 'P.CurrentPrice, '
		SET @Query += 'P.PrimaryImage, '
		SET @Query += 'P.ProductCode '
		
		SET @Query += 'from S_CartTemp CT left outer join S_Product P '
		SET @Query += 'on CT.ProductID = P.ProductID and P.CultureCode = ''' + @CultureCode + ''' '
		
		SET @Query += 'where CT.UserID = ''' + @UserID + ''' '
		
		IF @TempType <> 'all'
			SET @Query += 'and CT.TempType = ''' + @TempType + ''' '
		
		exec sp_executesql @Query
	END
go

/* BTS_Order */

create procedure dbo.S_Order_GetByID
@OrderID nvarchar(50)
as
	select 
		O.*,
		OS.Name as StatusName,
		(select SUM(Price*Quantity) from S_OrderDetails where OrderID = @OrderID) as TotalPrice
	from S_Order O
		left outer join 
			S_OrderStatus OS on O.StatusID = OS.StatusID
	where O.OrderID = @OrderID
go

/*
- UserID		: all | UserID
- StatusID		: all | StatusID
- Approved		: all | True | False
- IsDelete		: all | True | False
- DateColumn	: no : disable FromDate -> ToDate | CreateDate | UpdateDate | AprrovedDate
- Keyword		: 
- KeywordColumn	: all | ColumnName
- Orderby		: Column Name with desc/asc
*/
create procedure dbo.S_Order_GetList
@UserID nvarchar(50), 
@StartRow int, 
@EndRow int, 
@StatusID nvarchar(50), 
@Approved nvarchar(10), 
@IsDelete nvarchar(10),
@FromDate datetime, 
@ToDate datetime, 
@DateColumn nvarchar(50),
@Keyword nvarchar(200), 
@KeywordColumn nvarchar(50),
@Orderby nvarchar(20)
as
	BEGIN
		IF @Keyword <> ''
			SET @Keyword = dbo.Funtion_RemoveUTF8(@Keyword)
			
		DECLARE @Query nvarchar(1500)
		
		SET @Query = 'WITH TempTable as'
		SET @Query += '('
		
		SET @Query += 'select O.*, '
		SET @Query += 'OS.Name as StatusName, '
		SET @Query += '(select SUM(Quantity) from S_OrderDetails where OrderID = O.OrderID) as TotalDetails, '
		SET @Query += '(select SUM(Price*Quantity) from S_OrderDetails where OrderID = O.OrderID) as TotalPrice, '
		SET @Query += 'ROW_NUMBER() over (order by O.' + @OrderBy + ') as RowNum '
		
		SET @Query += 'from S_Order O '
		SET @Query += 'left outer join S_OrderStatus OS on O.StatusID = OS.StatusID '

		SET @Query += 'where 1 = 1'
		
		IF @UserID <> 'all'
			SET @Query += 'AND O.UserID = ''' + @UserID + ''' '
			
		IF @StatusID <> 'all'
			SET @Query += 'AND O.StatusID = ' + CAST(@StatusID As nvarchar(10)) + ' '
		
		IF @Approved <> 'all'
			SET @Query += 'AND O.Approved = ''' + @Approved + ''' '
		
		IF @IsDelete <> 'all'
			SET @Query += 'AND O.IsDelete = ''' + @IsDelete + ''' '
			
		IF @Keyword <> ''			
			BEGIN
				IF @KeywordColumn <> 'all'
					SET @Query += 'and dbo.Funtion_RemoveUTF8(O.' + @KeywordColumn + ') LIKE ''%' + @KeyWord + '%'' '
				ELSE
					BEGIN
						SET @Query += 'and ('
						SET @Query += 'dbo.Funtion_RemoveUTF8(O.Name) LIKE ''%' + @KeyWord + '%'' OR '
						SET @Query += 'dbo.Funtion_RemoveUTF8(O.Phone) LIKE ''%' + @KeyWord + '%'''
						SET @Query += ') '
					END					
			END
		
		IF @DateColumn <> 'no'
			BEGIN
				SET @Query += 'AND ''' + CAST(@FromDate As nvarchar(25)) + ''' <= O.' + @DateColumn + ' '
				SET @Query += 'AND ''' + CAST(@ToDate As nvarchar(25)) + ''' >= O.' + @DateColumn
			END
		
		SET @Query += ') '
		SET @Query += 'select *, (select COUNT(*) from TempTable) as TotalRow from TempTable where RowNum BETWEEN ' + CAST(@StartRow as nvarchar(10)) + ' AND ' + CAST(@EndRow as nvarchar(10))
		
		exec sp_executesql @Query
	END
go


/* S_OrderDetails */
create procedure dbo.S_OrderDetails_GetByID
@DetailsID nvarchar(50)
as
	select OD.*, 
		(select top 1 PrimaryImage from S_Product where ProductID = OD.ProductID) as PrimaryImage
	from S_OrderDetails OD where OD.DetailsID = @DetailsID
go

create procedure dbo.S_OrderDetails_GetByOrderID
@OrderID nvarchar(50)
as
	select OD.*, 
		(select top 1 PrimaryImage from S_Product where ProductID = OD.ProductID) as PrimaryImage
	from S_OrderDetails OD where OD.OrderID = @OrderID
go
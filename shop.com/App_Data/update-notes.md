### Planning :
	- Add option for method delete Product & Content & Order : permenent or not

# Jul 22, 2016
	- Add column IsDeleted to table S_Category, S_Product, S_Content_GetList
	- Allow to filter Category, Product, Content by IsDeleted
	- Add option "Permanant" to method delete Category, Content, Product

# Jul 20, 2016
	- Manage Log page : host role can view list log & log details

# Jun 27, 2016
	- Create log when add/update/delete content
	- Create log when add/update/delete order
	- Add yahoo, skype to site settings
	- Add enable/disable log to site settings

# Jun 18, 2016
	- Alter method Create Logged
	- Create log when admin create user
	- Create log when admin update user info
	- Create log when admin delete user

# Jun 16, 2016
	- Create log when add product
	- Create log when update product
	- Create log when delete product

# Jun 15, 2016
	- Add setting to enable/disable log
	- Add method DeepCopy to Helpers class
	- Create log when user login
	- Create log when user register
	- Create log when user update profile
	
# March 30, 2016
	- Add BackTop button to _Layout
	- Add Slick Slider (better for mobile) to Home page, can switch between Camera Slider & Slick Slider

# March 10, 2016
	- Alt Row Class (.gray) for all list in backend
	
# March 01, 2016
	- ManageProduct, ManageContent: fixed error if form doesn't have file control (select image to upload)
	- ManageProduct, ManageContent: allow setting disable/enable search
	
# February 23, 2016
	- Optimize angular service: workWithList.sortList: reverse can be true/false or desc/asc
	- multiContentSimple.js: after UpdateContent or UploadImage : sortList base on the setting

# January 19, 2016
	- ManageProduct, ManageContent: 
		+ Scroll to top when change page
		+ Allow to list all products / contents (dropdown filter by category)
		+ Allow to setting sort by column & direction in js file (can set for each content type)
	- Move the following js file to bundle (to prevent cache if file content changed)
		+ ~/Scripts/mProduct.min.js
		+ ~/Scripts/mContent.min.js
		+ ~/Scripts/mBanner.min.js
		+ ~/Scripts/mCategory.min.js
		+ ~/Scripts/mOrders.min.js
		+ ~/Scripts/multiContentSimple.min.js
		+ ~/Scripts/singleContent.min.js
		+ ~/Scripts/siteSettings.min.js
		+ ~/Scripts/sqlScript.min.js
	- MultiContentSimple: allow to sort by column & direction for each content type (setting in js file)	
	
# January 15, 2016
	- AdminController: method UploadAndResize: 
		+ Fixed auto add .png extension to small size images when upload
		+ If image original size < thumbs size: clone original image as thumbs (not resize & crop)
	
# January 13, 2016
	- Site.less: optimze @media screen with .less syntax, easy to maintenance
	- Global.asax.cs: always redirect to www.domain-name.xxx (if user type non www): good for SEO
	
# January 12, 2016
	- ManageProduct: optimize for mobile: header button in Form Edit Product, allow show/hide details images form, extends images form
	- ManageContent: optimize for mobile: header button in Form Edit Content, allow show/hide details images form, extends images form
	- ManageCategory: optimize list category for mobile

# January 11, 2016
	- Add jquery ui (css & js) to backend bundles: allow to use drag & drop
	- CSS: add overflow-x:hidden to class gb-page-width: fix horizontal scroll when using bootstrap grid (row & column)	

# December 22, 2015
	- set thumbs size from angular controller for ManageContent, SingleContent, MultiContentSimple. It's means allow to set thumbs size for each content type
	- ManageContent: set page size from angular controller. It's means allow to set page size for each content type
	- MultiContentSimple: allow to re-create all thumbs, can settings thumb size for each content type in angular controller

# December 17, 2015
	- Remove all admin skin Material design
	- Bundle for CSS & Scripts in backend
	
# December 15, 2015
	- Include bootstrap in font-end 
	- Style.css: changed mobile width to 767px (likes bootstrap)
	- Using bootstrap for some pages: Login, Register, UpdateProfile, ChangePassword, Contact 
	
# December 11, 2015
	- ManageProduct, ManageContent, ManageOrders, ManageUser: check press enter on search textbox
	- ManageProduct, ManageContent, ManageOrders: i18n for required keyword message on search
	
# December 03, 2015
	- change color style for datetime picker (Scripts\datetime-picker\DateTimePicker.css), likes bootstrap style
	
# December 01, 2015
	- when add to cart with quantity (product details page) or update cart quantity (checkout page): if quantity is not a number or quantity < 0, auto convert quantity = 1
	
# November 26, 2015
	- update angular service: getCurDateString (sGlobal.js): allow to add/minus month, date, year to the current datetime and return as string (mm-dd-yyyy or dd-mm-yyyy depends on culture code)
	- Remove Profile from Helpers class (Helpers.cs): for best performance & stability. To access UserProfile from Razor page, using: ViewBag.UserInfo.DisplayName, ....
	
# November 25, 2015
	- ManageProduct, ManageContent: allow set primary image from extends images & details images
	
# November 24, 2015
	- Allow to query in SqlScript
	
# November 23, 2015
	- Change Order status
	- Responsive for admin

# November 20, 2015
	- Allow users in role Admin create & manage user.
	- ManageUser: hide user ndtien84@gmail.com & hide Host role
	- Add admin page: AdvancedSettings (visible for Host only). This page is for Manage Order Status, Manage Role, ....
	
# November 17, 2015
	- add ForgotPassword page, user can reset password if forgot
	- mobile friendly for sitesettings
	- add SMTP config to sitesettings (visible for Host role only)
	- add Google Analytics & Google Map embed code to sitesettings (visible for Host role only)

# November 10, 2015
	- add field ProductCode for table S_OrderDetails
	- update angularjs: 1.3.14 -> 1.4.7

# November 9, 2015
	- ManageProduct: allow search product by Name or ProductCode
	- ManageProduct: add hashtag proid to the url when edit product (allow load exact the product when reload page, share the link or link edit from Fontend)
	- ManageContent: allow search content by Name
	- ManageContent: add hashtag ctid to the url when edit content (allow load exact the content when reload page, share the link or link edit from Fontend)
	- sGlobal.js: add 2 method: appHelpers.removeHash(hashName), appHelpers.setHash(hashName, hashValue)

# November 6, 2015
	- Add & modify some field to table S_Order: InvoiceID, Zip, Country, State, City, Address, ShipInfo, Tax, ShipCost, AdditionAmount, PromoteInfo
	- After Login or Register, move all CartTemp to new Registered/Logged in user
	- Add more field for table AspNetUsers: CreateDate, UpdateDate, LastLoginDate, FirstName, LastName, City, State, Country, Zip
	- Optimize procedure: Users_GetList, allow search user by ColumnName
	- ManageUser: allow search by: Name, Email, Phone
	- Update LastLoginDate for User when login

# November 5, 2015
	- Paging for ManageOrders
	- Optimize procedure: S_Order_GetList, allow to search order by ColumnName
	- Optimize method: S_Order_GetList (SController.cs): allow some parameters can be optional
	- ManageOrders: can search order by Email, Name, Phone, OrderID.
	- Auto detect date-time format (sGlobal.js)
	
# November 4, 2015
	- Optimize bundle script for font end
	- Create thumbs when upload image, the thumbs size can settings in AdminController.cs
	- At ManageProduct & ManageContent page: add method to re-create all thumbs (visible for role "Host" only). You can change the thumbs size setting in AdminController.cs and re-create thumbs. All of the old thumbs will be removed and replaced with the new ones with new size.
	
## November 2, 2015
	- Optimize in backend, allow work on IE 9.
	- Paging for manageuser
	- Optimize procedure: Users_GetList (users-roles.sql)
	- Optimize procedure: S_Order_GetList (cart.sql)
	
## November	1, 2015
	- Product & Content details (ckeditor), convert all absolute image (http://mydomain.com/s-media/myimage.jpg) url to relative url (/s-media/myimage.jpg) when save, to prevent broken image link when the site go online.
	- ManageProduct: extends images form can call from ManageProduct form, allow upload extends images before the product was created.
	- ManageContent: extends images form can call from ManageContent form, allow upload extends images before the content was created.

## October 30, 2015
	- optimize admin pages, admin page can switch between Bootstrap skin and Material Design skin

## October 28, 2015
	- optimize method S_Content_GetList (SController.cs): most op parameters can be optional
	- optimize method S_Product_GetList (SController.cs): most op parameters can be optional
	- optimize method S_Category_GetList, S_Category_GetListAllLevel (SController.cs): all of the parameters can be optional

## October 21, 2015
	- Add javascript function [appHelpers.getFormData]: get value of all control (textbox, textarea, select) in the form and return JSON. The control in the form must have name attribute
	- Add javascript function [appHelpers.clearFormData]: clear all value of the control (textbox, textarea). Set control (select) to index 0

## October 16, 2015
	- Add settings.maxItem in mContent.js : limit the total item. If $scope.totalRow >= settings.maxItem: button Add Content will invisible. This function just for client-side only, not validate on server-side.
	- Add update cart quantity function: CheckOut.cshtml
	- MultiContentSimple page (Admin) for simple content like Banner, content have a few field
	- ManageSingleContent page (Admin) for content have only 1 record like about-us ... Load the edit form immediately when page load
	- Add more fields for Site Settings: Slogan, LinkedIn, Pinterest, Youtube, Vimeo, Instagram, Tumblr, Flickr (SiteSettings.cshtml)

## October 14, 2015
	- Update method javascript [appHelpers.validate] (sGlobal.js): can validate <select></select> if the selected index == 0, by adding the following attr  to the select: ddl-select="true" data-msg="Please select the dropdown"

## October 11, 2015
	- Move all User function (login, logout, register, change-password, create-user, manage-user) to AccountController

## October 10, 2015
	- Add new feature: manage Content Extends Image (ManageContent)

## October 7, 2015
	- Add method GetRawPhoneNumber to shop.com.Helpers class
	- Using method: JsonConvert.DeserializeObject<T>(string) instead of JavaScriptSerializer -> Deserialize<T>(string). New method can convert date format: \/Date(1443752553687)\/
	- Edit procedure: S_Category_DeleteByID: set CategoryID = -1 in table S_Product where CategoryID = deleted CategoryID

## October 2, 2015
	- Add more column for table S_Product: IsOutOfStock(bit), Quantity(int)
	- New filter IsOutOfStock for method: S_Product_GetList

## September 28, 2015
	- i18n for Login, Register, UpdateProfile, ChangePassword, CreateUser

## September 25, 2015
	- Allow upload Details Images when Product/Content is not create yet (page: ManageProduct, ManageContent) (file: AdminController.cs, mProduct.js, mContent.js)
	(If user is upload details images but not create Product/Content, all those images becomes temp images)
	(Everytime user comes to ManageProduct page, it will check to delete temp images older than 1 day)
	- Delete all child Content images file when delete Product/Content (page: ManageProduct, ManageContent) (file: SController.cs)
	- Merge ndtGlobals.js & sGlobal.js into sGlobal.js
	- i18n for all js & .cshtml (in Admin) file (add new file: shopText.vi-vn.js, shopText.en-us.js)
	

## September 24, 2015
	- Resize image if width larger than 1280 pixel (page: ManageBanner, ManageProduct, ManageContent) (file: AdminController.cs)
	- Add overlay loading, check IsSending (prevent user double click on button Update), replace error message [console.log() by appHelpers.alertCall()] when update info (page: ManageBanner, ManageProduct, ManageContent) (file: mContent.js, mProduct.js, mBanner.js)
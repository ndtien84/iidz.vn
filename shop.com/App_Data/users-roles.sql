﻿/* AspNetUsers  */

/*
- Role			: all | name or id or role (depends on RoleType)
- RoleType		: name | id
- Keyword		:
- KeywordColumn	: all | ColumnName
*/

create procedure dbo.Users_GetList
@Role nvarchar(100), 
@RoleType nvarchar(10), 
@Keyword nvarchar(200), 
@KeywordColumn nvarchar(50),
@StartRow int, 
@EndRow int, 
@OrderBy nvarchar(20)
as
	BEGIN
		IF @Keyword <> ''
			SET @Keyword = dbo.Funtion_RemoveUTF8(@Keyword)
			
		DECLARE @Query nvarchar(1500)
		
		SET @Query = 'WITH TempTable as'
		SET @Query += '('
		
		SET @Query += 'select U.*, '
		SET @Query += 'ROW_NUMBER() over (order by U.' + @OrderBy + ') as RowNum '
		
		SET @Query += 'from '
		
		SET @Query += 'AspNetUsers U '
		SET @Query += 'left outer join AspNetUserRoles UR on U.Id = UR.RoleId '
		
		IF @Role <> 'all'
			BEGIN
				SET @Query += 'left outer join AspNetRoles R on R.Id = UR.RoleId '
				IF @RoleType = 'name'
					SET @Query += 'and R.Name = ' + @Role + ' '
				ELSE IF @RoleType = 'id'
					SET @Query += 'and R.Id = ' + @Role + ' '
			END
		
		SET @Query += 'where 1 = 1 '
		
		IF @Keyword <> ''			
			BEGIN
				IF @KeywordColumn <> 'all'
					BEGIN
						IF @KeywordColumn = 'Phone'
							BEGIN
								SET @Query += 'and ('
								SET @Query += 'dbo.Funtion_RemoveUTF8(U.CellPhone) LIKE ''%' + @KeyWord + '%'' OR '
								SET @Query += 'dbo.Funtion_RemoveUTF8(U.PhoneNumber) LIKE ''%' + @KeyWord + '%'''
								SET @Query += ') '
							END
						ELSE
							SET @Query += 'and dbo.Funtion_RemoveUTF8(U.' + @KeywordColumn + ') LIKE ''%' + @KeyWord + '%'' '
					END
				ELSE
					BEGIN
						SET @Query += 'and ('
						SET @Query += 'dbo.Funtion_RemoveUTF8(U.DisplayName) LIKE ''%' + @KeyWord + '%'' OR '
						SET @Query += 'dbo.Funtion_RemoveUTF8(U.CellPhone) LIKE ''%' + @KeyWord + '%'' OR '
						SET @Query += 'dbo.Funtion_RemoveUTF8(U.PhoneNumber) LIKE ''%' + @KeyWord + '%'' OR '
						SET @Query += 'dbo.Funtion_RemoveUTF8(U.Email) LIKE ''%' + @KeyWord + '%'''
						SET @Query += ') '
					END					
			END
		
		SET @Query += 'and U.UserName <> ''ndtien84@gmail.com'' '

		SET @Query += ') '
		SET @Query += 'select *, (select COUNT(*) from TempTable) as TotalRow from TempTable where RowNum BETWEEN ' + CAST(@StartRow as nvarchar(10)) + ' AND ' + CAST(@EndRow as nvarchar(10))

		exec sp_executesql @Query
	END
go



/* AspNetRoles  */
create procedure dbo.Roles_Update
@RoleID nvarchar(50), @RoleName nvarchar(100)
as
	BEGIN
		IF not exists (select * from AspNetRoles where Id = @RoleID)
			insert into AspNetRoles values(@RoleID, @RoleName)
		ELSE
			update AspNetRoles set Name = @RoleName where Id = @RoleID			
	END
go

create procedure dbo.Roles_AddUserRole
@UserID nvarchar(50), @RoleID nvarchar(50)
as
	insert into AspNetUserRoles values(@UserID, @RoleID)
go

create procedure dbo.Roles_DeleteUserRole
@UserID nvarchar(50)
as
	delete from AspNetUserRoles where UserId = @UserID
go	

create procedure dbo.Roles_DeleteByID
@RoleID nvarchar(50)
as
	BEGIN
		delete from AspNetRoles where Id = @RoleID
		delete from AspNetUserRoles where RoleId = @RoleID
	END		
go	

create procedure dbo.Roles_GetByID
@RoleID nvarchar(50)
as
	select * from AspNetRoles where Id = @RoleID
go	

create procedure dbo.Roles_GetListByUser
@UserID nvarchar(50)
as
	select
		R.*
	from		
		AspNetUserRoles UR
			left outer join AspNetRoles R on UR.RoleId = R.Id
	where UR.UserId = @UserID
go

create procedure dbo.Roles_GetList
as
	select * from AspNetRoles
go
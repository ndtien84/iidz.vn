﻿CREATE FUNCTION dbo.Funtion_RemoveUTF8
(
	@strInput NVARCHAR(4000)
) 
RETURNS NVARCHAR(4000)
as
	BEGIN
		Set @strInput=rtrim(ltrim(lower(@strInput)))
		IF @strInput IS NULL RETURN @strInput
		IF @strInput = '' RETURN @strInput
		Declare @text nvarchar(50), @i int
		Set @text='-''`~!@#$%^&*()?><:|}{,./\"''='';–'
		Select @i= PATINDEX('%['+@text+']%',@strInput ) 
		while @i > 0
			begin
				set @strInput = replace(@strInput, substring(@strInput, @i, 1), '')
				set @i = patindex('%['+@text+']%', @strInput)
			End
			Set @strInput =replace(@strInput,'  ',' ')
	        
		DECLARE @RT NVARCHAR(4000)
		DECLARE @SIGN_CHARS NCHAR(136)
		DECLARE @UNSIGN_CHARS NCHAR (136)
		SET @SIGN_CHARS = N'ăâđêôơưàảãạáằẳẵặắầẩẫậấèẻẽẹéềểễệế
					  ìỉĩịíòỏõọóồổỗộốờởỡợớùủũụúừửữựứỳỷỹỵý'
					  +NCHAR(272)+ NCHAR(208)
		SET @UNSIGN_CHARS = N'aadeoouaaaaaaaaaaaaaaaeeeeeeeeee
					  iiiiiooooooooooooooouuuuuuuuuuyyyyy'
		DECLARE @COUNTER int
		DECLARE @COUNTER1 int
		SET @COUNTER = 1
		WHILE (@COUNTER <=LEN(@strInput))
		BEGIN   
		  SET @COUNTER1 = 1
		   WHILE (@COUNTER1 <=LEN(@SIGN_CHARS)+1)
		   BEGIN
		 IF UNICODE(SUBSTRING(@SIGN_CHARS, @COUNTER1,1)) 
				= UNICODE(SUBSTRING(@strInput,@COUNTER ,1) )
		 BEGIN           
			  IF @COUNTER=1
				  SET @strInput = SUBSTRING(@UNSIGN_CHARS, @COUNTER1,1) 
				  + SUBSTRING(@strInput, @COUNTER+1,LEN(@strInput)-1)                   
			  ELSE
				  SET @strInput = SUBSTRING(@strInput, 1, @COUNTER-1) 
				  +SUBSTRING(@UNSIGN_CHARS, @COUNTER1,1) 
				  + SUBSTRING(@strInput, @COUNTER+1,LEN(@strInput)- @COUNTER)
				  BREAK
				   END
				 SET @COUNTER1 = @COUNTER1 +1
		   END
		   SET @COUNTER = @COUNTER +1
		End
	 SET @strInput = replace(@strInput,' ','-')
		RETURN lower(@strInput)
	END
GO

create table dbo.S_Settings
(
	SKey nvarchar(50),
	SType nvarchar(50),
	SValue nvarchar(500),
	primary key (SKey, SType)
)
go

/*
- CategoryType			: product | news | ...
*/
create table dbo.S_Category
(
	CategoryID nvarchar(50),
	CultureCode nvarchar(10),
	CategoryType nvarchar(20),
	GroupName nvarchar(50),
	ParentsID nvarchar(50),
	PrimaryImage nvarchar(200),
	SecondaryImage nvarchar(200),
	PrimaryDocument nvarchar(200),
	SecondaryDocument nvarchar(200),
	JsonGlobal nvarchar(4000),
	Label nvarchar(50),
	Priority int,
	Visible bit,
	IsDeleted bit,
	StartDate datetime,
	EndDate datetime,
	CreateBy nvarchar(50),
	LastUpdateBy nvarchar(50),
	CreateDate datetime,
	UpdateDate datetime,
	
	Name nvarchar(200),
	UrlKey nvarchar(200),
	JsonCulture nvarchar(4000),
	Overview nvarchar(2000),
	Information nvarchar(2000),
	Summary nvarchar(4000),
	Details nvarchar(max),
	
	primary key(CategoryID, CultureCode)
)
go

create table dbo.S_Product
(
	ProductID nvarchar(50),
	CultureCode nvarchar(10),
	CategoryID nvarchar(50),
	GroupName nvarchar(50),
	PrimaryImage nvarchar(200),
	SecondaryImage nvarchar(200),
	ReserveImage nvarchar(200),
	PrimaryDocument nvarchar(200),
	SecondaryDocument nvarchar(200),
	ReserveDocument nvarchar(200),
	JsonGlobal nvarchar(4000),
	Label nvarchar(50),
	Priority int,
	Visible bit,
	IsDeleted bit,
	IsOutOfStock bit,
	ProductCode nvarchar(50),
	Quantity int,
	ViewCount int,
	LikeCount int,
	OldPrice money,
	CurrentPrice money,
	PriceString nvarchar(200),
	Tag nvarchar(200),
	Link nvarchar(200),
	StartDate datetime,
	EndDate datetime,
	CreateBy nvarchar(50),
	LastUpdateBy nvarchar(50),
	CreateDate datetime,
	UpdateDate datetime,
	
	Name nvarchar(200),
	UrlKey nvarchar(200),
	JsonCulture nvarchar(4000),
	[Source] nvarchar(200),
	PostedBy nvarchar(200),
	[Description] nvarchar(4000),
	Overview nvarchar(4000),
	Information nvarchar(4000),
	Summary nvarchar(max),
	Details nvarchar(max),
	
	primary key (ProductID, CultureCode)
)
go

create table dbo.S_ProductCategory
(
	ProductID nvarchar(50),
	CategoryID nvarchar(50),
	primary key (ProductID, CategoryID)
)
go

/*
- ContentType		: product-image | category-image | banner | news | about-us
- ParentID			: -1 | ID from other table
- ParentsTable		: -1 | S_Category | S_Product | S_Content
*/

create table dbo.S_Content
(
	ContentID nvarchar(50),
	CultureCode nvarchar(10),
	CategoryID nvarchar(10),
	GroupName nvarchar(50),
	ContentType nvarchar(20),
	ParentsID nvarchar(50),
	ParentsTable nvarchar(50),
	PrimaryImage nvarchar(200),
	SecondaryImage nvarchar(200),
	ReserveImage nvarchar(200),
	PrimaryDocument nvarchar(200),
	SecondaryDocument nvarchar(200),
	ReserveDocument nvarchar(200),
	JsonGlobal nvarchar(4000),
	Label nvarchar(50),
	Link nvarchar(200),
	Tag nvarchar(200),
	Priority int,
	ViewCount int,
	LikeCount int,
	Visible bit,
	IsDeleted bit,
	StartDate datetime,
	EndDate datetime,
	CreateBy nvarchar(50),
	LastUpdateBy nvarchar(50),
	CreateDate datetime,
	UpdateDate datetime,
	
	Name nvarchar(200),
	UrlKey nvarchar(200),
	JsonCulture nvarchar(4000),
	[Description] nvarchar(4000),
	Overview nvarchar(4000),
	Information nvarchar(4000),
	Summary nvarchar(4000),
	Details nvarchar(max),
	
	primary key(ContentID, CultureCode)
)
go

create table S_ContentCategory
(
	ContentID nvarchar(50),
	CategoryID nvarchar(50),
	primary key (ContentID, CategoryID)
)
go

create table S_Log
(
	LogID nvarchar(50) primary key,
	LogType int,
	UserID nvarchar(50),
	Summary nvarchar(500),
	Details nvarchar(max),
	CreateDate datetime
)
go

/* #region S_Category */

create procedure dbo.S_Category_Update
@CategoryID nvarchar(50),
@CultureCode nvarchar(10),
@CategoryType nvarchar(20),
@GroupName nvarchar(50),
@ParentsID nvarchar(50),
@PrimaryImage nvarchar(200),
@SecondaryImage nvarchar(200),
@PrimaryDocument nvarchar(200),
@SecondaryDocument nvarchar(200),
@JsonGlobal nvarchar(4000),
@Label nvarchar(50),
@Priority int,
@Visible bit,
@IsDeleted bit,
@StartDate datetime,
@EndDate datetime,
@CreateBy nvarchar(50),
@LastUpdateBy nvarchar(50),

@Name nvarchar(200),
@UrlKey nvarchar(200),
@JsonCulture nvarchar(4000),
@Overview nvarchar(2000),
@Information nvarchar(2000),
@Summary nvarchar(4000),
@Details nvarchar(max)
as
	BEGIN
		IF not exists (select * from S_Category where CategoryID = @CategoryID and CultureCode = @CultureCode)
			BEGIN
				insert into S_Category values
				(
					@CategoryID,
					@CultureCode,
					@CategoryType,
					@GroupName,
					@ParentsID,
					@PrimaryImage,
					@SecondaryImage,
					@PrimaryDocument,
					@SecondaryDocument,
					@JsonGlobal,
					@Label,
					@Priority,
					@Visible,
					@IsDeleted,
					@StartDate,
					@EndDate,
					@CreateBy,
					@LastUpdateBy,
					GETDATE(),
					GETDATE(),

					@Name,
					@UrlKey,
					@JsonCulture,
					@Overview,
					@Information,
					@Summary,
					@Details
				)
			END
		ELSE
			BEGIN
				update S_Category set
					Name = @Name,
					UrlKey = @UrlKey,
					JsonCulture = @JsonCulture,
					Overview = @Overview,
					Information = @Information,
					Summary = @Summary,
					Details = @Details
				where CategoryID = @CategoryID and CultureCode = @CultureCode
			END
		
		-- update info for global column
		update S_Category set
			CategoryType = @CategoryType,
			GroupName = @GroupName,
			ParentsID = @ParentsID,
			PrimaryImage = @PrimaryImage,
			SecondaryImage = @SecondaryImage,
			PrimaryDocument = @PrimaryDocument,
			SecondaryDocument = @SecondaryDocument,
			JsonGlobal = @JsonGlobal,
			Label = @Label,
			Priority = @Priority,
			Visible = @Visible,
			IsDeleted = @IsDeleted,
			StartDate = @StartDate,
			EndDate = @EndDate,
			LastUpdateBy = @LastUpdateBy,
			UpdateDate = GETDATE()
		where CategoryID = @CategoryID
		
		-- return new object
		select * from S_Category where CategoryID = @CategoryID
	END
go

create procedure dbo.S_Category_DeleteByID
@CategoryID nvarchar(50), @Permanant bit
as
	BEGIN
		IF @Permanant = 1
			BEGIN
				delete from S_Category where CategoryID = @CategoryID
				update S_ProductCategory set CategoryID = '-1' where CategoryID = @CategoryID
				update S_Product set CategoryID = '-1' where CategoryID = @CategoryID
				delete from S_Content where ParentsTable = 'S_Category' and ParentsID = @CategoryID
				delete from S_ContentCategory where CategoryID = @CategoryID
				update S_Content set CategoryID = '-1' where CategoryID = @CategoryID
			END
		ELSE
			update S_Category set IsDeleted = 'True' where CategoryID = @CategoryID
	END
go

create procedure dbo.S_Category_GetByID
@CategoryID nvarchar(50), @CultureCode nvarchar(10)
as
	BEGIN
		IF @CultureCode = 'all'
			select *,
				(select COUNT(*) from S_ProductCategory where CategoryID = C.CategoryID) as ProductCount
			from 
				S_Category C where CategoryID = @CategoryID
		ELSE
			select *, 
				(select COUNT(*) from S_ProductCategory where CategoryID = C.CategoryID) as ProductCount
			from 
				S_Category C where CategoryID = @CategoryID and CultureCode = @CultureCode
	END
go

/*
- GetType		: full | simple
- CultureCode	: all | CultureCode
- CategoryType	: all | product | news | ...
- GroupName		: all | GroupName
- CreateBy		: all | UserID
- ParentsID		: all | ParentsID
- Label			: all | Label
- Visible		: all | True | False
- IsDeleted		: all | True | False
- ByDateRange	: True : get by StartDate < Now < EndDate
- OrderBy		: ColumnName with desc/asc
*/
create procedure dbo.S_Category_GetList
@GetType nvarchar(20),
@CultureCode nvarchar(10),
@CategoryType nvarchar(20),
@GroupName nvarchar(50),
@CreateBy nvarchar(50),
@ParentsID nvarchar(50),
@Label nvarchar(50),
@Visible nvarchar(10),
@IsDeleted nvarchar(10),
@ByDateRange bit,
@OrderBy nvarchar(30)
as
	BEGIN
		DECLARE @Query nvarchar(1000)
		
		SET @Query = 'select '
		
		IF @GetType = 'full'
			SET @Query += 'C.*, '
		ELSE IF @GetType = 'simple'
			SET @Query += 'C.CategoryID, C.CultureCode, C.Name, '
		
		IF @CategoryType = 'product'
			SET @Query += '(select COUNT(*) from S_ProductCategory where CategoryID = C.CategoryID) as ChildCount '
		ELSE
			SET @Query += '(select COUNT(*) from S_Content where CategoryID = C.CategoryID) as ChildCount '
		
		SET @Query += 'from S_Category C '
		
		SET @Query += 'where 1 = 1 '
		
		IF @CategoryType <> 'all'
			SET @Query += 'and C.CategoryType = ''' + @CategoryType + ''' '
		
		IF @GroupName <> 'all'
			SET @Query += 'and C.GroupName = ''' + @GroupName + ''' '
		
		IF @CreateBy <> 'all'
			SET @Query += 'and C.CreateBy = ''' + @CreateBy + ''' '
		
		IF @CultureCode <> 'all'
			SET @Query += 'and C.CultureCode = ''' + @CultureCode + ''' '
		
		IF @ParentsID <> 'all'
			SET @Query += 'and C.ParentsID = ''' + @ParentsID + ''' '
		
		IF @Label <> 'all'
			SET @Query += 'and C.Label LIKE ''%' + @Label + '%'' '
		
		IF @Visible <> 'all'
			SET @Query += 'and C.Visible = ''' + @Visible + ''' '
			
		IF @IsDeleted <> 'all'
			SET @Query += 'and C.IsDeleted = ''' + @IsDeleted + ''' '			
		
		IF @ByDateRange = 'True'
			SET @Query += 'and GETDATE() >= C.StartDate and GETDATE() <= C.EndDate '
		
		SET @Query += 'order by C.' + @OrderBy
		
		exec sp_executesql @Query
	END
go

/* #endregion */



/* #region S_Product */

create procedure dbo.S_Product_Update
@ProductID nvarchar(50),
@CultureCode nvarchar(10),
@CategoryID nvarchar(50),
@GroupName nvarchar(50),
@PrimaryImage nvarchar(200),
@SecondaryImage nvarchar(200),
@ReserveImage nvarchar(200),
@PrimaryDocument nvarchar(200),
@SecondaryDocument nvarchar(200),
@ReserveDocument nvarchar(200),
@JsonGlobal nvarchar(4000),
@Label nvarchar(50),
@Priority int,
@Visible bit,
@IsDeleted bit,
@IsOutOfStock bit,
@ProductCode nvarchar(50),
@Quantity int,
@ViewCount int,
@LikeCount int,
@OldPrice money,
@CurrentPrice money,
@PriceString nvarchar(200),
@Tag nvarchar(200),
@Link nvarchar(200),
@StartDate datetime,
@EndDate datetime,
@CreateBy nvarchar(50),
@LastUpdateBy nvarchar(50),

@Name nvarchar(200),
@UrlKey nvarchar(200),
@JsonCulture nvarchar(4000),
@Source nvarchar(200),
@PostedBy nvarchar(200),
@Description nvarchar(4000),
@Overview nvarchar(4000),
@Information nvarchar(4000),
@Summary nvarchar(max),
@Details nvarchar(max)
as
	BEGIN
		IF not exists (select * from S_Product where ProductID = @ProductID and CultureCode = @CultureCode)
			BEGIN
				insert into S_Product values
				(
					@ProductID,
					@CultureCode,
					@CategoryID,
					@GroupName,
					@PrimaryImage,
					@SecondaryImage,
					@ReserveImage,
					@PrimaryDocument,
					@SecondaryDocument,
					@ReserveDocument,
					@JsonGlobal,
					@Label,
					@Priority,
					@Visible,
					@IsDeleted,
					@IsOutOfStock,
					@ProductCode,
					@Quantity,
					@ViewCount,
					@LikeCount,
					@OldPrice,
					@CurrentPrice,
					@PriceString,
					@Tag,
					@Link,
					@StartDate,
					@EndDate,
					@CreateBy,
					@LastUpdateBy,
					GETDATE(),
					GETDATE(),

					@Name,
					@UrlKey,
					@JsonCulture,
					@Source,
					@PostedBy,
					@Description,
					@Overview,
					@Information,
					@Summary,
					@Details
				)
			END
		ELSE
			BEGIN
				update S_Product set
					Name = @Name,
					UrlKey = @UrlKey,
					JsonCulture = @JsonCulture,
					[Source] = @Source,
					PostedBy = @PostedBy,
					[Description] = @Description,
					Overview = @Overview,
					Information = @Information,
					Summary = @Summary,
					Details = @Details
				where ProductID = @ProductID and CultureCode = @CultureCode
			END
		
		-- update info for global column
		update S_Product set
			CategoryID = @CategoryID,
			GroupName = @GroupName,
			PrimaryImage = @PrimaryImage,
			SecondaryImage = @SecondaryImage,
			ReserveImage = @ReserveImage,
			PrimaryDocument = @PrimaryDocument,
			SecondaryDocument = @SecondaryDocument,
			ReserveDocument = @ReserveDocument,
			JsonGlobal = @JsonGlobal,
			Label = @Label,
			Priority = @Priority,
			Visible = @Visible,
			IsDeleted = @IsDeleted,
			IsOutOfStock = @IsOutOfStock,
			ProductCode = @ProductCode,
			Quantity = @Quantity,
			ViewCount = @ViewCount,
			LikeCount = @LikeCount,
			OldPrice = @OldPrice,
			CurrentPrice = @CurrentPrice,
			PriceString = @PriceString,
			Tag = @Tag,
			Link = @Link,
			StartDate = @StartDate,
			EndDate = @EndDate,
			LastUpdateBy = @LastUpdateBy,
			UpdateDate = GETDATE()
		where ProductID = @ProductID			
		
		-- return new object
		exec S_Product_GetByID @ProductID, 'all'
	END
go

create procedure dbo.S_Product_DeleteByID
@ProductID nvarchar(50), @Permanant bit
as
	BEGIN
		IF @Permanant  = 1
			BEGIN
				delete from S_Product where ProductID = @ProductID
				delete from S_ProductCategory where ProductID = @ProductID
				delete from S_Content where ParentsTable = 'S_Product' and ParentsID = @ProductID
			END
		ELSE
			update S_Product set IsDeleted = 'True' where ProductID = @ProductID
	END
go

create procedure dbo.S_Product_GetByID
@ProductID nvarchar(50), @CultureCode nvarchar(10)
as
	BEGIN
		IF @CultureCode = 'all'
			select
				P.*,
				C.Name as CategoryName
			from S_Product P 
				left outer join S_Category C on C.CategoryID = P.CategoryID
			where P.ProductID = @ProductID	
		ELSE
			select
				P.*,
				C.Name as CategoryName
			from S_Product P 
				left outer join S_Category C on C.CategoryID = P.CategoryID
			where 
				P.ProductID = @ProductID and P.CultureCode = @CultureCode
	END
go

/*
- GetType			: full | simple
- CultureCode		: all | CultureCode
- GroupName			: all | GroupName
- CategoryID		: all | CategoryID
- ExceptProductID	: -1 | ProductID : except by ProductID: case get related product
- CreateBy			: all | UserID
- Label				: all | Label
- Visible			: all | True | False
- IsDeleted			: all | True | False
- IsOutOfStock		: all | True | False
- ByDateRange		: True : get by StartDate < Now < EndDate
- Keyword			: Search by keyword, leave blank if not search
- KeywordColumn		: all (search in column Name, Tag) | name of the column which search in
- PriceRangeColumn	: no | OldPrice | CurrentPrice
- OrderBy			: ColumnName with desc/asc
*/
create procedure dbo.S_Product_GetList
@GetType nvarchar(20),
@CultureCode nvarchar(10), 
@GroupName nvarchar(50),
@CategoryID nvarchar(500),
@ExceptProductID nvarchar(50),
@CreateBy nvarchar(50),
@Label nvarchar(50),
@Visible nvarchar(10),
@IsDeleted nvarchar(10),
@IsOutOfStock nvarchar(10),
@ByDateRange bit,
@Keyword nvarchar(200), 
@KeywordColumn nvarchar(50),
@PriceRangeColumn nvarchar(50),
@StartPrice money,
@EndPrice money,
@StartRow int, 
@EndRow int, 
@OrderBy nvarchar(30)
as
	BEGIN
		DECLARE @Query nvarchar(2000)
		
		IF @Keyword <> ''
			SET @Keyword = dbo.Funtion_RemoveUTF8(@Keyword)
			
		IF @OrderBy = 'NEWID()'
			SET @OrderBy = 'order by NEWID()'
		ELSE			
			SET @OrderBy = 'order by P.' + @OrderBy


		SET @Query = 'WITH TempTable as'
		SET @Query += '('
		
		SET @Query += 'select '
		
		IF @GetType = 'full'
			SET @Query += 'P.*, '
		ELSE IF @GetType = 'simple'
			SET @Query += 'P.ProductID, P.CultureCode, P.Name, '
		
		SET @Query += 'C.Name as CategoryName, '
		
		SET @Query += 'ROW_NUMBER() over (' + @OrderBy + ') as RowNum '
		
		SET @Query += 'from S_Product P '
		SET @Query += 'left outer join S_ProductCategory PC on P.ProductID = PC.ProductID '
		SET @Query += 'left outer join S_Category C on C.CategoryID = P.CategoryID and C.CultureCode = P.CultureCode '
		
		SET @Query += 'where 1 = 1 '

		IF @GroupName <> 'all'
			SET @Query += 'and P.GroupName = ''' + @GroupName + ''' '
		
		IF @CultureCode <> 'all'
			SET @Query += 'and P.CultureCode = ''' + @CultureCode + ''' '
		
		IF @CategoryID <> 'all'
			SET @Query += 'and (P.CategoryID IN (' + @CategoryID + ') OR PC.CategoryID IN (' + @CategoryID + ')) '
			
		IF @ExceptProductID <> '-1'
			SET @Query += 'and P.ProductID <> ''' + @ExceptProductID + ''' '
		
		IF @CreateBy <> 'all'			
			SET @Query += 'and P.CreateBy = ''' + @CreateBy + ''' '
		
		IF @Label <> 'all'	
			SET @Query += 'and P.Label LIKE ''%' + @Label + '%'' '
		
		IF @Visible <> 'all'
			SET @Query += 'and P.Visible = ''' + @Visible + ''' '
			
		IF @IsDeleted <> 'all'
			SET @Query += 'and P.IsDeleted = ''' + @IsDeleted + ''' '			
		
		IF @IsOutOfStock <> 'all'
			SET @Query += 'and P.IsOutOfStock = ''' + @IsOutOfStock + ''' '			
			
		IF @ByDateRange = 'True'	
			SET @Query += 'and GETDATE() >= P.StartDate and GETDATE() <= P.EndDate '
			
		IF @Keyword <> ''			
			BEGIN
				IF @KeywordColumn <> 'all'
					IF @KeywordColumn = 'Name,Code'
						BEGIN
							SET @Query += 'and ('
							SET @Query += 'dbo.Funtion_RemoveUTF8(P.Name) LIKE ''%' + @KeyWord + '%'' OR '
							SET @Query += 'dbo.Funtion_RemoveUTF8(P.ProductCode) LIKE ''%' + @KeyWord + '%'''
							SET @Query += ') '
						END
					ELSE						
						SET @Query += 'and dbo.Funtion_RemoveUTF8(P.' + @KeywordColumn + ') LIKE ''%' + @KeyWord + '%'' '
				ELSE
					BEGIN
						SET @Query += 'and ('
						SET @Query += 'dbo.Funtion_RemoveUTF8(P.Name) LIKE ''%' + @KeyWord + '%'' OR '
						SET @Query += 'dbo.Funtion_RemoveUTF8(P.Tag) LIKE ''%' + @KeyWord + '%'''
						SET @Query += ') '
					END					
			END
		
		IF @PriceRangeColumn <> 'no'
			SET @Query += 'and P.' + @PriceRangeColumn + ' >= ' + CAST(@StartPrice as nvarchar(10)) + ' and P.' + @PriceRangeColumn + ' <= ' + CAST(@EndPrice as nvarchar(10)) + ' '
			
		SET @Query += ') '
		
		SET @Query += 'select *, (select COUNT(*) from TempTable) as TotalRow from TempTable where RowNum BETWEEN ' + CAST(@StartRow as nvarchar(10)) + ' AND ' + CAST(@EndRow as nvarchar(10))

		exec sp_executesql @Query
	END
go	

/* #endregion */


/* #region S_Content */

create procedure dbo.S_Content_Update
@ContentID nvarchar(50),
@CultureCode nvarchar(10),
@CategoryID nvarchar(50),
@GroupName nvarchar(50),
@ContentType nvarchar(20),
@ParentsID nvarchar(50),
@ParentsTable nvarchar(50),
@PrimaryImage nvarchar(200),
@SecondaryImage nvarchar(200),
@ReserveImage nvarchar(200),
@PrimaryDocument nvarchar(200),
@SecondaryDocument nvarchar(200),
@ReserveDocument nvarchar(200),
@JsonGlobal nvarchar(4000),
@Label nvarchar(50),
@Link nvarchar(200),
@Tag nvarchar(200),
@Priority int,
@ViewCount int,
@LikeCount int,
@Visible bit,
@IsDeleted bit,
@StartDate datetime,
@EndDate datetime,
@CreateBy nvarchar(50),
@LastUpdateBy nvarchar(50),

@Name nvarchar(200),
@UrlKey nvarchar(200),
@JsonCulture nvarchar(4000),
@Description nvarchar(4000),
@Overview nvarchar(4000),
@Information nvarchar(4000),
@Summary nvarchar(4000),
@Details nvarchar(max)
as
	BEGIN
		IF not exists (select * from S_Content where ContentID = @ContentID and CultureCode = @CultureCode)
			BEGIN
				insert into S_Content values
				(
					@ContentID,
					@CultureCode,
					@CategoryID,
					@GroupName,
					@ContentType,
					@ParentsID,
					@ParentsTable,
					@PrimaryImage,
					@SecondaryImage,
					@ReserveImage,
					@PrimaryDocument,
					@SecondaryDocument,
					@ReserveDocument,
					@JsonGlobal,
					@Label,
					@Link,
					@Tag,
					@Priority,
					@ViewCount,
					@LikeCount,
					@Visible,
					@IsDeleted,
					@StartDate,
					@EndDate,
					@CreateBy,
					@LastUpdateBy,
					GETDATE(),
					GETDATE(),

					@Name,
					@UrlKey,
					@JsonCulture,
					@Description,
					@Overview,
					@Information,
					@Summary,
					@Details
				)
			END
		ELSE
			BEGIN
				update S_Content set
					Name = @Name,
					UrlKey = @UrlKey,
					JsonCulture = @JsonCulture,
					[Description] = @Description,
					Overview = @Overview,
					Information = @Information,
					Summary = @Summary,
					Details = @Details
				where ContentID = @ContentID and CultureCode = @CultureCode					
			END
		
		-- update info for global column
		update S_Content set
			CategoryID = @CategoryID,
			GroupName = @GroupName,
			ContentType = @ContentType,
			ParentsID = @ParentsID,
			ParentsTable = @ParentsTable,
			PrimaryImage = @PrimaryImage,
			SecondaryImage = @SecondaryImage,
			ReserveImage = @ReserveImage,
			PrimaryDocument = @PrimaryDocument,
			SecondaryDocument = @SecondaryDocument,
			ReserveDocument = @ReserveDocument,
			JsonGlobal = @JsonGlobal,
			Label = @Label,
			Link = @Link,
			Tag = @Tag,
			Priority = @Priority,
			ViewCount = @ViewCount,
			LikeCount = @LikeCount,
			Visible = @Visible,
			IsDeleted = @IsDeleted,
			StartDate = @StartDate,
			EndDate = @EndDate,
			LastUpdateBy = @LastUpdateBy,
			UpdateDate = GETDATE()
		where ContentID = @ContentID		
				
		-- return new object
		select * from S_Content where ContentID = @ContentID
	END
go

create procedure dbo.S_Content_DeleteByID
@ContentID nvarchar(50), @Permanant bit
as
	BEGIN
		IF @Permanant = 1
			BEGIN
				delete from S_Content where ContentID = @ContentID
				delete from S_ContentCategory where ContentID = @ContentID
				delete from S_Content where ParentsID = @ContentID and ParentsTable = 'S_Content'
			END
		ELSE
			update S_Content set IsDeleted = 'True' where ContentID = @ContentID
	END
go

create procedure dbo.S_Content_GetByID
@ContentID nvarchar(50), @CultureCode nvarchar(10)
as
	BEGIN
		IF @CultureCode <> 'all'
			select * from S_Content where ContentID = @ContentID and CultureCode = @CultureCode
		ELSE
			select * from S_Content where ContentID = @ContentID
	END
go

/*
- GetType			: full | simple
- CultureCode		: all | CultureCode
- CategoryID		: all | CategoryID
- ExceptContentID	: -1 | ContentID : except by ContentID: case get related content
- GroupName			: all | GroupName
- CreateBy			: all | UserID
- Label				: all | Label
- ContentType		: all | product-image | category-image | banner | news | about-us
- ParentsID			: all | -1 | ParentsID
- ParentsTable		: all | -1 | S_Category | S_Product | S_Content
- Visible			: all | True | False
- IsDeleted			: all | True | False
- ByDateRange		: True : get by StartDate < Now < EndDate
- Keyword			: Search by keyword, leave blank if not search
- KeywordColumn		: all (search in column Name, Tag) | name of the column which search in
- OrderBy			: column name (CreateDate, Priority, ...) with asc or desc
*/
create procedure dbo.S_Content_GetList
@GetType nvarchar(20),
@CultureCode nvarchar(10), 
@CategoryID nvarchar(300),
@ExceptContentID nvarchar(50),
@GroupName nvarchar(50),
@CreateBy nvarchar(50),
@Label nvarchar(10),
@ContentType nvarchar(20), 
@ParentsID nvarchar(50), 
@ParentsTable nvarchar(50),
@StartRow int, 
@EndRow int, 
@Visible nvarchar(20),
@IsDeleted nvarchar(20),
@ByDateRange bit,
@Keyword nvarchar(200),
@KeywordColumn nvarchar(50),
@OrderBy nvarchar(30)
as
	BEGIN
		IF @Keyword <> ''
			SET @Keyword = dbo.Funtion_RemoveUTF8(@Keyword)
			
		DECLARE @Query nvarchar(1800)
		DECLARE @ParentsIDFieldName nvarchar(50)
				
		-- Get ParentsIDFieldName
		SET @ParentsIDFieldName = ''
		IF @ParentsTable = 'S_Category'
			SET @ParentsIDFieldName = 'CategoryID'
		ELSE IF @ParentsTable = 'S_Product'
			SET @ParentsIDFieldName = 'ProductID'
		ELSE IF @ParentsTable = 'S_Content'			
			SET @ParentsIDFieldName = 'ContentID'
		-- End Get ParentsIDFieldName
		
		IF @OrderBy = 'NEWID()'
			SET @OrderBy = 'order by NEWID()'
		ELSE			
			SET @OrderBy = 'order by C.' + @OrderBy
		
		
		SET @Query = 'WITH TempTable as'
		SET @Query += '('
		
		SET @Query += 'select '
		
		IF @GetType = 'full'
			SET @Query += 'C.*, '
		ELSE IF @GetType = 'simple'			
			SET @Query += 'C.ContentID, C.CultureCode, C.Name, '
		
		IF @ParentsID <> '-1' and @ParentsTable <> 'all'
			BEGIN
				IF @CultureCode = 'all'
					SET @Query +=  '(select top 1 Name from ' + @ParentsTable + ' where ' + @ParentsIDFieldName + ' = ''' + @ParentsID + ''') as ParentsName, '
				ELSE
					SET @Query +=  '(select Name from ' + @ParentsTable + ' where ' + @ParentsIDFieldName + ' = ''' + @ParentsID + ''' and CultureCode = ''' + @CultureCode + ''') as ParentsName, '
			END
		
		SET @Query += 'ROW_NUMBER() over (' + @OrderBy + ') as RowNum '
		
		SET @Query += 'from S_Content C '
		
		SET @Query += 'left outer join S_ContentCategory CC on CC.ContentID = C.ContentID '
		
		SET @Query += 'where 1 = 1 ' 
		
		IF @CategoryID <> 'all'
			SET @Query += 'and (C.CategoryID IN (' + @CategoryID + ') OR CC.CategoryID IN (' + @CategoryID +')) '
		
		IF @ExceptContentID <> '-1'
			SET @Query += 'and C.ContentID <> ''' + @ExceptContentID + ''' '

		IF @GroupName <> 'all'
			SET @Query += 'and C.GroupName = ''' + @GroupName + ''' '
		
		IF @CultureCode <> 'all'
			SET @Query += 'and C.CultureCode = ''' + @CultureCode + ''' '
		
		IF @CreateBy <> 'all'
			SET @Query += 'and C.CreateBy = ''' + @CreateBy + ''' '
			
		IF @Label <> 'all'
			SET @Query += 'and C.Label LIKE ''%' + @Label + '%'' '			
		
		IF @ContentType <> 'all'
			SET @Query += 'and C.ContentType = ''' + @ContentType + ''' '

		IF @ParentsID <> 'all'
			SET @Query += 'and C.ParentsID = ''' + @ParentsID + ''' '

		IF @ParentsTable <> 'all'
			SET @Query += 'and C.ParentsTable = ''' + @ParentsTable + ''' '
		
		IF @Visible <> 'all'
			SET @Query += 'and C.Visible = ''' + @Visible + ''' '
			
		IF @IsDeleted <> 'all'
			SET @Query += 'and C.IsDeleted = ''' + @IsDeleted + ''' '			
		
		IF @ByDateRange = 'True'
			SET @Query += 'and GETDATE() >= C.StartDate and GETDATE() <= C.EndDate '
		
		IF @Keyword <> ''
			BEGIN
				IF @KeywordColumn <> 'all'
					SET @Query += 'and dbo.Funtion_RemoveUTF8(C.' + @KeywordColumn + ') LIKE ''%' + @KeyWord + '%'''
				ELSE
					BEGIN
						SET @Query += 'and ('
						SET @Query += 'dbo.Funtion_RemoveUTF8(C.Name) LIKE ''%' + @KeyWord + '%'' OR '
						SET @Query += 'dbo.Funtion_RemoveUTF8(C.Tag) LIKE ''%' + @KeyWord + '%'''
						SET @Query += ')'
					END
			END

		SET @Query += ') '
		
		SET @Query += 'select *, (select COUNT(*) from TempTable) as TotalRow from TempTable where RowNum BETWEEN ' + CAST(@StartRow as nvarchar(10)) + ' AND ' + CAST(@EndRow as nvarchar(10))

		exec sp_executesql @Query
	END
go

/* #endregion */


/* #region S_Log */
drop procedure dbo.S_Log_GetList
go
create procedure dbo.S_Log_GetList
@LogType int,
@UserID nvarchar(50),
@DateColumn nvarchar(50),
@FromDate datetime,
@ToDate datetime,
@StartRow int, 
@EndRow int,
@OrderBy nvarchar(50)
as
	BEGIN
		DECLARE @Query nvarchar(1000)
		
		SET @Query = 'WITH TempTable as'
		SET @Query += '('
		
		SET @Query += 'select '
		SET @Query += 'L.*, '
		SET @Query += 'U.DisplayName, '
		SET @Query += 'ROW_NUMBER() over (order by L.' + @OrderBy + ') as RowNum '
		
		SET @Query += 'from S_Log L '
		SET @Query += 'left outer join AspNetUsers U on L.UserID = U.Id '
		SET @Query += 'where 1 = 1'
		
		IF @LogType <> 0
			SET @Query += 'and L.LogType = ' + CAST(@LogType as nvarchar(10)) + ' '
		IF @UserID <> 'all'
			SET @Query += 'and L.UserID = ''' + @UserID + ''' '		
			
		IF @DateColumn <> 'disable'
			BEGIN
				SET @Query += 'AND ''' + CAST(@FromDate As nvarchar(25)) + ''' <= L.' + @DateColumn + ' '
				SET @Query += 'AND ''' + CAST(@ToDate As nvarchar(25)) + ''' >= L.' + @DateColumn + ' '
			END				
		
		SET @Query += ')'
		SET @Query += 'select *, (select COUNT(*) from TempTable) as TotalRow from TempTable where RowNum BETWEEN ' + CAST(@StartRow as nvarchar(10)) + ' AND ' + CAST(@EndRow as nvarchar(10))
		
		exec sp_executesql @Query
	END
go	


/* #endregion */



select 
	C.*
from	
	S_Content C
		left outer join S_ContentCategory CC on C.ContentID = CC.ContentID
where C.CategoryID = '66d6a477' or CC.CategoryID = '66d6a477'
		
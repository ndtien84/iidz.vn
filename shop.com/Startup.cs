﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(shop.com.Startup))]
namespace shop.com
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

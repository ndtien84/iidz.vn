﻿using System.Web.Optimization;
using shop.com.Utils;
using System.Configuration;

namespace shop.com
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bool UsingMinJS = bool.Parse(ConfigurationManager.AppSettings["UsingMinJS"]);
            string JSPrefix = UsingMinJS ? ".min.js" : ".js";


            #region For Fontend

            bundles.Add(new StyleBundle("~/Content/SiteStyle").Include(
                "~/Content/bootstrap-3.3.7/css/bootstrap.min.css",
                "~/Content/Site.min.css"
            ));

            bundles.Add(new ScriptBundle("~/Scripts/SiteScripts").Include(
                "~/Scripts/jquery-1.11.1.min.js",
                "~/Scripts/angular.min.js",
                "~/Content/bootstrap-3.3.7/js/bootstrap.min.js",
                "~/Scripts/sGlobal" + JSPrefix,
                "~/Scripts/shopCtrl" + JSPrefix
            ));

            #endregion


            #region For Backend

            bundles.Add(new StyleBundle("~/Content/BackendStyle").Include(
                "~/Content/bootstrap-3.3.7/css/bootstrap.min.css",
                "~/Scripts/dropdown.js-master/jquery.dropdown.css",
                "~/Content/smoothness/jquery-ui.min.css",
                "~/Content/Site.min.css"
            ));

            bundles.Add(new ScriptBundle("~/Scripts/BackendScripts").Include(
                "~/Scripts/shopText." + Helpers.DefaultCultureCode + ".js",
                "~/Scripts/jquery-1.11.1.min.js",
                "~/Scripts/jquery-ui-1.11.4.min.js",
                "~/Scripts/angular.min.js",
                "~/Scripts/ndtLightbox" + JSPrefix,
                "~/Scripts/sGlobal" + JSPrefix,
                //"~/Scripts/image-scale.min.js",
                "~/Content/bootstrap-3.3.7/js/bootstrap.min.js",
                "~/Scripts/dropdown.js-master/jquery.dropdown.js"
            ));

            #endregion


            bundles.Add(new ScriptBundle("~/Scripts/ManageProduct").Include(
                "~/Scripts/mProduct" + JSPrefix
            ));

            bundles.Add(new ScriptBundle("~/Scripts/ManageContent").Include(
                "~/Scripts/mContent" + JSPrefix
            ));

            bundles.Add(new ScriptBundle("~/Scripts/ManageBanner").Include(
                "~/Scripts/mBanner" + JSPrefix
            ));

            bundles.Add(new ScriptBundle("~/Scripts/ManageCategory").Include(
                "~/Scripts/mCategory" + JSPrefix
            ));

            bundles.Add(new ScriptBundle("~/Scripts/ManageOrders").Include(
                "~/Scripts/datetime-picker/DateTimePicker.js",
                "~/Scripts/mOrders" + JSPrefix
            ));

            bundles.Add(new ScriptBundle("~/Scripts/MultiContentSimple").Include(
                "~/Scripts/multiContentSimple" + JSPrefix
            ));

            bundles.Add(new ScriptBundle("~/Scripts/SingleContent").Include(
                "~/Scripts/singleContent" + JSPrefix
            ));

            bundles.Add(new ScriptBundle("~/Scripts/SiteSettings").Include(
                "~/Scripts/siteSettings" + JSPrefix
            ));

            bundles.Add(new ScriptBundle("~/Scripts/SqlScript").Include(
                "~/Scripts/sqlScript" + JSPrefix
            ));

            bundles.Add(new ScriptBundle("~/Scripts/manageLog").Include(
                "~/Scripts/datetime-picker/DateTimePicker.js",
                "~/Scripts/manageLog" + JSPrefix
            ));


            BundleTable.EnableOptimizations = bool.Parse(ConfigurationManager.AppSettings["MinifyJS"]);
        }
    }
}

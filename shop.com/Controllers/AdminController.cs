﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using shop.com.Models;
using System.IO;
using System.Collections;
using System.Reflection;
using System.Web.Helpers;

using Newtonsoft.Json;
using System.Linq;
using System.Net.Mail;
using System.Net;
using shop.com.Utils;
using shop.com.Services;

namespace shop.com.Controllers
{
    [Authorize]
    [ValidateInput(false)]
    [Authorize(Roles = "Host, Admin")]
    public class AdminController : SBaseController
    {
        private int ProductPageSize = 10;
        private int TotalPage = 0;
        private int TotalRow = 0;

        private int ThumbsSizeProduct = 270;
        //private int ThumbsSizeContent = 270;


        #region SqlScript

        [Authorize(Roles = "Host")]
        public ActionResult SqlScript()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Host")]
        public JsonResult SqlScript(string act, string data)
        {
            Hashtable JsonTable = new Hashtable();

            try
            {
                if (act == "submit-sql")
                {
                    Hashtable QueryTable = JsonConvert.DeserializeObject<Hashtable>(data);
                    string SqlCommand = QueryTable["sqlCommand"].ToString();
                    bool RunAsScript = bool.Parse(QueryTable["runAsScript"].ToString());

                    CBO CBOCtrl = new CBO();

                    if (RunAsScript)
                    {
                        CBOCtrl.CreatePro(SqlCommand);
                    }
                    else
                    {
                        List<Hashtable> ListModel = CBOCtrl.GetQuery(SqlCommand);
                        JsonTable.Add("lstResult", ListModel);
                    }
                }
            }
            catch (Exception ex)
            {
                JsonTable.Add("error", ex.Message);
            }

            return Json(JsonTable);
        }

        #endregion

        #region Helpers

        /// <summary>
        /// return success | error message
        /// </summary>
        /// <param name="ImageStream"></param>
        /// <param name="FileName"></param>
        /// <param name="MaxWidth"></param>
        /// <param name="ThumbsSize">0: not create thumbs</param>
        /// <returns></returns>
        private string UploadAndResize(Stream ImageStream, string FileName, int MaxWidth, int ThumbsSize)
        {
            if (!Helpers.IsImage(FileName)) return "File is not an image";

            try
            {
                WebImage ObjImg = new WebImage(ImageStream);

                if (ObjImg.Width > MaxWidth)
                {
                    int NewHeight = (int)(MaxWidth / ((float)ObjImg.Width / ObjImg.Height));
                    ObjImg.Resize(MaxWidth, NewHeight, true, true);
                }

                // imageFormat: null, forceCorrectExtension: false: prevent add .png to small images
                ObjImg.Save(Helpers.GetMediaFolderPath() + FileName, imageFormat: null, forceCorrectExtension: false);


                if (ThumbsSize > 0)
                {
                    if (ThumbsSize >= ObjImg.Width)
                    {
                        // if Original image size < thumbs size: clone original image as thumbs (not crop & resize)
                        ObjImg.Save(Helpers.GetMediaFolderPath() + "thumbs-" + FileName, imageFormat: null, forceCorrectExtension: false); // clone source image as thumbs (NOT create thumbs)
                    }
                    else
                    {
                        // Create Thumbs
                        // Resize image too small must crop the border (bug of Resize method )
                        ObjImg.Resize(ThumbsSize + 1, ThumbsSize + 1, true, true).Crop(1, 1, 0, 0).Save(Helpers.GetMediaFolderPath() + "thumbs-" + FileName, imageFormat: null, forceCorrectExtension: false);
                    }
                }

                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        private string CreateThumbs(string FileName, int ThumbsSize)
        {
            string ReturnString = "success";

            if (!Helpers.IsImage(FileName)) ReturnString = "File is not an image";

            try
            {
                WebImage ObjImg = new WebImage(Helpers.GetMediaFolderPath() + FileName);
                // try to delete old thumbs if exist
                FileInfo ObjFile = new FileInfo(Helpers.GetMediaFolderPath() + "thumbs-" + FileName);
                if (ObjFile.Exists) ObjFile.Delete();
                // now create thumbs
                ObjImg.Resize(ThumbsSize + 1, ThumbsSize + 1, true, true).Crop(1, 1, 0, 0).Save(Helpers.GetMediaFolderPath() + "thumbs-" + FileName);
            }
            catch(Exception ex)
            {
                return ex.Message;
            }

            return ReturnString;
        }

        #endregion



        #region ManageCategory

        public ActionResult ManageCategory()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ManageCategory(string act, string data)
        {
            Hashtable JsonTable = new Hashtable();

            try
            {
                if (act == "load-init")
                {
                    #region Load Init

                    using (StreamReader sr = new StreamReader(Server.MapPath("~/Content/template/FormCategory.html")))
                    {
                        JsonTable.Add("tplFormCategory", sr.ReadToEnd());
                    }

                    JsonTable.Add("lstCategory", ObjCtrl.S_Category_GetListAllLevel(
                        CultureCode: CultureCode,
                        CategoryType: (CategoryType)Enum.Parse(typeof(CategoryType), data),
                        GroupName: "all",
                        CreateBy: "all",
                        ParentsID: "-1",
                        Label: "all",
                        ByDateRange: false,
                        ListCategory: null,
                        Level: 0,
                        CurrentCategoryID: "-1"
                    ));

                    #endregion
                }
                else if (act == "update-category")
                {
                    #region Update Category

                    S_CategoryModel Model = JsonConvert.DeserializeObject<S_CategoryModel>(data);

                    if (Model.CategoryID == "-1") Model.CreateBy = UserID;
                    Model.LastUpdateBy = UserID;
                    ObjCtrl.S_Category_Update(Model);

                    JsonTable.Add("lstCategory", ObjCtrl.S_Category_GetListAllLevel(
                        CultureCode: CultureCode,
                        CategoryType: (CategoryType)Enum.Parse(typeof(CategoryType), Model.CategoryType),
                        GroupName: "all",
                        CreateBy: "all",
                        ParentsID: "-1",
                        Label: "all",
                        ByDateRange: false,
                        ListCategory: null,
                        Level: 0,
                        CurrentCategoryID: "-1"
                    )); ;

                    #endregion
                }
                else if (act == "delete-category")
                {
                    #region Delete Category

                    S_CategoryModel Model = ObjCtrl.S_Category_GetByID(data, CultureCode)[0];

                    ObjCtrl.S_Category_DeleteByIDAndAllSub(data, false);

                    JsonTable.Add("lstCategory", ObjCtrl.S_Category_GetListAllLevel(
                        CultureCode: CultureCode,
                        CategoryType: (CategoryType)Enum.Parse(typeof(CategoryType), Model.CategoryType),
                        GroupName: "all",
                        CreateBy: "all",
                        ParentsID: "-1",
                        Label: "all",
                        ByDateRange: false,
                        ListCategory: null,
                        Level: 0,
                        CurrentCategoryID: "-1"
                    ));

                    #endregion
                }
            }
            catch (Exception ex)
            {
                JsonTable.Add("error", ex.Message);
            }

            return Json(JsonTable);
        }

        #endregion

        #region ManageProduct

        public ActionResult ManageProduct()
        {
            // delete temp image older than 1 day
            List<S_ContentModel> ListTempImage = ObjCtrl.S_Content_GetList(
                GroupName: "temp",
                ContentType: "all",
                ParentsID: "all",
                ParentsTable: "all",
                PageSize: 100,
                TotalRow: ref TotalRow,
                TotalPage: ref TotalPage
            );

            if (ListTempImage != null && ListTempImage.Count > 0)
            {
                foreach (S_ContentModel item in ListTempImage)
                {
                    if (item.CreateDate.AddDays(1) < DateTime.Now)
                    {
                        ObjCtrl.S_Content_DeleteByID(item.ContentID, true);
                        Helpers.DeleteMediaFile(item.PrimaryImage);
                    }
                }
            }

            return View();
        }

        [HttpPost]
        public JsonResult ManageProduct(string act, string data)
        {
            Hashtable JsonTable = new Hashtable();
            
            try
            {
                if (act == "load-init")
                {
                    #region Load Init

                    using (StreamReader sr = new StreamReader(Server.MapPath("~/Content/template/FormProduct.html")))
                    {
                        JsonTable.Add("tplFormProduct", sr.ReadToEnd());
                    }
                    using (StreamReader sr = new StreamReader(Server.MapPath("~/Content/template/FormProductExtendsImage.html")))
                    {
                        JsonTable.Add("tplFormExtendsImage", sr.ReadToEnd());
                    }
                    using (StreamReader sr = new StreamReader(Server.MapPath("~/Content/template/FormProductDetailsImage.html")))
                    {
                        JsonTable.Add("tplFormDetailsImage", sr.ReadToEnd());
                    }

                    JsonTable.Add("lstCategory", ObjCtrl.S_Category_GetListAllLevel(
                        CultureCode: CultureCode,
                        CategoryType: CategoryType.product,
                        GroupName: "all",
                        CreateBy: "all",
                        ParentsID: "-1",
                        Label: "all",
                        ByDateRange: false,
                        ListCategory: null,
                        Level: 0,
                        CurrentCategoryID: "-1"
                    ));

                    JsonTable.Add("genProductID", Helpers.GenerateID());

                    #endregion
                }
                else if (act == "load-list-product")
                {
                    #region Load List Product

                    Hashtable QueryTable = JsonConvert.DeserializeObject<Hashtable>(data);

                    string FilterType = QueryTable["type"].ToString();
                    string CategoryID = QueryTable["categoryID"].ToString();
                    int PageIndex = int.Parse(QueryTable["pageIndex"].ToString());
                    string Keyword = QueryTable["keyword"].ToString();
                    string KeywordColumn = QueryTable["keywordColumn"].ToString();
                    string ProductID = QueryTable["productID"].ToString();

                    string OrderBy = QueryTable["orderColumn"].ToString() + " " + QueryTable["orderDirection"].ToString();

                    ProductColumns OrderByColumn = (ProductColumns)Enum.Parse(typeof(ProductColumns), QueryTable["orderColumn"].ToString());
                    OrderDirection Direction = (OrderDirection)Enum.Parse(typeof(OrderDirection), QueryTable["orderDirection"].ToString());

                    List<S_ProductModel> ListReturn = new List<S_ProductModel>();

                    if (FilterType == "bycategory")
                    {
                        ListReturn = ObjCtrl.S_Product_GetList(
                            CategoryID: CategoryID,
                            PageSize: ProductPageSize,
                            PageIndex: PageIndex,
                            TotalRow: ref TotalRow,
                            TotalPage: ref TotalPage,
                            OrderByColumn: OrderByColumn,
                            Direction: Direction
                        );
                    }
                    else if (FilterType == "search")
                    {
                        ListReturn = ObjCtrl.S_Product_GetList(
                            TotalRow: ref TotalRow,
                            TotalPage: ref TotalPage,
                            PageSize: ProductPageSize,
                            PageIndex: PageIndex,
                            Keyword: Keyword,
                            OrderByColumn: OrderByColumn,
                            Direction: Direction
                        );
                    }

                    // load anonymous product: for link edit product from the FontEnd
                    if (ProductID != "-1")
                    {
                        if (ListReturn.Where(p => p.ProductID == ProductID).Count() <= 0) {
                            List<S_ProductModel> TempModel = ObjCtrl.S_Product_GetByID(ProductID, CultureCode, false);
                            if (TempModel.Count > 0)
                            {
                                ListReturn.Add(TempModel[0]);
                            }
                        }
                    }

                    JsonTable.Add("lstProduct", ListReturn);
                    JsonTable.Add("totalPage", TotalPage);
                    JsonTable.Add("totalRow", TotalRow);

                    #endregion
                }
                else if (act == "update-product")
                {
                    #region Update Product

                    S_ProductModel Model = JsonConvert.DeserializeObject<S_ProductModel>(data);

                    // case add new product
                    if (Model.CreateBy == "-1")
                    {
                        Model.CreateBy = UserID;
                        // check to update temp images if exist
                        List<S_ContentModel> ListTempImage = new List<S_ContentModel>();

                        ListTempImage.AddRange(ObjCtrl.S_Content_GetList(
                            GroupName: "temp",
                            ContentType: "product-details-img",
                            ParentsID: Model.ProductID,
                            ParentsTable: "S_Product",
                            PageSize: 100,
                            TotalRow: ref TotalRow,
                            TotalPage: ref TotalPage
                        ));

                        ListTempImage.AddRange(ObjCtrl.S_Content_GetList(
                            GroupName: "temp",
                            ContentType: "product-image",
                            ParentsID: Model.ProductID,
                            ParentsTable: "S_Product",
                            PageSize: 100,
                            TotalRow: ref TotalRow,
                            TotalPage: ref TotalPage
                        ));

                        if (ListTempImage != null && ListTempImage.Count > 0)
                        {
                            foreach (S_ContentModel item in ListTempImage)
                            {
                                item.GroupName = "global";
                                ObjCtrl.S_Content_Update(item);
                            }
                        }
                    }

                    Model.LastUpdateBy = UserID;
                    Model.Details = Model.Details.Replace(Helpers.Domain + "/", "/");
                    Model = ObjCtrl.S_Product_Update(Model)[0];

                    JsonTable.Add("newProduct", Model);
                    JsonTable.Add("genProductID", Helpers.GenerateID());

                    #endregion
                }
                else if (act == "delete-product")
                {
                    #region Delete Product

                    ObjCtrl.S_Product_DeleteByID(data, false);

                    #endregion
                }
                else if (act == "load-product-image")
                {
                    JsonTable.Add("curLstExtendsImg", ObjCtrl.S_Content_GetList(
                        ContentType: "product-image",
                        ParentsID: data,
                        ParentsTable: "S_Product",
                        PageSize: 100,
                        TotalRow: ref TotalRow,
                        TotalPage: ref TotalPage)
                    );
                }
                else if (act == "delete-product-image")
                {
                    ObjCtrl.S_Content_DeleteByID(data, true);
                }
                else if (act == "load-details-image")
                {
                    JsonTable.Add("curLstDetailsImage", ObjCtrl.S_Content_GetList(
                        ContentType: "product-details-img",
                        ParentsID: data,
                        ParentsTable: "S_Product",
                        PageSize: 100,
                        TotalRow: ref TotalRow,
                        TotalPage: ref TotalPage)
                    );
                }
                else if (act == "delete-details-image")
                {
                    ObjCtrl.S_Content_DeleteByID(data, true);
                }
                else if (act == "upload-files")
                {
                    #region Upload File

                    //Hashtable QueryTable = ObjSer.Deserialize<Hashtable>(data);
                    Hashtable QueryTable = JsonConvert.DeserializeObject<Hashtable>(data);

                    string ProductID = QueryTable["productID"].ToString();
                    string FileType = QueryTable["fileType"].ToString();
                    string GroupName = QueryTable["groupName"].ToString();

                    if (Request.Files.Count > 0)
                    {
                        #region Upload Product Primary Image

                        if (FileType == "PrimaryImage")
                        {
                            S_ProductModel ProductModel = ObjCtrl.S_Product_GetByID(ProductID, CultureCode, false)[0];

                            // delete old image if exists
                            if (ProductModel.PrimaryImage != "default.png")
                            {
                                Helpers.DeleteMediaFile(ProductModel.PrimaryImage);
                            }
                            // update database and upload new image
                            ProductModel.PrimaryImage = Helpers.GenerateFileName(Request.Files[0].FileName);
                            ObjCtrl.S_Product_Update(ProductModel);

                            UploadAndResize(Request.Files[0].InputStream, ProductModel.PrimaryImage, 1280, ThumbsSizeProduct);

                            JsonTable.Add("imgName", ProductModel.PrimaryImage);
                        }

                        #endregion

                        #region Upload Product Image (Accept Multi Files)

                        if (FileType == "ProductImage" || FileType == "DetailsImage")
                        {
                            string CurContentType = FileType == "ProductImage" ? "product-image" : "product-details-img";

                            List<S_ContentModel> ListReturn = new List<S_ContentModel>();
                            S_ContentModel ObjContent;

                            foreach (string file in Request.Files)
                            {
                                var FileContent = Request.Files[file];
                                ObjContent = new S_ContentModel();
                                ObjContent.GroupName = GroupName;
                                ObjContent.ContentType = CurContentType;
                                ObjContent.ParentsTable = "S_Product";
                                ObjContent.ParentsID = ProductID;
                                ObjContent.PrimaryImage = Helpers.GenerateFileName(FileContent.FileName);
                                ObjContent.CreateBy = UserID;
                                ObjContent.LastUpdateBy = UserID;

                                ObjContent = ObjCtrl.S_Content_Update(ObjContent)[0];

                                UploadAndResize(FileContent.InputStream, ObjContent.PrimaryImage, 1280, ThumbsSizeProduct);

                                ListReturn.Add(ObjContent);
                            }

                            JsonTable.Add("lstImage", ListReturn);
                        }

                        #endregion
                    }

                    #endregion
                }
                else if (act == "recreate-thumbs")
                {
                    #region Re-create all thumbs

                    string ReturnString = "No Product(s) found";

                    List<S_ProductModel> ListP = ObjCtrl.S_Product_GetList(
                        TotalRow: ref TotalRow,
                        TotalPage: ref TotalPage,
                        PageSize: 500
                    );

                    if (ListP != null && ListP.Count > 0)
                    {
                        ReturnString = "";
                        string ItemResult = "";
                        string ErrorString = "";
                        int SuccessCount = 0;
                        int ErrorCount = 0;

                        foreach (S_ProductModel item in ListP)
                        {
                            if (item.PrimaryImage != "default.png")
                            {
                                ItemResult = CreateThumbs(item.PrimaryImage, ThumbsSizeProduct);
                                if (ItemResult == "success")
                                {
                                    SuccessCount++;
                                }
                                else
                                {
                                    ErrorCount++;
                                    ErrorString += "- Product [" + item.Name + "]: " + ItemResult + "<br/>";
                                }
                            }
                        }

                        ReturnString += SuccessCount + " Product Thumbs was re-created. With: " + ErrorCount + " error";
                        if (ErrorCount > 0)
                        {
                            ReturnString += ": <br/>";
                            ReturnString += ErrorString;
                        }
                    }

                    JsonTable.Add("message", ReturnString);

                    #endregion
                }
                else if (act == "set-primary-image")
                {
                    #region Set Primary Image

                    Hashtable QueryTable = JsonConvert.DeserializeObject<Hashtable>(data);

                    S_ProductModel ProductModel = ObjCtrl.S_Product_GetByID(QueryTable["productID"].ToString(), CultureCode, false)[0];
                    ProductModel.PrimaryImage = QueryTable["imageName"].ToString();
                    ObjCtrl.S_Product_Update(ProductModel);

                    S_ContentModel ImageModel = ObjCtrl.S_Content_GetByID(QueryTable["imageID"].ToString(), CultureCode)[0];
                    ImageModel.PrimaryImage = QueryTable["productImage"].ToString();
                    ObjCtrl.S_Content_Update(ImageModel);

                    if (QueryTable["productImage"].ToString() == "default.png")
                    {
                        ObjCtrl.S_Content_DeleteByID(QueryTable["imageID"].ToString(), true);
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                JsonTable.Add("error", ex.Message);
            }

            return Json(JsonTable);
        }

        #endregion

        #region Manage banner

        public ActionResult ManageBanner()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ManageBanner(string act, string data)
        {
            Hashtable JsonTable = new Hashtable();

            try
            {
                if (act == "load-init")
                {
                    #region Load Init

                    JsonTable.Add("lstBanner", ObjCtrl.S_Content_GetList(
                        ContentType: "banner",
                        PageSize: 100,
                        TotalRow: ref TotalRow,
                        TotalPage: ref TotalPage)
                    );

                    #endregion
                }
                else if (act == "update-banner")
                {
                    #region Update Banner

                    //S_ContentModel Model = ObjSer.Deserialize<S_ContentModel>(data);
                    S_ContentModel Model = JsonConvert.DeserializeObject<S_ContentModel>(data);
                    List<S_ContentModel> ListReturn = new List<S_ContentModel>();
                    S_ContentModel ObjContent;

                    if (Request.Files.Count <= 0)
                    {
                        #region Case update info only

                        List<S_ContentModel> PrevModel = ObjCtrl.S_Content_GetByID(Model.ContentID, CultureCode);
                        if (PrevModel.Count > 0)
                        {
                            PrevModel[0].Priority = Model.Priority;
                            Model = ObjCtrl.S_Content_Update(PrevModel[0])[0];
                            ListReturn.Add(Model);
                        }

                        #endregion
                    }
                    else
                    {
                        #region Case Add New Banner or Update Banner with new Image

                        foreach (string file in Request.Files)
                        {
                            #region Delete Previous Images if exists

                            if (Model.ContentID != "-1")
                            {
                                ObjContent = ObjCtrl.S_Content_GetByID(Model.ContentID, CultureCode)[0];
                                if (ObjContent.PrimaryImage != "default.png")
                                {
                                    Helpers.DeleteMediaFile(ObjContent.PrimaryImage);
                                }
                            }

                            #endregion

                            var FileContent = Request.Files[file];
                            ObjContent = new S_ContentModel();
                            ObjContent.ContentID = Model.ContentID;
                            ObjContent.ContentType = "banner";
                            ObjContent.Priority = Model.Priority;
                            ObjContent.ParentsTable = "-1";
                            ObjContent.ParentsID = "-1";
                            ObjContent.PrimaryImage = Helpers.GenerateFileName(FileContent.FileName);
                            ObjContent.CreateBy = UserID;
                            ObjContent.LastUpdateBy = UserID;

                            ObjContent = ObjCtrl.S_Content_Update(ObjContent)[0];
                            UploadAndResize(FileContent.InputStream, ObjContent.PrimaryImage, 2000, 0);

                            ListReturn.Add(ObjContent);
                        }

                        #endregion
                    }

                    JsonTable.Add("lstBanner", ListReturn);

                    #endregion
                }
                else if (act == "delete-banner")
                {
                    #region Delete Banner

                    ObjCtrl.S_Content_DeleteByID(data, false);

                    #endregion
                }
            }
            catch (Exception ex)
            {
                JsonTable.Add("error", ex.Message);
            }

            return Json(JsonTable);
        }

        #endregion

        #region Manage Orders

        public ActionResult ManageOrders()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ManageOrders(string act, string data)
        {
            Hashtable JsonTable = new Hashtable();

            try
            {
                if (act == "load-init")
                {
                    #region Load Init

                    using (StreamReader sr = new StreamReader(Server.MapPath("~/Content/template/AdminOrderDetails.html")))
                    {
                        JsonTable.Add("tplOrderDetails", sr.ReadToEnd());
                    }

                    JsonTable.Add("lstStatus", ObjCtrl.S_OrderStatus_GetList(IsDelete: "False", Visible: "True"));

                    #endregion
                }
                else if (act == "get-order")
                {
                    #region Get Orders

                    DateTime StartDate = DateTime.Now;
                    DateTime EndDate = DateTime.Now;

                    Hashtable FilterTable = JsonConvert.DeserializeObject<Hashtable>(data);

                    string FilterType = FilterTable["type"].ToString();
                    OrderColumns ColumnName = (OrderColumns)Enum.Parse(typeof(OrderColumns), FilterTable["colName"].ToString());
                    string Keyword = FilterTable["keyword"].ToString();
                    int PageSize = int.Parse(FilterTable["pageSize"].ToString());
                    int PageIndex = int.Parse(FilterTable["pageIndex"].ToString());

                    if (FilterTable["startDate"] != null)
                    {
                        StartDate = DateTime.Parse(FilterTable["startDate"].ToString());
                    }

                    if (FilterTable["endDate"] != null)
                    {
                        EndDate = DateTime.Parse(FilterTable["endDate"].ToString());
                        EndDate = EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);
                    }

                    List<S_OrderModel> ListOrder = new List<S_OrderModel>();

                    if (FilterType == "date-range")
                    {
                        ListOrder = ObjCtrl.S_Order_GetList(
                            TotalRow: ref TotalRow,
                            TotalPage: ref TotalPage,
                            PageSize: PageSize,
                            PageIndex: PageIndex,
                            FromDate: StartDate,
                            ToDate: EndDate,
                            DateColumn: OrderColumns.CreateDate
                        );
                    }
                    else if (FilterType == "search")
                    {
                        ListOrder = ObjCtrl.S_Order_GetList(
                            TotalRow: ref TotalRow,
                            TotalPage: ref TotalPage,
                            PageSize: PageSize,
                            PageIndex: PageIndex,
                            FromDate: DateTime.Now,
                            ToDate: DateTime.Now,
                            Keyword: Keyword,
                            KeywordColumn: ColumnName
                        );
                    }

                    JsonTable.Add("lstOrder", ListOrder);
                    JsonTable.Add("totalPage", TotalPage);
                    JsonTable.Add("totalRow", TotalRow);

                    #endregion
                }
                else if (act == "update-status")
                {
                    #region Update Status

                    Hashtable QueryTable = JsonConvert.DeserializeObject<Hashtable>(data);
                    S_OrderModel Model = ObjCtrl.S_Order_GetByID(QueryTable["OrderID"].ToString(), false);
                    Model.StatusID = QueryTable["StatusID"].ToString();

                    Model = ObjCtrl.S_Order_Update(Model);

                    JsonTable.Add("newObj", Model);

                    #endregion
                }
                else if (act == "delete-order")
                {
                    ObjCtrl.S_Order_DeleteByID(data);
                }
            }
            catch (Exception ex)
            {
                JsonTable.Add("error", ex.Message);
            }

            return Json(JsonTable);
        }

        #endregion

        #region SiteSettings

        public ActionResult SiteSettings()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SiteSettings(string act, string data)
        {
            Hashtable JsonTable = new Hashtable();

            try
            {
                if (act == "load-init")
                {
                    #region Load Init

                    JsonTable.Add("siteSettings", Helpers.SiteSettings);
                    JsonTable.Add("isHost", User.IsInRole("Host"));

                    #endregion
                }
                else if (act == "save-site-settings")
                {
                    #region Save Site Settings

                    S_SiteSettingsModel Model = JsonConvert.DeserializeObject<S_SiteSettingsModel>(data);
                    S_SettingsModel SettingModel;

                    PropertyInfo[] ListProperties = Model.GetType().GetProperties();
                    if (ListProperties.Length > 0)
                    {
                        foreach (PropertyInfo item in ListProperties)
                        {
                            SettingModel = ObjCtrl.S_Settings_GetByID(item.Name, "site");
                            if (SettingModel == null)
                            {
                                SettingModel = new S_SettingsModel();
                            }
                            SettingModel.SType = "site";
                            SettingModel.SKey = item.Name;
                            SettingModel.SValue = Model.GetType().GetProperty(item.Name).GetValue(Model).ToString();
                            ObjCtrl.S_Settings_Update(SettingModel);
                        }
                    }

                    #endregion
                }
                else if (act == "test-smtp")
                {
                    #region Test SMTP

                    S_SiteSettingsModel TempModel = JsonConvert.DeserializeObject<S_SiteSettingsModel>(data);

                    var message = new MailMessage();
                    //message.To.Add(new MailAddress(MailTo));
                    message.To.Add(TempModel.SMTPUsername);
                    message.From = new MailAddress("test-smtp@default.com");
                    //message.ReplyTo = new MailAddress(ReplyTo);
                    message.Subject = "Test SMTP settings from " + Helpers.Domain.Replace("http://", "");
                    message.Body = "Hello, your SMTP settings OK";
                    message.IsBodyHtml = true;

                    using (var smtp = new SmtpClient())
                    {
                        var credential = new NetworkCredential
                        {
                            UserName = TempModel.SMTPUsername, //"ndt.no.reply@gmail.com",
                            Password = TempModel.SMTPPassword //"ndt$2012"
                        };
                        smtp.Credentials = credential;
                        smtp.Host = TempModel.SMTPHost; //"smtp.gmail.com";
                        smtp.Port = int.Parse(TempModel.SMTPPort);
                        smtp.EnableSsl = TempModel.SMTPEnableSSL;
                        //await smtp.SendMailAsync(message);
                        smtp.Send(message);
                    }

                    #endregion
                }
                else if(act == "upload-files")
                {
                    #region Upload File

                    //Hashtable QueryTable = ObjSer.Deserialize<Hashtable>(data);
                    Hashtable QueryTable = JsonConvert.DeserializeObject<Hashtable>(data);
                    string FileType = QueryTable["fileType"].ToString();

                    if (Request.Files.Count > 0)
                    {
                        #region Upload Product Primary Image

                        if (FileType == "Logo")
                        {
                            S_SettingsModel Model = ObjCtrl.S_Settings_GetByID("Logo", "site");
                            if(Model == null)
                            {
                                Model = new S_SettingsModel();
                                Model.SKey = "Logo";
                                Model.SType = "site";
                            }

                            // delete old logo if exists
                            if (Model.SValue != "default.png")
                            {
                                Helpers.DeleteMediaFile(Model.SValue);
                            }
                            // update database and upload new image
                            Model.SValue = Helpers.GenerateFileName(Request.Files[0].FileName);
                            ObjCtrl.S_Settings_Update(Model);
                            Request.Files[0].SaveAs(Helpers.GetMediaFolderPath() + Model.SValue);

                            JsonTable.Add("newLogo", Model.SValue);
                        }

                        #endregion
                    }

                    #endregion
                }
            }
            catch(Exception ex)
            {
                JsonTable.Add("error", ex.Message);
            }

            return Json(JsonTable);
        }

        #endregion



        #region Manage Content

        public ActionResult ManageContent()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ManageContent(string act, string data)
        {
            Hashtable JsonTable = new Hashtable();
            try
            {
                if (act == "load-init")
                {
                    #region Load Init

                    if (data == ContentTypesConst.Service || data == ContentTypesConst.News)
                    {
                        using (StreamReader sr = new StreamReader(Server.MapPath("~/Content/template/FormNews.html")))
                        {
                            JsonTable.Add("tplFormContent", sr.ReadToEnd());
                        }
                    }
                    else if (data == ContentTypesConst.Project)
                    {
                        using (StreamReader sr = new StreamReader(Server.MapPath("~/Content/template/FormProject.html")))
                        {
                            JsonTable.Add("tplFormContent", sr.ReadToEnd());
                        }
                    }

                    using (StreamReader sr = new StreamReader(Server.MapPath("~/Content/template/FormContentImage.html")))
                    {
                        JsonTable.Add("tplFormImage", sr.ReadToEnd());
                    }
                    using (StreamReader sr = new StreamReader(Server.MapPath("~/Content/template/FormContentDetailsImage.html")))
                    {
                        JsonTable.Add("tplFormDetailsImage", sr.ReadToEnd());
                    }

                    JsonTable.Add("lstCategory", ObjCtrl.S_Category_GetListAllLevel(
                        CultureCode: CultureCode,
                        CategoryType: (CategoryType)Enum.Parse(typeof(CategoryType), data),
                        GroupName: "all",
                        CreateBy: "all",
                        ParentsID: "-1",
                        Label: "all",
                        ByDateRange: false,
                        ListCategory: null,
                        Level: 0,
                        CurrentCategoryID: "-1"
                    ));

                    JsonTable.Add("genContentID", Helpers.GenerateID());

                    #endregion
                }
                else if (act == "load-list-content")
                {
                    #region Load List Content

                    Hashtable QueryTable = JsonConvert.DeserializeObject<Hashtable>(data);

                    string ContentType = QueryTable["type"].ToString();
                    string FilterType = QueryTable["filterType"].ToString();
                    string CategoryID = QueryTable["categoryID"].ToString();
                    int PageSize = int.Parse(QueryTable["pageSize"].ToString());
                    int PageIndex = int.Parse(QueryTable["pageIndex"].ToString());
                    string Keyword = QueryTable["keyword"].ToString();
                    string KeywordColumn = QueryTable["keywordColumn"].ToString();
                    string ContentID = QueryTable["contentID"].ToString();

                    string OrderBy = QueryTable["orderColumn"].ToString() + " " + QueryTable["orderDirection"].ToString();

                    ContentColumns OrderByColumn = (ContentColumns)Enum.Parse(typeof(ContentColumns), QueryTable["orderColumn"].ToString());
                    OrderDirection Direction = (OrderDirection)Enum.Parse(typeof(OrderDirection), QueryTable["orderDirection"].ToString());

                    List<S_ContentModel> ListReturn = new List<S_ContentModel>();

                    if (FilterType == "bycategory")
                    {
                        //JsonTable.Add("lstContent", ObjCtrl.S_Content_GetList("full", CultureCode, CategoryID, "-1", "all", "all", "all", ContentType, "-1", "-1", ProductPageSize, PageIndex, "True", false, "", "", "UpdateDate desc", ref TotalRow, ref TotalPage, false));
                        ListReturn = ObjCtrl.S_Content_GetList(
                            TotalRow: ref TotalRow,
                            TotalPage: ref TotalPage,
                            CategoryID: CategoryID,
                            CultureCode: CultureCode,
                            ContentType: ContentType,
                            PageSize: PageSize,
                            PageIndex: PageIndex,
                            OrderByColumn: OrderByColumn,
                            Direction: Direction
                        );
                    }
                    else if (FilterType == "search")
                    {
                        ListReturn = ObjCtrl.S_Content_GetList(
                            TotalRow: ref TotalRow,
                            TotalPage: ref TotalPage,
                            CultureCode: CultureCode,
                            ContentType: ContentType,
                            PageSize: PageSize,
                            PageIndex: PageIndex,
                            Keyword: Keyword,
                            OrderByColumn: OrderByColumn,
                            Direction: Direction
                        );
                    }

                    // load anonymous content: for link edit content from the FontEnd
                    if (ContentID != "-1")
                    {
                        if (ListReturn.Where(c => c.ContentID == ContentID).Count() <= 0)
                        {
                            List<S_ContentModel> TempModel = ObjCtrl.S_Content_GetByID(ContentID, CultureCode);
                            if (TempModel.Count > 0)
                            {
                                ListReturn.Add(TempModel[0]);
                            }
                        }
                    }


                    JsonTable.Add("lstContent", ListReturn);
                    JsonTable.Add("totalPage", TotalPage);
                    JsonTable.Add("totalRow", TotalRow);

                    #endregion
                }
                else if (act == "update-content")
                {
                    #region Update Content

                    // update content
                    //S_ContentModel Model = ObjSer.Deserialize<S_ContentModel>(data);
                    S_ContentModel Model = JsonConvert.DeserializeObject<S_ContentModel>(data);

                    // case add new content
                    if (Model.CreateBy == "-1")
                    {
                        Model.CreateBy = UserID;
                        // check to update temp images if exist
                        //List<S_ContentModel> ListTemp = ObjCtrl.S_Content_GetList("full", "all", "all", "-1", "temp", "all", "all", "content-details-img", Model.ContentID, "S_Content", 100, 1, "True", false, "", "", "Priority desc", ref TotalRow, ref TotalPage, false);
                        List<S_ContentModel> ListTemp = new List<S_ContentModel>();

                        ListTemp.AddRange(ObjCtrl.S_Content_GetList(
                            TotalRow: ref TotalRow,
                            TotalPage: ref TotalPage,
                            ContentType: "content-details-img",
                            ParentsID: Model.ContentID,
                            ParentsTable: "S_Content",
                            PageSize: 100
                        ));
                        ListTemp.AddRange(ObjCtrl.S_Content_GetList(
                            TotalRow: ref TotalRow,
                            TotalPage: ref TotalPage,
                            ContentType: "content-image",
                            ParentsID: Model.ContentID,
                            ParentsTable: "S_Content",
                            PageSize: 100
                        ));

                        if (ListTemp != null && ListTemp.Count > 0)
                        {
                            foreach (S_ContentModel item in ListTemp)
                            {
                                item.GroupName = "global";
                                ObjCtrl.S_Content_Update(item);
                            }
                        }
                    }

                    Model.LastUpdateBy = UserID;
                    Model.Details = Model.Details.Replace(Helpers.Domain + "/", "/");
                    Model = ObjCtrl.S_Content_Update(Model)[0];

                    JsonTable.Add("newContent", Model);
                    JsonTable.Add("genContentID", Helpers.GenerateID());

                    #endregion
                }
                else if (act == "delete-content")
                {
                    ObjCtrl.S_Content_DeleteByID(data, false);
                }

                else if (act == "load-content-image")
                {
                    #region Content Images

                    JsonTable.Add("curLstImg", ObjCtrl.S_Content_GetList(
                        ContentType: "content-image",
                        ParentsID: data,
                        ParentsTable: "S_Content",
                        PageSize: 100,
                        TotalRow: ref TotalRow,
                        TotalPage: ref TotalPage)
                    );

                    #endregion
                }
                else if (act == "delete-content-image")
                {
                    ObjCtrl.S_Content_DeleteByID(data, true);
                }

                else if (act == "load-content-details-image")
                {
                    //JsonTable.Add("curLstImg", ObjCtrl.S_Content_GetList("full", "all", "all", "-1", "all", "all", "all", "content-details-img", data, "S_Content", 50, 1, "True", false, "", "", "Priority desc", ref TotalRow, ref TotalPage, false));
                    JsonTable.Add("curLstImg", ObjCtrl.S_Content_GetList(
                        TotalRow: ref TotalRow, 
                        TotalPage: ref TotalPage, 
                        ContentType: "content-details-img", 
                        ParentsID: data, 
                        ParentsTable: "S_Content", 
                        PageSize: 50)
                    );
                }
                else if (act == "delete-content-details-image")
                {
                    ObjCtrl.S_Content_DeleteByID(data, true);
                }
                else if (act == "upload-files")
                {
                    #region Upload File

                    Hashtable QueryTable = JsonConvert.DeserializeObject<Hashtable>(data);
                    string ContentID = QueryTable["contentID"].ToString();
                    string FileType = QueryTable["fileType"].ToString();
                    string GroupName = QueryTable["groupName"].ToString();
                    int ThumbsSize = int.Parse(QueryTable["thumbsSize"].ToString());

                    if (Request.Files.Count > 0)
                    {
                        #region Upload Content Primary Image

                        if (FileType == "PrimaryImage")
                        {
                            S_ContentModel ContentModel = ObjCtrl.S_Content_GetByID(ContentID, CultureCode)[0];

                            // delete old image if exists
                            if (ContentModel.PrimaryImage != "default.png")
                            {
                                Helpers.DeleteMediaFile(ContentModel.PrimaryImage);
                            }
                            // update database and upload new image
                            ContentModel.PrimaryImage = Helpers.GenerateFileName(Request.Files[0].FileName);
                            ObjCtrl.S_Content_Update(ContentModel);
                            UploadAndResize(Request.Files[0].InputStream, ContentModel.PrimaryImage, 1280, ThumbsSize);

                            JsonTable.Add("imgName", ContentModel.PrimaryImage);
                        }

                        #endregion

                        #region Upload ContentImage & ContentDetailsImage (Accept Multi Files)

                        if (FileType == "ContentDetailsImage" || FileType == "ContentImage")
                        {
                            string ConType = FileType == "ContentDetailsImage" ? "content-details-img" : "content-image";

                            List<S_ContentModel> ListReturn = new List<S_ContentModel>();
                            S_ContentModel ObjContent;

                            foreach (string file in Request.Files)
                            {
                                var FileContent = Request.Files[file];
                                ObjContent = new S_ContentModel();
                                ObjContent.GroupName = GroupName;
                                ObjContent.ContentType = ConType;
                                ObjContent.ParentsTable = "S_Content";
                                ObjContent.ParentsID = ContentID;
                                ObjContent.PrimaryImage = Helpers.GenerateFileName(FileContent.FileName);
                                ObjContent.CreateBy = UserID;
                                ObjContent.LastUpdateBy = UserID;

                                ObjContent = ObjCtrl.S_Content_Update(ObjContent)[0];
                                UploadAndResize(FileContent.InputStream, ObjContent.PrimaryImage, 1280, ThumbsSize);

                                ListReturn.Add(ObjContent);
                            }

                            JsonTable.Add("lstImage", ListReturn);
                        }

                        #endregion
                    }

                    #endregion
                }
                else if (act == "recreate-thumbs")
                {
                    #region Re-create all thumbs

                    Hashtable QueryTable = JsonConvert.DeserializeObject<Hashtable>(data);

                    string ContentType = QueryTable["type"].ToString();
                    int ThumbsSize = int.Parse(QueryTable["thumbsSize"].ToString());
                    string ReturnString = "No Item(s) found";
                    
                    List<S_ContentModel> ListC = ObjCtrl.S_Content_GetList(
                        TotalRow: ref TotalRow,
                        TotalPage: ref TotalPage,
                        PageSize: 500,
                        ContentType: ContentType,
                        ParentsID: "all",
                        ParentsTable: "all"
                    );

                    if (ListC != null && ListC.Count > 0)
                    {
                        ReturnString = "";
                        string ItemResult = "";
                        string ErrorString = "";
                        int SuccessCount = 0;
                        int ErrorCount = 0;

                        foreach (S_ContentModel item in ListC)
                        {
                            if (item.PrimaryImage != "default.png")
                            {
                                ItemResult = CreateThumbs(item.PrimaryImage, ThumbsSize);
                                if (ItemResult == "success")
                                {
                                    SuccessCount++;
                                }
                                else
                                {
                                    ErrorCount++;
                                    ErrorString += "- Item [" + item.Name + "]: " + ItemResult + "<br/>";
                                }
                            }
                        }

                        ReturnString += SuccessCount + " Item Thumbs was re-created. With: " + ErrorCount + " error";
                        if (ErrorCount > 0)
                        {
                            ReturnString += ": <br/>";
                            ReturnString += ErrorString;
                        }
                    }

                    JsonTable.Add("message", ReturnString);

                    #endregion
                }
                else if (act == "set-primary-image")
                {
                    #region Set Primary Image

                    Hashtable QueryTable = JsonConvert.DeserializeObject<Hashtable>(data);

                    S_ContentModel ContentModel = ObjCtrl.S_Content_GetByID(QueryTable["contentID"].ToString(), CultureCode)[0];
                    ContentModel.PrimaryImage = QueryTable["imageName"].ToString();
                    ObjCtrl.S_Content_Update(ContentModel);


                    S_ContentModel ImageModel = ObjCtrl.S_Content_GetByID(QueryTable["imageID"].ToString(), CultureCode)[0];
                    ImageModel.PrimaryImage = QueryTable["contentImage"].ToString();
                    ObjCtrl.S_Content_Update(ImageModel);

                    if (QueryTable["contentImage"].ToString() == "default.png")
                    {
                        ObjCtrl.S_Content_DeleteByID(QueryTable["imageID"].ToString(), true);
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                JsonTable.Add("error", ex.Message);
            }

            return Json(JsonTable);
        }

        #endregion

        #region Multi Content Simple

        public ActionResult MultiContentSimple()
        {
            return View();
        }

        [HttpPost]
        public JsonResult MultiContentSimple(string act, string data)
        {
            Hashtable JsonTable = new Hashtable();
            try
            {
                if (act == "load-init")
                {
                    #region Load Init


                    #endregion
                }
                else if (act == "load-list")
                {
                    #region Load List Content

                    Hashtable QueryTable = JsonConvert.DeserializeObject<Hashtable>(data);
                    string ContentType = QueryTable["ContentType"].ToString();
                    int PageIndex = int.Parse(QueryTable["PageIndex"].ToString());
                    int PageSize = int.Parse(QueryTable["PageSize"].ToString());

                    ContentColumns OrderByColumn = (ContentColumns)Enum.Parse(typeof(ContentColumns), QueryTable["OrderColumn"].ToString());
                    OrderDirection Direction = (OrderDirection)Enum.Parse(typeof(OrderDirection), QueryTable["OrderDirection"].ToString());

                    JsonTable.Add("lstContent", ObjCtrl.S_Content_GetList(
                        ContentType: ContentType,
                        PageSize: PageSize,
                        PageIndex: PageIndex,
                        TotalRow: ref TotalRow,
                        TotalPage: ref TotalPage,
                        OrderByColumn: OrderByColumn,
                        Direction: Direction
                    ));

                    JsonTable.Add("totalPage", TotalPage);
                    JsonTable.Add("totalRow", TotalRow);

                    #endregion
                }
                else if (act == "update-content")
                {
                    #region Update Content

                    S_ContentModel Model = JsonConvert.DeserializeObject<S_ContentModel>(data);

                    // case add new content
                    if (Model.CreateBy == "-1")
                    {
                        Model.CreateBy = UserID;
                    }

                    Model.LastUpdateBy = UserID;

                    Model = ObjCtrl.S_Content_Update(Model)[0];

                    JsonTable.Add("newContent", Model);

                    #endregion
                }
                else if (act == "delete-content")
                {
                    ObjCtrl.S_Content_DeleteByID(data, false);
                }
                else if (act == "upload-files")
                {
                    #region Upload File

                    Hashtable QueryTable = JsonConvert.DeserializeObject<Hashtable>(data);
                    string ContentID = QueryTable["ContentID"].ToString();
                    string FileType = QueryTable["FileType"].ToString();
                    int ThumbsSize = int.Parse(QueryTable["ThumbsSize"].ToString());

                    if (Request.Files.Count > 0)
                    {
                        #region Upload Content Primary Image

                        if (FileType == "PrimaryImage")
                        {
                            S_ContentModel Model = ObjCtrl.S_Content_GetByID(ContentID, CultureCode)[0];

                            // delete old image if exists
                            if (Model.PrimaryImage != "default.png")
                            {
                                Helpers.DeleteMediaFile(Model.PrimaryImage);
                            }
                            // update database and upload new image
                            Model.PrimaryImage = Helpers.GenerateFileName(Request.Files[0].FileName);
                            Model = ObjCtrl.S_Content_Update(Model)[0];
                            UploadAndResize(Request.Files[0].InputStream, Model.PrimaryImage, 1280, ThumbsSize);

                            JsonTable.Add("newContent", Model);
                        }

                        #endregion
                    }

                    #endregion
                }
                else if (act == "recreate-thumbs")
                {
                    #region Re-create all thumbs

                    Hashtable QueryTable = JsonConvert.DeserializeObject<Hashtable>(data);

                    string ContentType = QueryTable["type"].ToString();
                    int ThumbsSize = int.Parse(QueryTable["thumbsSize"].ToString());
                    string ReturnString = "No Item(s) found";

                    List<S_ContentModel> ListC = ObjCtrl.S_Content_GetList(
                        TotalRow: ref TotalRow,
                        TotalPage: ref TotalPage,
                        PageSize: 500,
                        ContentType: ContentType,
                        ParentsID: "all",
                        ParentsTable: "all"
                    );

                    if (ListC != null && ListC.Count > 0)
                    {
                        ReturnString = "";
                        string ItemResult = "";
                        string ErrorString = "";
                        int SuccessCount = 0;
                        int ErrorCount = 0;

                        foreach (S_ContentModel item in ListC)
                        {
                            if (item.PrimaryImage != "default.png")
                            {
                                ItemResult = CreateThumbs(item.PrimaryImage, ThumbsSize);
                                if (ItemResult == "success")
                                {
                                    SuccessCount++;
                                }
                                else
                                {
                                    ErrorCount++;
                                    ErrorString += "- Item [" + item.Name + "]: " + ItemResult + "<br/>";
                                }
                            }
                        }

                        ReturnString += SuccessCount + " Item Thumbs was re-created. With: " + ErrorCount + " error";
                        if (ErrorCount > 0)
                        {
                            ReturnString += ": <br/>";
                            ReturnString += ErrorString;
                        }
                    }

                    JsonTable.Add("message", ReturnString);

                    #endregion
                }
            }
            catch (Exception ex)
            {
                JsonTable.Add("error", ex.Message);
            }

            return Json(JsonTable);
        }

        #endregion

        #region Single Content

        public ActionResult SingleContent()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SingleContent(string act, string data)
        {
            Hashtable JsonTable = new Hashtable();
            try
            {
                if (act == "load-init")
                {
                    #region Load Init

                    using (StreamReader sr = new StreamReader(Server.MapPath("~/Content/template/FormSingleContentDetailsImage.html")))
                    {
                        JsonTable.Add("tplFormDetailsImage", sr.ReadToEnd());
                    }

                    S_ContentModel Model = new S_ContentModel();
                    List<S_ContentModel> ListContent = ObjCtrl.S_Content_GetList(
                        ContentType: data,
                        PageSize: 1,
                        TotalRow: ref TotalRow,
                        TotalPage: ref TotalPage
                    );

                    if (ListContent != null && ListContent.Count > 0) Model = ListContent[0];

                    JsonTable.Add("curContent", Model);

                    #endregion
                }
                else if (act == "update-content")
                {
                    #region Update Content

                    S_ContentModel Model = JsonConvert.DeserializeObject<S_ContentModel>(data);

                    // case add new content
                    if (Model.CreateBy == "-1")
                    {
                        Model.CreateBy = UserID;
                    }

                    Model.LastUpdateBy = UserID;
                    Model.Details = Model.Details.Replace(Helpers.Domain + "/", "/");
                    Model = ObjCtrl.S_Content_Update(Model)[0];

                    JsonTable.Add("newContent", Model);

                    #endregion
                }
                else if (act == "load-extends-image")
                {
                    #region Extends Images

                    JsonTable.Add("lstExtendsImage", ObjCtrl.S_Content_GetList(
                        ContentType: "content-image",
                        ParentsID: data,
                        ParentsTable: "S_Content",
                        PageSize: 100,
                        TotalRow: ref TotalRow,
                        TotalPage: ref TotalPage)
                    );

                    #endregion
                }
                else if (act == "delete-extends-image")
                {
                    ObjCtrl.S_Content_DeleteByID(data, true);
                }

                else if (act == "load-content-details-image")
                {
                    #region Load Content Details Images

                    JsonTable.Add("lstDetailsImage", ObjCtrl.S_Content_GetList(
                        ContentType: "content-details-img",
                        ParentsID: data,
                        ParentsTable: "S_Content",
                        PageSize: 100,
                        TotalRow: ref TotalRow,
                        TotalPage: ref TotalPage)
                    );

                    #endregion
                }
                else if (act == "delete-content-details-image")
                {
                    ObjCtrl.S_Content_DeleteByID(data, true);
                }
                else if (act == "upload-files")
                {
                    #region Upload File

                    Hashtable QueryTable = JsonConvert.DeserializeObject<Hashtable>(data);
                    string ContentID = QueryTable["contentID"].ToString();
                    string FileType = QueryTable["fileType"].ToString();
                    int ThumbsSize = int.Parse(QueryTable["thumbsSize"].ToString());

                    if (Request.Files.Count > 0)
                    {
                        #region Upload Content Primary Image

                        if (FileType == "PrimaryImage")
                        {
                            S_ContentModel ContentModel = ObjCtrl.S_Content_GetByID(ContentID, CultureCode)[0];

                            // delete old image if exists
                            if (ContentModel.PrimaryImage != "default.png")
                            {
                                Helpers.DeleteMediaFile(ContentModel.PrimaryImage);
                            }
                            // update database and upload new image
                            ContentModel.PrimaryImage = Helpers.GenerateFileName(Request.Files[0].FileName);
                            ObjCtrl.S_Content_Update(ContentModel);
                            UploadAndResize(Request.Files[0].InputStream, ContentModel.PrimaryImage, 1280, ThumbsSize);

                            JsonTable.Add("imgName", ContentModel.PrimaryImage);
                        }

                        #endregion

                        #region Upload ContentImage & ContentDetailsImage (Accept Multi Files)

                        if (FileType == "ContentDetailsImage" || FileType == "ContentImage")
                        {
                            string ConType = FileType == "ContentDetailsImage" ? "content-details-img" : "content-image";

                            List<S_ContentModel> ListReturn = new List<S_ContentModel>();
                            S_ContentModel ObjContent;

                            foreach (string file in Request.Files)
                            {
                                var FileContent = Request.Files[file];
                                ObjContent = new S_ContentModel();
                                ObjContent.ContentType = ConType;
                                ObjContent.ParentsTable = "S_Content";
                                ObjContent.ParentsID = ContentID;
                                ObjContent.PrimaryImage = Helpers.GenerateFileName(FileContent.FileName);
                                ObjContent.CreateBy = UserID;
                                ObjContent.LastUpdateBy = UserID;

                                ObjContent = ObjCtrl.S_Content_Update(ObjContent)[0];
                                UploadAndResize(FileContent.InputStream, ObjContent.PrimaryImage, 1280, ThumbsSize);

                                ListReturn.Add(ObjContent);
                            }

                            JsonTable.Add("lstImage", ListReturn);
                        }

                        #endregion
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                JsonTable.Add("error", ex.Message);
            }

            return Json(JsonTable);
        }

        #endregion

        #region Advanced Settings

        public ActionResult AdvancedSettings()
        {
            return View();
        }

        [HttpPost]
        public JsonResult AdvancedSettings(string act, string data)
        {
            Hashtable JsonTable = new Hashtable();

            try
            {
                if (act == "load-init")
                {
                    #region Load Init

                    List<S_OrderStatusModel> ListOrderStatus = ObjCtrl.S_OrderStatus_GetList(IsDelete: "False", Visible: "True");
                    JsonTable.Add("lstOrderStatus", ListOrderStatus);
                    JsonTable.Add("isHost", User.IsInRole("Host"));

                    #endregion
                }
                else if (act == "update-order-status")
                {
                    #region Update Order Status

                    S_OrderStatusModel Model = JsonConvert.DeserializeObject<S_OrderStatusModel>(data);
                    Model = ObjCtrl.S_OrderStatus_Update(Model);

                    JsonTable.Add("newObj", Model);

                    #endregion
                }
                else if (act == "delete-order-status")
                {
                    ObjCtrl.S_OrderStatus_DeleteByID(data);
                }
            }
            catch(Exception ex)
            {
                JsonTable.Add("error", ex.Message);
            }

            return Json(JsonTable);
        }

        #endregion


        #region Log  

        public ActionResult ManageLog()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ManageLog(string act, string data)
        {
            Hashtable JsonTable = new Hashtable();

            try
            {
                if (act == "load-init")
                {
                    #region Load Init

                    using (StreamReader sr = new StreamReader(Server.MapPath("~/Content/template/FormLogDetail.html")))
                    {
                        JsonTable.Add("tplLogDetail", sr.ReadToEnd());
                    }

                    #endregion
                }
                else if (act == "get-list")
                {
                    #region Get List

                    Hashtable QueryTable = JsonConvert.DeserializeObject<Hashtable>(data);
                    string SType = QueryTable["Type"].ToString();
                    S_LogType LogType = (S_LogType)int.Parse(QueryTable["LogType"] != null ? QueryTable["LogType"].ToString() : "0");
                    int PageIndex = int.Parse(QueryTable["PageIndex"].ToString());
                    int PageSize = int.Parse(QueryTable["PageSize"].ToString());

                    string OrderBy = "CreateDate desc";

                    if (QueryTable["OrderColumn"] != null && QueryTable["OrderDirection"] != null)
                    {
                        OrderBy = QueryTable["OrderColumn"].ToString().Trim() + " " + QueryTable["OrderDirection"].ToString().Trim();
                    }

                    if (SType == "date-range")
                    {
                        DateTime FromDate = DateTime.Parse(QueryTable["FromDate"].ToString());
                        DateTime ToDate = DateTime.Parse(QueryTable["ToDate"].ToString()).AddDays(1).AddMinutes(-1);

                        JsonTable.Add("lstLog", ObjCtrl.S_Log_GetList(
                            LogType: LogType,
                            TotalPage: ref TotalPage,
                            TotalRow: ref TotalRow,
                            DateColumn: "CreateDate",
                            FromDate: FromDate,
                            ToDate: ToDate,
                            PageSize: PageSize,
                            PageIndex: PageIndex
                        ));
                    }

                    JsonTable.Add("totalRow", TotalRow);
                    JsonTable.Add("totalPage", TotalPage);

                    #endregion
                }
            }
            catch (Exception ex)
            {
                JsonTable.Add("error", ex.Message);
            }

            return Json(JsonTable);
        }

        #endregion
    }
}
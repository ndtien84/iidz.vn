﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using shop.com.Models;

using shop.com.Utils;
using System.Linq;

namespace shop.com.Controllers
{
    public class HomeController : SBaseController
    {
        private int TotalPage = 0;
        private int TotalRow = 0;
        private int ProductPageSize = 6;

        #region Index  

        public ActionResult Index()
        {
            #region Get Project

            ViewBag.ListProject = ObjCtrl.S_Content_GetList(
                TotalRow: ref TotalRow,
                TotalPage: ref TotalPage,
                ContentType: ContentTypesConst.Project,
                CultureCode: CultureCode,
                PageSize: 8,
                OrderByColumn: ContentColumns.UpdateDate
            );

            #endregion

            #region Get Service

            ViewBag.ListService = ObjCtrl.S_Content_GetList(
                TotalRow: ref TotalRow,
                TotalPage: ref TotalPage,
                ContentType: ContentTypesConst.Service,
                CultureCode: CultureCode,
                PageSize: 3,
                OrderByColumn: ContentColumns.UpdateDate
            );

            #endregion

            #region Get Banner

            ViewBag.ListBanner = ObjCtrl.S_Content_GetList(
                ContentType: "banner",
                TotalRow: ref TotalRow,
                TotalPage: ref TotalPage
            );

            #endregion

            #region Get Team

            ViewBag.ListTeam = ObjCtrl.S_Content_GetList(
                TotalRow: ref TotalRow,
                TotalPage: ref TotalPage,
                ContentType: ContentTypesConst.Team,
                CultureCode: CultureCode,
                PageSize: 5
            );

            #endregion

            return View();
        }

        #endregion

        #region Gioi Thieu

        public ActionResult GioiThieu()
        {
            List<S_ContentModel> ListAbout = ObjCtrl.S_Content_GetList(
                ContentType: "about-us",
                TotalRow: ref TotalRow,
                TotalPage: ref TotalPage
            );

            if (ListAbout != null && ListAbout.Count > 0) ViewBag.ObjAbout = ListAbout[0];

            return View();
        }

        #endregion

        #region Dich Vu

        public ActionResult DichVu()
        {
            //dich-vu/{id}/{urlkey}

            string CurType = "list";
            string ContentID = "-1";
            int PageIndex = 1;
            int PageSize = 10;

            if (RouteData.Values["id"] != null && RouteData.Values["urlkey"] != null)
            {
                ContentID = RouteData.Values["id"].ToString();
                CurType = "details";
            }
            else
            {
                if (RouteData.Values["id"] != null) PageIndex = int.Parse(RouteData.Values["id"].ToString());
            }

            if (CurType == "list")
            {
                ViewBag.ListContent = ObjCtrl.S_Content_GetList(
                    TotalRow: ref TotalRow,
                    TotalPage: ref TotalPage,
                    ContentType: ContentTypesConst.Service,
                    CultureCode: CultureCode,
                    PageSize: PageSize,
                    PageIndex: PageIndex,
                    OrderByColumn: ContentColumns.UpdateDate
                );
            }
            else if (CurType == "details")
            {
                ViewBag.ObjContent = ObjCtrl.S_Content_GetByID(ContentID, CultureCode)[0];
                ViewBag.ListContent = ObjCtrl.S_Content_GetList(
                    TotalRow: ref TotalRow,
                    TotalPage: ref TotalPage,
                    ContentType: ContentTypesConst.Service,
                    CultureCode: CultureCode,
                    PageSize: 5,
                    ExceptContentID: ContentID,
                    OrderByColumn: ContentColumns.UpdateDate
                );

                // update view count
                if (Request.Cookies["cvc_" + ContentID] == null)
                {
                    ObjCtrl.S_Content_UpdateCount(ContentID, "view");

                    HttpCookie Cookie = new HttpCookie("cvc_" + ContentID, "true");
                    Cookie.Expires = DateTime.Now.AddMinutes(10);
                    Response.Cookies.Add(Cookie);
                }
            }

            ViewBag.PageIndex = PageIndex;
            ViewBag.CurType = CurType;
            ViewBag.TotalPage = TotalPage;

            return View();
        }

        #endregion

        #region Du An

        public ActionResult DuAn()
        {
            //du-an/{id}/{urlkey}

            string CurType = "list";
            string ContentID = "-1";
            int PageIndex = 1;
            int PageSize = 8;

            if (RouteData.Values["id"] != null && RouteData.Values["urlkey"] != null)
            {
                ContentID = RouteData.Values["id"].ToString();
                CurType = "details";
            }
            else
            {
                if (RouteData.Values["id"] != null) PageIndex = int.Parse(RouteData.Values["id"].ToString());
            }

            if (CurType == "list")
            {
                ViewBag.ListContent = ObjCtrl.S_Content_GetList(
                    TotalRow: ref TotalRow,
                    TotalPage: ref TotalPage,
                    ContentType: ContentTypesConst.Project,
                    CultureCode: CultureCode,
                    PageSize: PageSize,
                    PageIndex: PageIndex,
                    OrderByColumn: ContentColumns.UpdateDate
                );
            }
            else if (CurType == "details")
            {
                ViewBag.ObjContent = ObjCtrl.S_Content_GetByID(ContentID, CultureCode)[0];
                ViewBag.ListContent = ObjCtrl.S_Content_GetList(
                    TotalRow: ref TotalRow,
                    TotalPage: ref TotalPage,
                    ContentType: ContentTypesConst.Project,
                    CultureCode: CultureCode,
                    PageSize: 5,
                    OrderByColumn: ContentColumns.UpdateDate
                );

                // update view count
                if (Request.Cookies["cvc_" + ContentID] == null)
                {
                    ObjCtrl.S_Content_UpdateCount(ContentID, "view");

                    HttpCookie Cookie = new HttpCookie("cvc_" + ContentID, "true");
                    Cookie.Expires = DateTime.Now.AddMinutes(10);
                    Response.Cookies.Add(Cookie);
                }
            }

            ViewBag.PageIndex = PageIndex;
            ViewBag.CurType = CurType;
            ViewBag.TotalPage = TotalPage;

            return View();
        }

        #endregion

        #region Lien He

        public ActionResult LienHe()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public JsonResult LienHe(string formContent)
        {
            Hashtable JsonTable = new Hashtable();

            try
            {
                //Hashtable EmailFormInfo = ObjSer.Deserialize<Hashtable>(formContent);
                Hashtable EmailFormInfo = JsonConvert.DeserializeObject<Hashtable>(formContent);

                string MailBody = "<p>Thông tin người liên hệ</p>";

                MailBody += "- Tên: " + EmailFormInfo["Name"].ToString() + "<br/>";
                MailBody += "- Email: " + EmailFormInfo["Email"].ToString() + "<br/>";
                MailBody += "- Điện thoại: " + EmailFormInfo["Phone"].ToString() + "<br/>";
                MailBody += "- Nội dung: " + EmailFormInfo["Content"].ToString() + "<br/>";

                string MailResult = Helpers.SendEmail("Khách hàng liên hệ từ website iidz.vn", MailBody, EmailFormInfo["Email"].ToString(), Helpers.SiteSettings.Email, null, null, EmailFormInfo["Email"].ToString());

                if (MailResult != "success") JsonTable.Add("error", MailResult);
            }
            catch (Exception ex)
            {
                JsonTable.Add("error", ex.Message);
            }

            return Json(JsonTable);
        }

        #endregion

        #region Product

        public ActionResult Product()
        {
            PartialListProductModel ListProductModel = new PartialListProductModel();

            if (RouteData.Values["ptype"] != null) ListProductModel.PType = RouteData.Values["ptype"].ToString();
            if (RouteData.Values["id"] != null) ListProductModel.CurrentID = RouteData.Values["id"].ToString();
            if (RouteData.Values["pageindex"] != null) ListProductModel.PageIndex = int.Parse(RouteData.Values["pageindex"].ToString());


            if (ListProductModel.PType == "catid")
            {
                #region Load List Product

                if (ListProductModel.CurrentID != "all")
                {
                    List<S_CategoryModel> CategoryModel = ObjCtrl.S_Category_GetByID(ListProductModel.CurrentID, CultureCode);
                    if (CategoryModel.Count > 0)
                    {
                        ListProductModel.CategoryName = CategoryModel[0].Name;
                        ListProductModel.CategoryUrlKey = CategoryModel[0].UrlKey;
                    }
                }

                ListProductModel.ListProduct = ObjCtrl.S_Product_GetList(
                    CategoryID: ListProductModel.CurrentID, 
                    PageSize: ProductPageSize, 
                    PageIndex: ListProductModel.PageIndex, 
                    TotalRow: ref TotalRow, 
                    TotalPage: ref TotalPage, 
                    FromSubCategory: true
                );

                ListProductModel.TotalPage = TotalPage;

                #endregion
            }
            else if (ListProductModel.PType == "proid")
            {
                #region Load Product Details

                List<S_ProductModel> ProductTemp = ObjCtrl.S_Product_GetByID(ListProductModel.CurrentID, CultureCode, true);
                if (ProductTemp.Count > 0)
                {
                    // get product
                    ListProductModel.ObjProduct = ProductTemp[0];

                    // get category name
                    List<S_CategoryModel> CategoryModel = ObjCtrl.S_Category_GetByID(ListProductModel.ObjProduct.CategoryID, CultureCode);
                    if (CategoryModel.Count > 0)
                    {
                        ListProductModel.CategoryName = CategoryModel[0].Name;
                        ListProductModel.CategoryUrlKey = CategoryModel[0].UrlKey;
                    }

                    // update view count
                    if (Request.Cookies["pvc_" + ListProductModel.CurrentID] == null)
                    {
                        ObjCtrl.S_Product_UpdateCount(ListProductModel.CurrentID, "view");

                        HttpCookie Cookie = new HttpCookie("pvc_" + ListProductModel.CurrentID, "true");
                        Cookie.Expires = DateTime.Now.AddMinutes(10);
                        Response.Cookies.Add(Cookie);
                    }
                }

                // get related product (same CategoryID)
                ListProductModel.ListProduct = ObjCtrl.S_Product_GetList(
                    CategoryID: ListProductModel.ObjProduct.CategoryID,
                    ExceptProductID: ListProductModel.ObjProduct.ProductID,
                    PageSize: 4,
                    TotalRow: ref TotalRow,
                    TotalPage: ref TotalPage
                );

                #endregion
            }

            return View(ListProductModel);
        }

        #endregion

        #region News

        public ActionResult News()
        {
            //tin-tuc/{id}/{urlkey}

            string CurType = "list";
            string ContentID = "-1";
            int PageIndex = 1;
            int PageSize = 10;

            if (RouteData.Values["id"] != null && RouteData.Values["urlkey"] != null)
            {
                ContentID = RouteData.Values["id"].ToString();
                CurType = "details";
            }
            else
            {
                if (RouteData.Values["id"] != null) PageIndex = int.Parse(RouteData.Values["id"].ToString());
            }

            if (CurType == "list")
            {
                ViewBag.ListContent = ObjCtrl.S_Content_GetList(
                    TotalRow: ref TotalRow,
                    TotalPage: ref TotalPage,
                    ContentType: "news",
                    CultureCode: CultureCode,
                    PageSize: PageSize,
                    PageIndex: PageIndex,
                    OrderByColumn: ContentColumns.UpdateDate
                );
            }
            else if (CurType == "details")
            {
                ViewBag.ObjContent = ObjCtrl.S_Content_GetByID(ContentID, CultureCode)[0];
                ViewBag.ListContent = ObjCtrl.S_Content_GetList(
                    TotalRow: ref TotalRow,
                    TotalPage: ref TotalPage,
                    ContentType: "news",
                    CultureCode: CultureCode,
                    PageSize: 5,
                    OrderByColumn: ContentColumns.UpdateDate
                );

                // update view count
                if (Request.Cookies["cvc_" + ContentID] == null)
                {
                    ObjCtrl.S_Content_UpdateCount(ContentID, "view");

                    HttpCookie Cookie = new HttpCookie("cvc_" + ContentID, "true");
                    Cookie.Expires = DateTime.Now.AddMinutes(10);
                    Response.Cookies.Add(Cookie);
                }
            }

            ViewBag.PageIndex = PageIndex;
            ViewBag.CurType = CurType;
            ViewBag.TotalPage = TotalPage;

            return View();
        }

        #endregion

        #region CheckOut

        public ActionResult CheckOut()
        {
            return View();
        }

        #endregion

        #region UserOrder

        public ActionResult UserOrder()
        {
            return View();
        }

        #endregion



        


        #region Cart Ctrl

        [HttpPost]
        public JsonResult CartCtrl(string ccAct, string data, string extdata)
        {
            Hashtable JsonTable = new Hashtable();
            try
            {
                if (ccAct == "load-cart-temp")
                {
                    #region Init & Load Cart Temp

                    JsonTable.Add("lstCart", ObjCtrl.S_CartTemp_GetListByUser(UserID, CultureCode, CartTempTypes.cart));

                    ApplicationUser ShortUserInfo = new ApplicationUser();
                    ShortUserInfo.DisplayName = UserInfo.DisplayName;
                    ShortUserInfo.Email = UserInfo.Email;
                    ShortUserInfo.CellPhone = UserInfo.CellPhone;
                    ShortUserInfo.Address = UserInfo.Address;

                    JsonTable.Add("userProfile", ShortUserInfo);

                    #endregion
                }
                else if (ccAct == "add-to-cart")
                {
                    #region Add To Cart

                    //S_CartTempModel Model = ObjSer.Deserialize<S_CartTempModel>(data);
                    S_CartTempModel Model = JsonConvert.DeserializeObject<S_CartTempModel>(data);
                    Model.UserID = UserID;
                    Model.TempType = "cart";

                    Model = ObjCtrl.S_CartTemp_Update(Model);

                    JsonTable.Add("newItem", Model); //d4b91762 // 29f3d3c9

                    #endregion
                }
                else if (ccAct == "update-cart")
                {
                    List<S_CartTempModel> ListTemp = JsonConvert.DeserializeObject<List<S_CartTempModel>>(data);
                    if (ListTemp != null && ListTemp.Count > 0)
                    {
                        foreach (S_CartTempModel item in ListTemp)
                        {
                            ObjCtrl.S_CartTemp_UpdateQuantity(item.CartTempID, item.Quantity);
                        }
                    }
                }
                else if (ccAct == "remove-cart")
                {
                    ObjCtrl.S_CartTemp_DeleteByID(data);
                }
                else if (ccAct == "complete-cart")
                {
                    #region Complete Cart

                    S_OrderDetailsModel ObjDetails;

                    //S_OrderModel ObjOrder = ObjSer.Deserialize<S_OrderModel>(extdata);
                    S_OrderModel ObjOrder = JsonConvert.DeserializeObject<S_OrderModel>(extdata);
                    //List<S_CartTempModel> ListCartTemp = ObjSer.Deserialize<List<S_CartTempModel>>(data);
                    //List<S_CartTempModel> ListCartTemp = JsonConvert.DeserializeObject<List<S_CartTempModel>>(data);

                    List<S_CartTempModel> ListCartTemp = ObjCtrl.S_CartTemp_GetListByUser(UserID, CultureCode, CartTempTypes.cart);

                    if (ListCartTemp.Count > 0 && ObjOrder != null)
                    {
                        #region Insert object order

                        ObjOrder.UserID = UserID;
                        ObjOrder = ObjCtrl.S_Order_Update(ObjOrder);

                        #endregion

                        #region Insert order details

                        foreach (S_CartTempModel CartTemp in ListCartTemp)
                        {
                            if (CartTemp.Quantity > 0)
                            {
                                ObjDetails = new S_OrderDetailsModel();
                                ObjDetails.OrderID = ObjOrder.OrderID;
                                ObjDetails.ProductID = CartTemp.ProductID;
                                ObjDetails.ProductName = CartTemp.ProductName;
                                ObjDetails.ProductCode = CartTemp.ProductCode;
                                ObjDetails.Quantity = CartTemp.Quantity;
                                ObjDetails.Price = CartTemp.CurrentPrice;

                                ObjCtrl.S_OrderDetails_Update(ObjDetails);
                            }
                        }

                        #endregion

                        #region Send Email

                        #region Create Mail Content

                        string TdStyle = "style='padding:10px;border:1px solid #000;'";
                        int QuantityTotal = 0;
                        decimal GrandTotal = 0;
                        decimal SubTotal = 0;

                        string MailBody = "<h2>Thông tin người đặt hàng</h2>";

                        MailBody += "- Tên: " + ObjOrder.Name + "<br/>";
                        MailBody += "- Email: " + ObjOrder.Email + "<br/>";
                        MailBody += "- Điện thoại: " + ObjOrder.Phone + "<br/>";
                        MailBody += "- Ghi chú đơn hàng: " + ObjOrder.Overview + "<br/>";
                        MailBody += "- Ngày đặt hàng: " + DateTime.Now.ToString("dd-MM-yyyy") + "<br/>";


                        MailBody = "<h2>Chi tiết đơn hàng</h2>";

                        MailBody += "<table style='border-collapse:collapse;border:1px solid #000;'>";

                        MailBody += "<tr>";
                        MailBody += "<th " + TdStyle + ">Tên sản phẩm</th>";
                        MailBody += "<th " + TdStyle + ">Đơn giá</th>";
                        MailBody += "<th " + TdStyle + ">Số lượng</th>";
                        MailBody += "<th " + TdStyle + ">Thành tiền</th>";
                        MailBody += "</tr>";

                        foreach (S_CartTempModel CartTemp in ListCartTemp)
                        {
                            if (CartTemp.Quantity > 0)
                            {
                                SubTotal = CartTemp.Quantity * CartTemp.CurrentPrice;
                                GrandTotal += SubTotal;
                                QuantityTotal += CartTemp.Quantity;

                                MailBody += "<tr>";
                                MailBody += "<td " + TdStyle + ">" + CartTemp.ProductName + "</td>";
                                MailBody += "<td " + TdStyle + ">" + Helpers.ConvertToVND(CartTemp.CurrentPrice) + "</td>";
                                MailBody += "<td " + TdStyle + ">" + CartTemp.Quantity + "</td>";
                                MailBody += "<td " + TdStyle + ">" + Helpers.ConvertToVND(SubTotal) + "</td>";
                                MailBody += "</tr>";
                            }
                        }

                        MailBody += "<tr>";
                        MailBody += "<td " + TdStyle + " colspan='2'>Tổng</td>";
                        MailBody += "<td " + TdStyle + ">" + QuantityTotal + "</td>";
                        MailBody += "<td " + TdStyle + ">" + Helpers.ConvertToVND(GrandTotal) + "</td>";
                        MailBody += "</tr>";

                        MailBody += "</table>";

                        #endregion


                        // Email for admin
                        //string AdminResult = Helpers.SendEmail("Đặt hàng từ shop", MailBody, ObjOrder.Email, "ndtien84@gmail.com", null, null, ObjOrder.Email);
                        // Email for Customer
                        //string CustomerResult = Helpers.SendEmail("Đặt hàng từ shop", MailBody, "ndt.no.reply@gmail.com", ObjOrder.Email, null, null, "ndt.no.reply@gmail.com");

                        string EmailError = "";
                        //if (AdminResult != "success") EmailError = "Error send email to admin: " + AdminResult;
                        //if (CustomerResult != "success") EmailError += "<br/>Error send email to customer: " + CustomerResult;

                        if (EmailError != "") JsonTable.Add("error", EmailError);

                        #endregion

                        // delete cart temp
                        ObjCtrl.S_CartTemp_DeleteByUser(UserID, "cart");
                    }

                    #endregion
                }
                else if (ccAct == "list-order-init")
                {
                    #region List User Order

                    using (StreamReader sr = new StreamReader(Server.MapPath("~/Content/template/UserOrderDetails.html")))
                    {
                        JsonTable.Add("tplUserOrderDetails", sr.ReadToEnd());
                    }

                    //UserID, 100, 1, "all", "all", "False", DateTime.Now, DateTime.Now, "no", "CreateDate desc", ref TotalRow, ref TotalPage, true

                    JsonTable.Add("lstOrder", ObjCtrl.S_Order_GetList(
                        TotalPage: ref TotalPage,
                        TotalRow: ref TotalRow,
                        FromDate: DateTime.Now,
                        ToDate: DateTime.Now,
                        UserID: UserID
                    ));

                    #endregion
                }
            }
            catch (Exception ex)
            {
                JsonTable.Add("error", ccAct + " : " + ex.Message);
            }

            return Json(JsonTable);
        }

        #endregion

        #region Partial List Category

        [ChildActionOnly]
        public ActionResult _ListCategory(string categoryType, string className = "")
        {
            //dich-vu/{id}/{urlkey}

            string PType = "catid"; // catid | proid
            string ID = "all"; // all | CategoryID | ProductID
            int PageIndex = 1;

            if (RouteData.Values["ptype"] != null) PType = RouteData.Values["ptype"].ToString();
            if (RouteData.Values["id"] != null) ID = RouteData.Values["id"].ToString();
            if (RouteData.Values["pageindex"] != null) PageIndex = int.Parse(RouteData.Values["pageindex"].ToString());

            string CurrentCategoryID = "-1";
            string CurrentCategoryName = "Categories";

            if (PType == "catid")
            {
                CurrentCategoryID = ID;
                List<S_CategoryModel> CategoryModel = ObjCtrl.S_Category_GetByID(ID, CultureCode);
                if (CategoryModel.Count > 0) CurrentCategoryName = CategoryModel[0].Name;
            }
            else
            {
                List<S_ContentModel> ContentModel = ObjCtrl.S_Content_GetByID(ID, CultureCode);
                if (ContentModel.Count > 0)
                {
                    CurrentCategoryID = ContentModel[0].CategoryID;
                    CurrentCategoryName = "";
                }
            }

            ViewBag.CurrentCategoryName = CurrentCategoryName;

            List<S_CategoryModel> ListCategory = new List<S_CategoryModel>();

            if (categoryType != ContentTypesConst.Service)
            {
                ListCategory = ObjCtrl.S_Category_GetListAllLevel(
                    CategoryType: (CategoryType)Enum.Parse(typeof(CategoryType), categoryType),
                    ParentsID: "-1",
                    CultureCode: CultureCode,
                    CurrentCategoryID: CurrentCategoryID
                );
            }

            #region Hack For Service

            if (categoryType == ContentTypesConst.Service)
            {
                List<S_ContentModel> ListProject = ObjCtrl.S_Content_GetList(
                    ContentType: ContentTypesConst.Service,
                    TotalRow: ref TotalRow,
                    TotalPage: ref TotalPage,
                    PageSize: 5,
                    CultureCode: CultureCode
                );

                if (ListProject != null && ListProject.Count > 0)
                {
                    string CurrentContentID = "-1";
                    if (RouteData.Values["id"] != null)
                    {
                        CurrentContentID = RouteData.Values["id"].ToString();
                    }

                    ListCategory = new List<S_CategoryModel>();

                    foreach (var item in ListProject)
                    {
                        string CategoryClass = CurrentContentID == item.ContentID ? "category-item category-level-1 item-selected" : "category-item category-level-1";
                        ListCategory.Add(new S_CategoryModel
                        {
                            CategoryID = item.ContentID,
                            CategoryType = ContentTypesConst.Service,
                            UrlKey = item.UrlKey,
                            ClassName = CategoryClass,
                            Name = item.Name
                        });
                    }
                }
            }

            #endregion


            ViewBag.ClassName = className;

            return PartialView("_ListCategory", ListCategory);
        }

        #endregion
    }
}
﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using shop.com.Models;
using shop.com.App_GlobalResources;
using System.Collections;
using Newtonsoft.Json;

using System.Linq;
using System.Collections.Generic;
using shop.com.Utils;

namespace shop.com.Controllers
{
    [Authorize]
    public class AccountController : SBaseController
    {
        #region Private Members

        private int TotalPage = 0;
        private int TotalRow = 0;

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        #endregion


        #region Register

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, DisplayName = model.DisplayName };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    Helpers.SetOnlineCount(false, false); // decrease guest count
                    Helpers.SetOnlineCount(true, true); // increase member count

                    var NewUser = UserManager.Users.Where(u => u.UserName == model.Email).First();

                    ObjCtrl.Uti_MoveDataUserTemp(UserID, NewUser.Id); // update cart temp to this user

                    // create log
                    ObjCtrl.S_Log_Create(
                        LogType: S_LogType.UserRegister,
                        UserID: UserID,
                        CurRequest: Request,
                        details: NewUser
                    );

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return RedirectToAction("Index", "Home");
                }
                AddErrors(result);
            }

            return View();
        }

        #endregion

        #region Login & Logout

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    Helpers.SetOnlineCount(false, false); // decrease guest count
                    Helpers.SetOnlineCount(true, true); // increase member count

                    ApplicationUser CurUser = UserManager.Users.Where(u => u.UserName == model.Email).First();
                    // update cart temp to this user
                    ObjCtrl.Uti_MoveDataUserTemp(UserID, CurUser.Id);
                    // set last login date
                    CurUser.LastLoginDate = DateTime.Now;
                    UserManager.Update(CurUser);

                    // create log
                    ObjCtrl.S_Log_Create(
                        LogType: S_LogType.UserLogin,
                        CurRequest: Request,
                        UserID: CurUser.Id
                    );

                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                    ModelState.AddModelError("", ResGlobal.LoginFail);
                    return View(model);
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            Helpers.SetOnlineCount(true, false);// descrease member count
            Helpers.SetOnlineCount(false, true);// increase guest count
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        #endregion

        #region Update Profile

        public ActionResult UpdateProfile()
        {
            return View();
        }

        [HttpPost]        
        public JsonResult UpdateProfile(string userProfile)
        {
            Hashtable JsonTable = new Hashtable();

            try
            {
                var OldProfile = Helpers.DeepCopy(UserInfo);

                S_UsersModel UserProfile = JsonConvert.DeserializeObject<S_UsersModel>(userProfile);

                UserInfo.DisplayName = UserProfile.DisplayName;
                UserInfo.CellPhone = UserProfile.CellPhone;
                UserInfo.Address = UserProfile.Address;
                UserManager.Update(UserInfo);

                // create log
                ObjCtrl.S_Log_Create(
                    LogType: S_LogType.UserUpdateProfile,
                    UserID: UserID,
                    CurRequest: Request,
                    details: new object[] { OldProfile, UserInfo }
                );
            }
            catch (Exception ex)
            {
                JsonTable.Add("error", ex.Message);
            }

            return Json(JsonTable);
        }

        #endregion

        #region Change Password

        public ActionResult ChangePassword(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
               message == ManageMessageId.ChangePasswordSuccess ? ResGlobal.ChangePasswordSuccess
               : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
               : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
               : "";

            ViewBag.ReturnUrl = Url.Action("ChangePassword");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("ChangePassword", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);

            return View(model);
        }

        #endregion


        #region Forgot Password

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            if (Request.QueryString["token"] != null)
            {
                string RequestUserID = Request.QueryString["token"].ToString();

                if (UserManager.Users.Where(u => u.Id == RequestUserID).Count() > 0)
                {
                    string UserEmail = UserManager.Users.Where(u => u.Id == RequestUserID).First().Email;


                    string NewPassword = Helpers.GenerateID();

                    UserManager.RemovePassword(RequestUserID);
                    UserManager.AddPassword(RequestUserID, NewPassword);

                    string MailContent = "<h2>Your new password: " + NewPassword + ". Please change your password immediately after logging in</h2>";
                    Helpers.SendEmail(MailSubject: "[" + Helpers.Domain.Replace("http://", "") + "] Reset password", MailBody: MailContent, MailFrom: Helpers.SiteSettings.Email, MailTo: UserEmail, CC: null, BCC: null, ReplyTo: Helpers.SiteSettings.Email);

                    ViewBag.ResetResult = "success";
                }
                else
                {
                    ViewBag.ResetResult = "invalidtoken";
                }
            }

            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(string Email)
        {
            if (Email == "") ViewBag.Message = ResGlobal.RequireEmail;
            else
            {
                if (UserManager.Users.Where(u => u.Email == Email).Count() <= 0) ViewBag.Message = ResGlobal.EmailNotExist;
                else
                {
                    string UserID = UserManager.Users.Where(u => u.Email == Email).First().Id;

                    string MailContent = "<h2>You received a request to reset password from " + Helpers.Domain + "</h2>";
                    MailContent += "<a href='" + Helpers.Domain + "/account/forgot-password?token=" + UserID + "'>Click here to reset your password</a>";
                    Helpers.SendEmail(MailSubject: "[" + Helpers.Domain.Replace("http://", "") + "] Reset password", MailBody: MailContent, MailFrom: Helpers.SiteSettings.Email, MailTo: Email, CC: null, BCC: null, ReplyTo: Helpers.SiteSettings.Email);

                    ViewBag.Message = "A confirmation email has been sent to your email address";
                }
            }

            return View();
        }

        #endregion


        #region Create User
        [Authorize(Roles = "Host, Admin")]
        public ActionResult CreateUser(ManageMessageId? message)
        {
            if (message == ManageMessageId.CreateUserSuccess) ViewBag.StatusMessage = "Create user success";

            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Host, Admin")]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<ActionResult> CreateUser(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, DisplayName = model.DisplayName };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    //await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    // create log
                    ObjCtrl.S_Log_Create(
                        LogType: S_LogType.UserCreateByAdmin,
                        UserID: UserID,
                        CurRequest: Request,
                        details: user
                    );

                    return RedirectToAction("CreateUser", "Account", new { Message = ManageMessageId.CreateUserSuccess });
                }
                AddErrors(result);
            }

            return View();
        }

        #endregion

        #region Manage User

        [Authorize(Roles = "Host, Admin")]
        public ActionResult ManageUser()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Host, Admin")]
        public JsonResult ManageUser(string act, string data)
        {
            Hashtable JsonTable = new Hashtable();

            try
            {
                if (act == "load-init")
                {
                    #region Load Init

                    //JsonTable.Add("lstUser", ObjCtrl.S_Users_GetList("all", "", 10, int.Parse(data), "Email", ref TotalRow, ref TotalPage, true));
                    //JsonTable.Add("totalPage", TotalPage);
                    JsonTable.Add("lstRole", ObjCtrl.S_Roles_GetList());

                    #endregion
                }
                else if (act == "get-user")
                {
                    #region Get List USer

                    Hashtable FilterTable = JsonConvert.DeserializeObject<Hashtable>(data);

                    List<S_UsersModel> ListUser = new List<S_UsersModel>();

                    string FilterType = FilterTable["type"].ToString();
                    string ColumnName = FilterTable["colName"].ToString();
                    string Keyword= FilterTable["keyword"].ToString();
                    int PageSize = int.Parse(FilterTable["pageSize"].ToString());
                    int PageIndex = int.Parse(FilterTable["pageIndex"].ToString());

                    if (FilterType == "all")
                    {
                        ListUser = ObjCtrl.S_Users_GetList(
                            TotalRow: ref TotalRow,
                            TotalPage: ref TotalPage,
                            PageSize: PageSize,
                            PageIndex: PageIndex
                        );
                    }
                    else if (FilterType == "search")
                    {
                        ListUser = ObjCtrl.S_Users_GetList(
                            TotalRow: ref TotalRow,
                            TotalPage: ref TotalPage,
                            Keyword: Keyword,
                            KeywordColumn: ColumnName,
                            PageSize: PageSize,
                            PageIndex: PageIndex
                        );
                    }

                    JsonTable.Add("lstUser", ListUser);
                    JsonTable.Add("totalPage", TotalPage);
                    JsonTable.Add("totalRow", TotalRow);

                    #endregion
                }
                else if (act == "update-user")
                {
                    #region Update User

                    Hashtable UserData = JsonConvert.DeserializeObject<Hashtable>(data);

                    string InputUserID = UserData["userid"].ToString();
                    string LstRole = UserData["listRole"].ToString();
                    string NewPassword = UserData["newPass"].ToString();

                    ApplicationUser OldProfile = Helpers.DeepCopy(UserManager.FindById(InputUserID));

                    // set new role for user
                    ObjCtrl.S_Roles_DeleteUserRole(InputUserID);
                    if (LstRole.Length > 0)
                    {
                        string[] SelectedRole = LstRole.Split('|');
                        for (int i = 0; i < SelectedRole.Length; i++)
                        {
                            ObjCtrl.S_Roles_AddUserRole(InputUserID, SelectedRole[i]);
                        }
                    }

                    // set new password for user if ...
                    if (NewPassword.Length > 4)
                    {
                        UserManager.RemovePassword(InputUserID);
                        UserManager.AddPassword(InputUserID, NewPassword);
                    }

                    ApplicationUser NewProfile = UserManager.FindById(InputUserID);

                    // create log
                    ObjCtrl.S_Log_Create(
                        LogType: S_LogType.UserUpdateByAdmin,
                        UserID: UserID,
                        CurRequest: Request,
                        details: new object[] { OldProfile, NewProfile }
                    );

                    #endregion
                }
                else if (act == "delete-user")
                {
                    #region Delete User

                    ApplicationUser DUser = UserManager.FindById(data);
                    if (DUser != null)
                    {
                        UserManager.Delete(DUser);

                        // create log
                        ObjCtrl.S_Log_Create(
                            LogType: S_LogType.UserDelete,
                            UserID: UserID,
                            CurRequest: Request,
                            details: DUser
                        );
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                JsonTable.Add("error", ex.Message);
            }

            return Json(JsonTable);
        }

        #endregion


        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            CreateUserSuccess,
            Error
        }

        #endregion
    }
}
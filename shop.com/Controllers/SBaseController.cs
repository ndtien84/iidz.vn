﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using Microsoft.AspNet.Identity;
using shop.com.Models;
using Microsoft.AspNet.Identity.Owin;
using System.Reflection;
using System.Collections;
using shop.com.Utils;
using shop.com.Services;

namespace shop.com.Controllers
{
    public class SBaseController : Controller
    {
        public ShopServices ObjCtrl = new ShopServices();
        public string CultureCode { get; set; }
        public string UserID { get; set; }
        public ApplicationUser UserInfo { get; set; }

        public SBaseController()
        {
            CultureCode = Helpers.DefaultCultureCode;
            UserID = "-1";
            UserInfo = new ApplicationUser();
        }

        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            #region User Profiles

            if (User.Identity.IsAuthenticated)
            {
                UserID = User.Identity.GetUserId();
                UserInfo = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(UserID);
            }

            ViewBag.UserInfo = UserInfo;

            #endregion

            #region Site Settings

            Hashtable SettingTable = ObjCtrl.S_Settings_GetList("site");

            PropertyInfo[] ListProperties = Helpers.SiteSettings.GetType().GetProperties();
            if (ListProperties.Length > 0)
            {
                foreach (PropertyInfo Prop in ListProperties)
                {
                    if (SettingTable[Prop.Name] != null)
                    {
                        if (Prop.PropertyType == typeof(string))
                        {
                            Helpers.SiteSettings.GetType().GetProperty(Prop.Name).SetValue(Helpers.SiteSettings, SettingTable[Prop.Name]);
                        }
                        else if (Prop.PropertyType == typeof(bool))
                        {
                            try
                            {
                                Helpers.SiteSettings.GetType().GetProperty(Prop.Name).SetValue(Helpers.SiteSettings, bool.Parse(SettingTable[Prop.Name].ToString()));
                            }
                            catch (Exception ex) { }
                        }
                    }
                }
            }

            #endregion

            ObjCtrl.CurrentRequest = Request;
            ObjCtrl.UserID = UserID;

            if (UserID == "-1")
            {
                if (Request.Cookies["_suid"] != null) UserID = Request.Cookies["_suid"].Value.ToString();
                else
                {
                    string NewID = Helpers.GenerateID();
                    HttpCookie Cookie = new HttpCookie("_suid", NewID);
                    Cookie.Expires = DateTime.Now.AddYears(1);
                    Response.Cookies.Add(Cookie);
                    UserID = NewID;
                }
            }

            if (Request.Cookies["_culture"] != null) CultureCode = Request.Cookies["_culture"].Value.ToString();

            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(CultureCode);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            return base.BeginExecuteCore(callback, state);
        }
    }
}
﻿using shop.com.Models;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;

using System.Net.Mail;
using System.Net;
using System.Collections.Generic;

using System.Linq;
using Newtonsoft.Json;
using shop.com.Services;


namespace shop.com.Utils
{
    public static class Helpers
    {
        public static S_SiteSettingsModel SiteSettings = new S_SiteSettingsModel();
        public static S_SiteCountModel SiteCount = new S_SiteCountModel();

        public static string Domain
        {
            get
            {
                return System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            }
        }

        public static string DefaultCultureCode
        {
            get
            {
                return "vi-vn";
                //return "en-us";
            }
        }

        public static string GetRawPhoneNumber(string Input)
        {
            Input = new string(Input.Where(c => char.IsNumber(c)).ToArray());
            return Input;
        }

        public static T DeepCopy<T>(T SourceObject)
        {
            var serialized = JsonConvert.SerializeObject(SourceObject);
            return JsonConvert.DeserializeObject<T>(serialized);
        }

        #region Create Page

        public static List<S_PageModel> ListPages
        {
            get
            {
                S_PageModel ObjP;
                List<S_PageModel> ListP = new List<S_PageModel>();

                ObjP = new S_PageModel();
                ObjP.ActionName = "Index";
                ObjP.LinkTitle = "Trang chủ";
                ObjP.PageTitle = "Trang chủ";
                ObjP.UrlKey = "index";
                ListP.Add(ObjP);

                ObjP = new S_PageModel();
                ObjP.ActionName = "About";
                ObjP.LinkTitle = "Giới thiệu";
                ObjP.PageTitle = "Giới thiệu";
                ObjP.UrlKey = "about";
                ListP.Add(ObjP);

                ObjP = new S_PageModel();
                ObjP.ActionName = "Product";
                ObjP.LinkTitle = "Sản phẩm";
                ObjP.PageTitle = "Sản phẩm";
                ObjP.UrlKey = "product";
                ListP.Add(ObjP);

                ObjP = new S_PageModel();
                ObjP.ActionName = "News";
                ObjP.LinkTitle = "Tin tức";
                ObjP.PageTitle = "Tin tức";
                ObjP.UrlKey = "news";
                ListP.Add(ObjP);

                ObjP = new S_PageModel();
                ObjP.ActionName = "Contact";
                ObjP.LinkTitle = "Liên hệ";
                ObjP.PageTitle = "Liên hệ";
                ObjP.UrlKey = "contact";
                ListP.Add(ObjP);

                return ListP;
            }
        }

        #endregion


        #region Site Count

        public static void ResetOnlineCount()
        {
            using (ShopServices MyCtrl = new ShopServices())
            {
                S_SettingsModel ModelMember = MyCtrl.S_Settings_GetByID("OnlineMember", "count");
                if (ModelMember == null)
                {
                    ModelMember = new S_SettingsModel();
                    ModelMember.SKey = "OnlineMember";
                    ModelMember.SType = "count";
                }
                ModelMember.SValue = "0";
                MyCtrl.S_Settings_Update(ModelMember);

                S_SettingsModel ModelGuest = MyCtrl.S_Settings_GetByID("OnlineGuest", "count");
                if (ModelGuest == null)
                {
                    ModelGuest = new S_SettingsModel();
                    ModelGuest.SKey = "OnlineGuest";
                    ModelGuest.SType = "count";
                }
                ModelGuest.SValue = "0";
                MyCtrl.S_Settings_Update(ModelGuest);
            }
        }

        public static void SetOnlineCount(bool IsMember, bool Increase)
        {
            System.Web.HttpContext.Current.Session["IsAuthenticated"] = IsMember;

            using (ShopServices MyCtrl = new ShopServices())
            {
                try
                {
                    string SKey = IsMember ? "OnlineMember" : "OnlineGuest";

                    S_SettingsModel Model = MyCtrl.S_Settings_GetByID(SKey, "count");

                    if (Model == null)
                    {
                        Model = new S_SettingsModel();
                        Model.SKey = SKey;
                        Model.SType = "count";
                        Model.SValue = "0";
                    }

                    if (Increase) Model.SValue = (int.Parse(Model.SValue) + 1).ToString();
                    else Model.SValue = (int.Parse(Model.SValue) - 1).ToString();

                    MyCtrl.S_Settings_Update(Model);

                    Hashtable CountS = MyCtrl.S_Settings_GetList("count");
                    if (CountS["OnlineMember"] != null) SiteCount.OnlineMember = int.Parse(CountS["OnlineMember"].ToString());
                    if (CountS["OnlineGuest"] != null) SiteCount.OnlineGuest = int.Parse(CountS["OnlineGuest"].ToString());
                    if (CountS["TotalVisit"] != null) SiteCount.TotalVisit = int.Parse(CountS["TotalVisit"].ToString());

                    if (SiteCount.OnlineGuest < 0) SiteCount.OnlineGuest = 0;
                    if (SiteCount.OnlineMember < 0) SiteCount.OnlineMember = 0;
                }
                catch(Exception ex)
                {
                    StreamWriter st = File.CreateText(GetMediaFolderPath() + "error_" + GenerateID() + ".txt");
                    st.WriteLine(ex.Message);
                    st.Flush();
                    st.Close();
                }
            }
        }

        public static void IncreaseVisitCount()
        {
            using (ShopServices MyCtrl = new ShopServices())
            {
                S_SettingsModel Model = new S_SettingsModel();
                Model = MyCtrl.S_Settings_GetByID("TotalVisit", "count");
                if (Model == null)
                {
                    Model = new S_SettingsModel();
                    Model.SKey = "TotalVisit";
                    Model.SType = "count";
                    Model.SValue = "0";
                }
                Model.SValue = (int.Parse(Model.SValue) + 1).ToString();
                MyCtrl.S_Settings_Update(Model);

                SiteCount.TotalVisit = int.Parse(Model.SValue);
            }
        }

        #endregion



        #region Send Email

        /// <summary>
        /// 
        /// </summary>
        /// <param name="MailSubject"></param>
        /// <param name="MailBody"></param>
        /// <param name="MailFrom"></param>
        /// <param name="MailTo">mail to muliple email by using comma character (,)</param>
        /// <param name="CC">null or CC to muliple email by using comma character (,)</param>
        /// <param name="BCC">null or BCC to muliple email by using comma character (,)</param>
        /// <param name="ReplyTo"></param>
        /// <returns>success | error message</returns>
        public static string SendEmail(string MailSubject, string MailBody, string MailFrom, string MailTo, string CC, string BCC, string ReplyTo)
        {
            try
            {
                var message = new MailMessage();
                //message.To.Add(new MailAddress(MailTo));
                message.To.Add(MailTo);
                if (CC != null) message.CC.Add(CC);
                if (BCC != null) message.Bcc.Add(BCC);
                message.From = new MailAddress(MailFrom);
                message.ReplyTo = new MailAddress(ReplyTo);
                message.Subject = MailSubject;
                message.Body = MailBody;
                message.IsBodyHtml = true;

                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = SiteSettings.SMTPUsername,
                        Password = SiteSettings.SMTPPassword
                    };
                    smtp.Credentials = credential;
                    smtp.Host = SiteSettings.SMTPHost;
                    smtp.Port = int.Parse(SiteSettings.SMTPPort);
                    smtp.EnableSsl = SiteSettings.SMTPEnableSSL;
                    //await smtp.SendMailAsync(message);
                    smtp.Send(message);
                }

                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion


        #region Check Is Image File

        public static bool IsImage(string FileName)
        {
            string[] Temp = FileName.Split('.');
            string FileExt = Temp[Temp.Length - 1];

            if (FileExt.ToLower() == "jpg" || FileExt.ToLower() == "jpeg" || FileExt.ToLower() == "png" || FileExt.ToLower() == "gif" || FileExt.ToLower() == "bmp")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region Convert VND

        public static string ConvertToVND(string InputMoney)
        {
            int j = 0;
            for (int i = InputMoney.Length; i >= 0; i--)
            {
                if (j % 3 == 0 && j != 0 && i != 0)
                {
                    InputMoney = InputMoney.Insert(i, ".");
                }
                j++;
            }
            return InputMoney;
        }

        public static string ConvertToVND(decimal Money)
        {
            string InputMoney = "";

            InputMoney = Money.ToString("0");


            int j = 0;
            for (int i = InputMoney.Length; i >= 0; i--)
            {
                if (j % 3 == 0 && j != 0 && i != 0)
                {
                    InputMoney = InputMoney.Insert(i, ".");
                }
                j++;
            }
            return InputMoney;
        }

        #endregion

        #region Delete Media File

        public static bool DeleteMediaFile(string FileName)
        {
            try
            {
                FileInfo ObjFile = new FileInfo(GetMediaFolderPath() + FileName);
                if (ObjFile.Exists) ObjFile.Delete();

                ObjFile = new FileInfo(GetMediaFolderPath() + "thumbs-" + FileName);
                if (ObjFile.Exists) ObjFile.Delete();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion

        #region Date Time

        public static string GetWeekDay(DateTime ObjDate, string CultureC)
        {
            string ReturnString = ObjDate.DayOfWeek.ToString();

            if (CultureC.ToLower() == "vi-vn")
            {
                switch (ObjDate.DayOfWeek.ToString().ToLower())
                {
                    case "monday":
                        ReturnString = "Thứ 2";
                        break;
                    case "tuesday":
                        ReturnString = "Thứ 3";
                        break;
                    case "wednesday":
                        ReturnString = "Thứ 4";
                        break;
                    case "thursday":
                        ReturnString = "Thứ 5";
                        break;
                    case "friday":
                        ReturnString = "Thứ 6";
                        break;
                    case "saturday":
                        ReturnString = "Thứ 7";
                        break;
                    case "sunday":
                        ReturnString = "Chủ Nhật";
                        break;
                    default:
                        ReturnString = "";
                        break;
                }
            }

            return ReturnString;
        }

        #endregion

        #region Generate

        public static string GenerateID()
        {
            string NewID = "";

            Uri objUri = new Uri("http://ndtnotes.com/" + Guid.NewGuid().ToString());
            NewID = string.Format("{0:X}", objUri.GetHashCode()).ToLower();

            return NewID;
        }

        // generate image name
        public static string GenerateFileName(string FileName)
        {
            string[] Temp = FileName.Split('.');

            string FileExt = Temp[Temp.Length - 1];
            string Name = ConvertASCIIChar(Temp[0]);

            return GenerateID() + "_" + Name + "." + FileExt;
        }

        #endregion

        #region Static Link & Path

        public static string GetYoutubeID(string YoutubeUrl)
        {
            return Regex.Match(YoutubeUrl, @"youtu(?:\.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)").Groups[1].Value;
        }

        public static string GetMediaFolderPath()
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "s-media") + @"\";
        }

        public static string GetMediaLink(string FileName)
        {
            return Domain + "/s-media/" + FileName;
        }

        public static string GetMediaLink()
        {
            return Domain + "/s-media/";
        }

        #endregion

        #region Convert ASCII Char

        public static string ConvertASCIIChar(string InputString)
        {
            InputString = InputString.ToLower();

            string Filter1 = "áàảãạăắặằẳẵâấầẩẫậ";
            string Filter2 = "éèẻẽẹêếềểễệ";
            string Filter3 = "íìỉĩị";
            string Filter4 = "óòỏõọôốồổỗộơớờởỡợ";
            string Filter5 = "úùủũụưứừửữự";
            string Filter6 = "ýỳỷỹỵ";
            string Filter7 = @"`~!@#$%^&*()-_=+{}[]\|/?.>,<:""'";

            foreach (char myChar in Filter1.ToCharArray())
            {
                InputString = InputString.Replace(myChar.ToString(), "a");
            }
            foreach (char myChar in Filter2.ToCharArray())
            {
                InputString = InputString.Replace(myChar.ToString(), "e");
            }
            foreach (char myChar in Filter3.ToCharArray())
            {
                InputString = InputString.Replace(myChar.ToString(), "i");
            }
            foreach (char myChar in Filter4.ToCharArray())
            {
                InputString = InputString.Replace(myChar.ToString(), "o");
            }
            foreach (char myChar in Filter5.ToCharArray())
            {
                InputString = InputString.Replace(myChar.ToString(), "u");
            }
            foreach (char myChar in Filter6.ToCharArray())
            {
                InputString = InputString.Replace(myChar.ToString(), "y");
            }
            foreach (char myChar in Filter7.ToCharArray())
            {
                InputString = InputString.Replace(myChar.ToString(), " ");
            }

            InputString = InputString.Replace("đ", "d");

            InputString = InputString.Replace("'", "");
            InputString = InputString.Replace("\"", "");
            InputString = InputString.Replace("  ", " ");
            InputString = InputString.Replace("   ", " ");
            InputString = InputString.Replace("    ", " ");
            InputString = InputString.Replace("     ", " ");
            InputString = InputString.Replace("      ", " ");

            InputString = InputString.Replace(" ", "-");

            return InputString;
        }

        #endregion
    }
}
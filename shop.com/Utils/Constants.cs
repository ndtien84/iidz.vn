﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace shop.com.Utils
{
    public static class ContentTypesConst
    {
        public static string HomeBanner = "home-banner";

        public static string Banner = "banner";

        public static string Youtube = "youtube";

        public static string ExternalLink = "ext-link";

        public static string AdvertiseBanner = "ad-banner";

        public static string AboutUs = "about-us";

        public static string News = "news";

        public static string Team = "team";

        public static string ProductImage = "product-image";

        public static string ProductDetailsImage = "product-details-img";

        public static string ContentImage = "content-image";

        public static string ContentDetailsImage = "content-details-img";

        public static string Service = "service";

        public static string Project = "project";
    }
}
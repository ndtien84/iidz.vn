﻿namespace shop.com.Utils
{
    public enum S_LogType
    {
        All = 0,
        CreateLogError = 1,
        UserLogin = 2,
        UserRegister = 3,
        UserUpdateProfile = 4,
        UserCreateByAdmin = 5,
        UserUpdateByAdmin = 6,
        UserDelete = 7,

        ProductAdd = 8,
        ProductUpdate = 9,
        ProductDelete = 10,

        ContentAdd = 11,
        ContentUpdate = 12,
        ContentDelete = 13,

        OrderCreate = 14,
        OrderUpdate = 15,
        OrderDelete = 16
    }

    public enum GetListType
    {
        full,
        simple
    }

    public enum CategoryType
    {
        product,
        news,
        service,
        project
    }

    public enum Visible
    {
        all,
        True,
        False
    }

    public enum IsDeleted
    {
        all,
        True,
        False
    }

    public enum IsOutOfStock
    {
        all,
        True,
        False
    }

    public enum OrderDirection
    {
        desc,
        asc
    }

    public enum ProductPriceRangeColumns
    {
        no,
        OldPrice,
        CurrentPrice
    }

    public enum CategoryColumns
    {
        all,
        CategoryID,
        CultureCode,
        CategoryType,
        GroupName,
        ParentsID,
        PrimaryImage,
        SecondaryImage,
        PrimaryDocument,
        SecondaryDocument,
        JsonGlobal,
        Label,
        Priority,
        Visible,
        IsDeleted,
        StartDate,
        EndDate,
        CreateBy,
        LastUpdateBy,
        CreateDate,
        UpdateDate,

        Name,
        UrlKey,
        JsonCulture,
        Overview,
        Information,
        Summary,
        Details,
    }

    public enum ProductColumns
    {
        all,
        ProductID,
        CultureCode,
        CategoryID,
        GroupName,
        PrimaryImage,
        SecondaryImage,
        ReserveImage,
        PrimaryDocument,
        SecondaryDocument,
        ReserveDocument,
        JsonGlobal,
        Label,
        Priority,
        Visible,
        IsDeleted,
        IsOutOfStock,
        ProductCode,
        Quantity,
        ViewCount,
        LikeCount,
        OldPrice,
        CurrentPrice,
        PriceString,
        Tag,
        Link,
        StartDate,
        EndDate,
        CreateBy,
        LastUpdateBy,
        CreateDate,
        UpdateDate,

        Name,
        UrlKey,
        JsonCulture,
        Source,
        PostedBy,
        Description,
        Overview,
        Information,
        Summary,
        Details
    }

    public enum ContentColumns
    {
        all,
        ContentID,
        CultureCode,
        CategoryID,
        GroupName,
        ContentType,
        ParentsID,
        ParentsTable,
        PrimaryImage,
        SecondaryImage,
        ReserveImage,
        PrimaryDocument,
        SecondaryDocument,
        ReserveDocument,
        JsonGlobal,
        Label,
        Link,
        Tag,
        Priority,
        ViewCount,
        LikeCount,
        Visible,
        IsDeleted,
        StartDate,
        EndDate,
        CreateBy,
        LastUpdateBy,
        CreateDate,
        UpdateDate,

        Name,
        UrlKey,
        JsonCulture,
        Description,
        Overview,
        Information,
        Summary,
        Details
    }

    public enum CartTempColumns
    {
        all,
        CartTempID,
        TempType,
        UserID,
        ProductID,
        Quantity,
        Notes,
        Overview,
        Summary,
        CreateDate,
        ExpiredDate
    }

    public enum OrderColumns
    {
        all,
        no,
        OrderID,
        InvoiceID,
        UserID,

        Name,
        Phone,
        CellPhone,
        Email,
        Country ,
        State,
        City,
        Address,
        ShipInfo,

        Tax,
        ShipCost,
        AdditionAmount,
        PromoteInfo,

        PaymentType,
        StatusID,

        Overview,
        Summary,
        Description,
        Details,

        Approved,
        IsDelete,

        CreateDate,
        UpdateDate,
        AprrovedDate
    }

    public enum OrderDetailsColumns
    {
        all,
        DetailsID,
        OrderID,
        ProductID,
        ProductName,
        ProductCode,
        Quantity,
        Price,
        Notes,
        Overview,
        Summary
    }

    public enum CartTempTypes
    {
        all,
        cart,
        wishlist,
        compare
    }

    public enum OrdersApproved
    {
        all,
        True,
        False
    }
}